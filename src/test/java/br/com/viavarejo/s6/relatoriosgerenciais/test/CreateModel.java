package br.com.viavarejo.s6.relatoriosgerenciais.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;

public class CreateModel {

	//private static Class<?> clazz = DesvioHist.class;
	private static StringBuilder builder; 
	
	
	public static void main(String... args) throws Exception {
		Map<String, Class<?>> allClass = getClassList();
		BufferedWriter writer = null;
		for (String key : allClass.keySet()) {
			Class<?> clazz = allClass.get(key);
			try {
				writer = new BufferedWriter(new FileWriter(new File("c:/projetos/generated/models/" + clazz.getSimpleName() + ".ts")));
				//writer = Files.newBufferedWriter(Paths.get("c:/projetos/models/" + clazz.getSimpleName() + ".ts"));
 				writer.write(writeModelString(clazz));
 				writer.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private static String writeModelString(Class<?> clazz) {
		if(builder == null) builder = new StringBuilder();
		if(builder != null) builder.setLength(0);
		builder.append("export class ");
		builder.append(clazz.getSimpleName());
		builder.append(" {\n");
		Field[] fields = clazz.getDeclaredFields();
		for(Field field : fields) {
			if(!field.getName().equals("serialVersionUID")) {
				if(field.getType().isAssignableFrom(String.class)) {
					builder.append("\t");
					builder.append(field.getName());
					builder.append(": String;");
					//System.out.println(field.getName() + ": String");
				}
				else if(field.getType().isAssignableFrom(Integer.class) || 
						field.getType().isAssignableFrom(BigDecimal.class) ||
						field.getType().isAssignableFrom(Long.class) ||
						field.getType().isAssignableFrom(int.class)) {
					builder.append("\t");
					builder.append(field.getName());
					builder.append(": Number;");
					//System.out.println(field.getName() + ": Number");
				}
				else if(field.getType().isAssignableFrom(Date.class) ||
						field.getType().isAssignableFrom(Time.class)) {
					builder.append("\t");
					builder.append(field.getName());
					builder.append(": Date;");
					//System.out.println(field.getName() + ": Date");
				}
				else {
					builder.append("\t");
					builder.append(field.getName());
					builder.append(": ");
					builder.append(field.getType());
					//System.out.println(field.getName() + ": " + field.getType());
				}
				
				builder.append("\n");
			}
		}
		// Constructor
		builder.append("\n\tconstructor(values: Object = {}) {\n");
		builder.append("\t\tObject.assign(this, values);\n");
		builder.append("\t}\n");
		builder.append("}");
		return builder.toString();
	}
	
	
	private static Map<String, Class<?>> getClassList() throws ClassNotFoundException {
		Map<String, Class<?>> allClasses = new HashMap<String, Class<?>>();
		final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(
				false);
		provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));
		final Set<BeanDefinition> classes = provider
				.findCandidateComponents("br.com.viavarejo.s6.relatoriosgerenciais.domain.model");
		for (BeanDefinition bean : classes) {
			Class<?> clazz = Class.forName(bean.getBeanClassName());
			allClasses.put(clazz.getSimpleName(), clazz);
		}
		return allClasses;
	}

}
