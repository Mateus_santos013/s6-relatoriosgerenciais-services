package br.com.viavarejo.s6.relatoriosgerenciais.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.model.RelatorioCSV;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.file.XLSFormatter;

public class CreateFile {

	private static final String FILE_NAME = "c:/projetos/teste.xls";

	public static void main(String[] args) {
		List<RelatorioCSV> list = new ArrayList<RelatorioCSV>();
		RelatorioCSV rel = new RelatorioCSV();
		rel.setNumLinha(1);
		rel.setConteudo("teste1;teste2;teste3;teste4;teste5");
		list.add(rel);
		list.add(rel);
		list.add(rel);
		HSSFWorkbook workbook = new XLSFormatter().geraXLS(list, "Relatorio XPTO");

		try {
			FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Done");
	}
}
