package br.com.viavarejo.s6.relatoriosgerenciais.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityCredentials;

public class SecurityCredentialsTest {

	public static void main(String... args) throws JsonProcessingException {
		System.out.println("started\n");
		
		SecurityCredentials credentials = new SecurityCredentials();
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.println(mapper.writeValueAsString(credentials));
		
		System.out.println("\nended");
	}

}
