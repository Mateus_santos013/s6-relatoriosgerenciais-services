package br.com.viavarejo.s6.relatoriosgerenciais.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class CSVTest {

	private final static String JSON = "[\n\t{\"nome\" : \"Kleiton\", \"Sobrenome\" : \"Brito\"},\n\t{\"nome\" : \"Jose\"}\n]"; 
	public static void main(String... args) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("Started");
		
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode root = mapper.readValue(JSON, ArrayNode.class);
		Iterator<String> fieldsIterator = root.get(0).fieldNames();
		
		
		List<String> fields = new ArrayList<String>();
		while (fieldsIterator.hasNext()) {
			fields.add(fieldsIterator.next());
		}
		
		for(JsonNode node : root) {
			for(String field : fields) {
				System.out.println(node.get(field));
			}
			
		}
		System.out.println("Ended");
	}
}
