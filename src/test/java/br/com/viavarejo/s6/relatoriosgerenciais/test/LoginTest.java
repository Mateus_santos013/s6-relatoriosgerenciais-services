package br.com.viavarejo.s6.relatoriosgerenciais.test;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityCredentials;

public class LoginTest {

	private final static String json = "{\r\n" + 
			"	\"empresa\": 77,\r\n" + 
			"	\"matricula\": 59722,\r\n" + 
			"	\"senha\": \"homolog02\"\r\n" + 
			"	\r\n" + 
			"}";
	
	public static void main(String... args) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		SecurityCredentials user = mapper.readValue(json, SecurityCredentials.class);
		System.out.println(user.toString());
		
		System.out.println(mapper.writeValueAsString(user));
		
		
		
	}

}
