package br.com.viavarejo.s6.relatoriosgerenciais.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

public class CreateServices {

	private static final String servicesPackage = "br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa";
	private static StringBuilder builder; 
	
	public static void main(String... args) throws Exception {
		Map<String, Class<?>> allClass = getClassList();
		BufferedWriter writer = null;
		for (String key : allClass.keySet()) {
			Class<?> clazz = allClass.get(key);
			try {
				writer = new BufferedWriter(new FileWriter(new File("c:/projetos/generated/services/" + clazz.getSimpleName().split("Controller")[0] + ".service.ts")));
				//writer = Files.newBufferedWriter(Paths.get("c:/projetos/models/" + clazz.getSimpleName() + ".ts"));
 				writer.write(writeServiceString(clazz));
 				writer.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
		
		
		
	}
	
	private static String writeServiceString(Class<?> clazz) {
		if(builder == null) builder = new StringBuilder();
		if(builder != null) builder.setLength(0);
		
		String simpleClassName = clazz.getSimpleName().split("Controller")[0];
		
		builder.append("import { Injectable }                     from '@angular/core';\n");
		builder.append("import { environment  }                   from 'environments/environment';\n");
		builder.append("import { Response }                 	  from '@angular/http';\n");
		builder.append("import { Observable, throwError }         from 'rxjs';\n");
		builder.append("import { HttpClient } 					  from '@angular/common/http';\n");
		builder.append("import { map, catchError }                from 'rxjs/operators';\n");
		// Model Import
		builder.append("import { ");
		builder.append(simpleClassName);
		builder.append(" }\t\t\tfrom 'models/");
		builder.append(simpleClassName);
		builder.append("';\n\n");
		builder.append("const API_URL = environment.apiURL;\n");
		
		// GET ENDPOINT
		RequestMapping p = clazz.getAnnotation(RequestMapping.class);
		String endpoint = p.value()[0];
		for(Method method : clazz.getMethods()) {
			GetMapping g = method.getAnnotation(GetMapping.class);
			if(g != null) {
				endpoint += g.value()[0];
				
			}
		}
		builder.append("const ENDPOINT = '");
		builder.append(endpoint);
		builder.append("';\n\n");
		builder.append("@Injectable()\n");
		builder.append("export class ");
		builder.append(simpleClassName);
		builder.append("Service");
		builder.append(" {\n\n");
		builder.append("\tconstructor(private http: HttpClient) { }\n\n");
		// HANDLE ERROR
		builder.append("\tprivate handleError (response: any) { \n");
		builder.append("\t\treturn throwError(response.error.body);\n");
		builder.append("\t}\n\n");
		
		// FIND ALL
		builder.append("\tpublic findAll(params: any): Observable<");
		builder.append(simpleClassName);
		builder.append("[]> {\n");
		builder.append("\t\treturn this.http.get(`${API_URL}${ENDPOINT}`, {\n");
		builder.append("\t\t\tparams: params\n");
		builder.append("\t\t})\n");
		builder.append("\t\t.pipe(\n");
		builder.append("\t\t\tmap((response : Response | any) => {\n");
		builder.append("\t\t\t\treturn response.map((model) => new ");
		builder.append(simpleClassName);
		builder.append("(model));\n");
		builder.append("\t\t\t}),\n");
		builder.append("\t\t\tcatchError(this.handleError)\n");
		builder.append("\t\t);\n");
		builder.append("\t}\n");
		
		builder.append("}");
		
		return builder.toString();
	}
	
	private static Map<String, Class<?>> getClassList() throws ClassNotFoundException {
		Map<String, Class<?>> allClasses = new HashMap<String, Class<?>>();
		final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(
				false);
		provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));
		final Set<BeanDefinition> classes = provider
				.findCandidateComponents(servicesPackage);
		for (BeanDefinition bean : classes) {
			Class<?> clazz = Class.forName(bean.getBeanClassName());
			allClasses.put(clazz.getSimpleName(), clazz);
		}
		return allClasses;
	}
}
