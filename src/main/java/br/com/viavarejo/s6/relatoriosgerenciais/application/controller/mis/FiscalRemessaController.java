package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.mis;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/mis")
public class FiscalRemessaController {
	
	@Autowired
	@Qualifier("FiscalRemessa")
  	private StoredProcedureRepository repository;

	@GetMapping("/fiscal-remessa")
    public ResponseEntity<?> listar(
    		@RequestParam(name="dataInicio") String dataInicio,
 		   	@RequestParam(name="dataFim") String dataFim,
 		    @RequestParam(name="filiais") String filiais,
 		    @RequestParam(name="situacoes") String situacoes,
 		    @RequestParam(name="empresa") Integer empresa,
 		    @RequestParam(name="cnpj") Long cnpj) {
		
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("dataInicio", dataInicio);
		params.put("dataFim", dataFim);
		params.put("filiais", filiais);
		params.put("situacoes", situacoes);
		params.put("empresa", empresa);
		params.put("cnpj", cnpj);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.FISCAL_REMESSA, params);
    }
}