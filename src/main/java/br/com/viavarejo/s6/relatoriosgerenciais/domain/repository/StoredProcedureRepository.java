package br.com.viavarejo.s6.relatoriosgerenciais.domain.repository;

import java.util.List;
import java.util.Map;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.RepositoryException;

public interface StoredProcedureRepository {

	public List<?> findAll(StoredProcedures storedProcedure, Map<String, Object> parameters) throws RepositoryException;
	public List<?> findAll(StoredProcedures storedProcedure) throws RepositoryException;
}