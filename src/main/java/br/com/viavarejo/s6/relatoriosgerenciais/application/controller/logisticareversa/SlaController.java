package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/logistica-reversa")
public class SlaController {

	@Autowired
	@Qualifier("Sla")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/sla")
	public ResponseEntity<?> listar(
    		@RequestParam(name="dataInicio") String dataInicio,
    		@RequestParam(name="dataFim") String dataFim,
    		@RequestParam(name="origOrdFrete") String origOrdFrete,
    		@RequestParam(name="situacoes") String situacoes,
    		@RequestParam(name="ocorrencias") String ocorrencias) {
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
 		params.put("dataInicio", dataInicio);
 		params.put("dataFim", dataFim);
 		params.put("origOrdFrete", origOrdFrete);
 		params.put("situacoes", situacoes);
 		params.put("paramBranco", "");
 		params.put("ocorrencias", ocorrencias);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.SLA, params);
    }
}