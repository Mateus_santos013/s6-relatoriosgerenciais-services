package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTAuthenticationFilter;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTAuthenticationProvider;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTLoginFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JWTAuthenticationProvider authenticationProvider;
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable()
		.cors().and()
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, "/auth/login").permitAll()
		.antMatchers("/ping").permitAll()
		.antMatchers(
				"/v2/api-docs", 
				"/configuration/ui", 
				"/swagger-resources", 
				"/configuration/security", 
				"/swagger-ui.html", 
				"/webjars/**").permitAll()
		.antMatchers("/mis/**").hasRole("A140")
		.antMatchers("/logistica-reversa/**").hasRole("S875")
		.antMatchers("/crl/**").hasRole("S876")
		.antMatchers("/saq/**").hasRole("S877")
		.anyRequest().authenticated()
		.and()
		.addFilterBefore(
				new JWTLoginFilter("/auth/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(
				new JWTAuthenticationFilter(),	UsernamePasswordAuthenticationFilter.class);
	}
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}

}