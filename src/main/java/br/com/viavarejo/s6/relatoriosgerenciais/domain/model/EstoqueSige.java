package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EstoqueSige extends BaseModel<EstoqueSige> {

	private static final long serialVersionUID = 6478341731265166349L;

	private int idItem;
	private String nome;
	private String itegSituacao;
	private String depto;
	private int codSetor;
	private String setor;
	private int codFamilia;
	private String familia;
	private int codSub;
	private String sub;
	private int chaveFilial;
	private String tipDep;
	private int qtFisica;
	private int qtRomaneada;
	private int qtReservada;
	private int qtAlocada;
	private int qtSaldo;
	private BigDecimal m3;
	private BigDecimal m3Total;
	private BigDecimal valUnitario;
	private Integer idFornecedor;
	private String fornNome;
	private String fornApelido;

	public int getIdItem() {
		return idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getItegSituacao() {
		return itegSituacao;
	}

	public void setItegSituacao(String itegSituacao) {
		this.itegSituacao = itegSituacao;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public int getCodSetor() {
		return codSetor;
	}

	public void setCodSetor(int codSetor) {
		this.codSetor = codSetor;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public int getCodFamilia() {
		return codFamilia;
	}

	public void setCodFamilia(int codFamilia) {
		this.codFamilia = codFamilia;
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public int getCodSub() {
		return codSub;
	}

	public void setCodSub(int codSub) {
		this.codSub = codSub;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public int getChaveFilial() {
		return chaveFilial;
	}

	public void setChaveFilial(int chaveFilial) {
		this.chaveFilial = chaveFilial;
	}

	public String getTipDep() {
		return tipDep;
	}

	public void setTipDep(String tipDep) {
		this.tipDep = tipDep;
	}

	public int getQtFisica() {
		return qtFisica;
	}

	public void setQtFisica(int qtFisica) {
		this.qtFisica = qtFisica;
	}

	public int getQtRomaneada() {
		return qtRomaneada;
	}

	public void setQtRomaneada(int qtRomaneada) {
		this.qtRomaneada = qtRomaneada;
	}

	public int getQtReservada() {
		return qtReservada;
	}

	public void setQtReservada(int qtReservada) {
		this.qtReservada = qtReservada;
	}

	public int getQtAlocada() {
		return qtAlocada;
	}

	public void setQtAlocada(int qtAlocada) {
		this.qtAlocada = qtAlocada;
	}

	public int getQtSaldo() {
		return qtSaldo;
	}

	public void setQtSaldo(int qtSaldo) {
		this.qtSaldo = qtSaldo;
	}

	public BigDecimal getM3() {
		return m3;
	}

	public void setM3(BigDecimal m3) {
		this.m3 = m3;
	}

	public BigDecimal getM3Total() {
		return m3Total;
	}

	public void setM3Total(BigDecimal m3Total) {
		this.m3Total = m3Total;
	}

	public BigDecimal getValUnitario() {
		return valUnitario;
	}

	public void setValUnitario(BigDecimal valUnitario) {
		this.valUnitario = valUnitario;
	}

	public Integer getIdFornecedor() {
		return idFornecedor;
	}

	public void setIdFornecedor(Integer idFornecedor) {
		this.idFornecedor = idFornecedor;
	}

	public String getFornNome() {
		return fornNome;
	}

	public void setFornNome(String fornNome) {
		this.fornNome = fornNome;
	}

	public String getFornApelido() {
		return fornApelido;
	}

	public void setFornApelido(String fornApelido) {
		this.fornApelido = fornApelido;
	}

	@Override
	public EstoqueSige map(ResultSet rs) throws SQLException {
		EstoqueSige model = new EstoqueSige();
		model.setIdItem(rs.getInt("ID_ITEM"));
		model.setNome(rs.getString("NOME").trim());
		model.setItegSituacao(rs.getString("ITEG_SITUACAO").trim());
		model.setDepto(rs.getString("DEPTO").trim());
		model.setSetor(rs.getString("SETOR").trim());
		model.setFamilia(rs.getString("FAMILIA").trim());
		model.setSub(rs.getString("SUB").trim());
		model.setChaveFilial(rs.getInt("CHAVE_FILIAL"));
		model.setTipDep(rs.getString("TIPDEP").trim());
		model.setQtFisica(rs.getInt("QT_FISICA"));
		model.setQtSaldo(rs.getInt("QT_SALDO"));
		model.setValUnitario(rs.getBigDecimal("VL_UNITARIO"));
		model.setIdFornecedor(rs.getInt("ID_FORNECEDOR"));
		model.setFornNome(rs.getString("FORN_NOME"));
		model.setFornApelido(rs.getString("FORN_APELIDO"));
		return model;
	}
}