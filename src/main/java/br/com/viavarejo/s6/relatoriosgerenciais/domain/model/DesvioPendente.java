package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DesvioPendente extends BaseModel<DesvioPendente> {

	private static final long serialVersionUID = -3506712463275020293L;

	private String std;
	private Integer documento;
	private Integer empresaTransportadora;
	private Integer transportadoraOrigem;
	private String filial;
	private Date dataInclusao;
	private String nomeTransportadora;
	private Integer totalDesvios;
	private Integer id;
	private String serieNota;
	private Integer numNota;
	
	public String getStd() {
		return std;
	}

	public void setStd(String std) {
		this.std = std;
	}

	public Integer getDocumento() {
		return documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public Integer getEmpresaTransportadora() {
		return empresaTransportadora;
	}

	public void setEmpresaTransportadora(Integer empresaTransportadora) {
		this.empresaTransportadora = empresaTransportadora;
	}

	public Integer getTransportadoraOrigem() {
		return transportadoraOrigem;
	}

	public void setTransportadoraOrigem(Integer transportadoraOrigem) {
		this.transportadoraOrigem = transportadoraOrigem;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public String getNomeTransportadora() {
		return nomeTransportadora;
	}

	public void setNomeTransportadora(String nomeTransportadora) {
		this.nomeTransportadora = nomeTransportadora;
	}

	public Integer getTotalDesvios() {
		return totalDesvios;
	}

	public void setTotalDesvios(Integer totalDesvios) {
		this.totalDesvios = totalDesvios;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerieNota() {
		return serieNota;
	}

	public void setSerieNota(String serieNota) {
		this.serieNota = serieNota;
	}

	public Integer getNumNota() {
		return numNota;
	}

	public void setNumNota(Integer numNota) {
		this.numNota = numNota;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public DesvioPendente map(ResultSet rs) throws SQLException {
		DesvioPendente model = new DesvioPendente();
		model.setDocumento(rs.getInt("DOCUMENTO"));
		model.setEmpresaTransportadora(rs.getInt("EMPRESA_TRANSPORTADORA"));
		model.setTransportadoraOrigem(rs.getInt("TRANSPORTADORA_ORIGEM"));
		model.setFilial(StringUtils.trim(rs.getString("FILIAL").trim()));
		model.setDataInclusao(rs.getDate("DATA_INCLUSAO"));
		model.setNomeTransportadora(StringUtils.trim(rs.getString("NOME_TRANSPORTADORA").trim()));
		model.setTotalDesvios(hasColumn(rs, "TOTAL_DESVIOS") ? rs.getInt("TOTAL_DESVIOS") : 0);
		model.setSerieNota(StringUtils.trim(rs.getString("SERIE_NOTA")));
		model.setNumNota(rs.getInt("NUMERO_NOTA"));
		
		
		return model;
	}
}