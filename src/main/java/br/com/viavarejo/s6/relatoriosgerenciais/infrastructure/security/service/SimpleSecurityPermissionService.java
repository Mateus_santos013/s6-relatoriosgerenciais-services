package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.service;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class SimpleSecurityPermissionService {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${db2.schema}")
	private String SCHEMA;
	
	@Value("${auth.sistemas}")
	private String[] APPLICATION_SYSTEMS;
	
	private List<SimpleGrantedAuthority> getRoles(Integer codigoEmpresa, Integer codigoFuncionario, String codigoSistema) throws SQLException {
		return jdbcTemplate.query("{ CALL " + SCHEMA + ".J2S039( ?, ?, ?, ) }",
				new Object[] { 
						StringUtils.leftPad(String.valueOf(codigoEmpresa), 2, "0"),
						StringUtils.leftPad(String.valueOf(codigoFuncionario), 8, "0"),
						codigoSistema 
				},
				new RowMapper<SimpleGrantedAuthority>() {
				public SimpleGrantedAuthority mapRow(ResultSet rs, int rowNum) throws SQLException {
	                String sistema 		= rs.getString("CD_SIS_TRN");
	                String transacao 	= rs.getString("CD_TRN");
	                return new SimpleGrantedAuthority(String.format("ROLE_%s%s", sistema, transacao));
	            }
		});
	}
	
	public List<SimpleGrantedAuthority> getRoles(Integer codigoEmpresa, Integer codigoFuncionario) throws SQLException {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		for(String sistema : APPLICATION_SYSTEMS) {
			List<SimpleGrantedAuthority> roles = getRoles(codigoEmpresa, codigoFuncionario, sistema);
			authorities.addAll(roles);
		}
		return authorities;
	}
}