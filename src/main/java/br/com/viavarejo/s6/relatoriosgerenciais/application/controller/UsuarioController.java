package br.com.viavarejo.s6.relatoriosgerenciais.application.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityUser;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.service.JWTTokenService;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.service.SimpleSecurityPermissionService;
import io.jsonwebtoken.Claims;

@RestController
public class UsuarioController {


	
	@Autowired
	private SimpleSecurityPermissionService service;
	

	@GetMapping("/user")
	public ResponseEntity<SecurityUser> getUser(@RequestHeader("authorization") String token) throws JsonProcessingException, IOException, SQLException {
		Claims claims = JWTTokenService.getClaims(token);
		
		
		String nome = claims.get("nome").toString();
		int empresa = Integer.parseInt(claims.get("empresa").toString());
		int matricula = Integer.parseInt(claims.get("matricula").toString());
		int filial = Integer.parseInt(claims.get("filial").toString());
		Date expiration = new Date(Long.parseLong(claims.get("expirationTime").toString()));
		SecurityUser user = new SecurityUser();
		user.setNome(nome);
		user.setEmpresa(empresa); 
		user.setMatricula(matricula);
		user.setFilial(filial);
		user.setExpirationTime(expiration);
			
		List<SimpleGrantedAuthority> authorities = service.getRoles(empresa, matricula);
		
		user.setPermissoes(authorities);
		
		return new ResponseEntity<SecurityUser>(user, HttpStatus.OK);
	}
}