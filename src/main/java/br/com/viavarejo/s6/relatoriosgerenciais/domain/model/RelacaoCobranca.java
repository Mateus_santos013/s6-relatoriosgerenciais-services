package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class RelacaoCobranca extends BaseModel<RelacaoCobranca> {

	private static final long serialVersionUID = 911235366984263078L;

	private String status;
	private String codRet;
	private String msgRet;
	private Integer codEmp;
	private Integer filial;
	private String razao;
	private Integer nfRemessa;
	private String serieNf;
	private String dataEmissao;
	private String natOp;
	private Integer nfFaturamento;
	private String serieFaturamento;
	private String dataEmiFat;
	private String garantia;
	private Integer codOrcamento;
	private Integer seqOrcamento;
	private String aprovacao;
	private String tipo;
	private String dtAprovOrc;
	private String situSatex;
	private BigDecimal custoMerc;
	private BigDecimal valorOrc;
	private String devSconserto;
	private BigDecimal valPagamento;
	private BigDecimal percentual;
	private Integer numPmr;
	private String stPrm;
	private Integer regSaida;
	private Integer preNota;
	private String stRegistro;
	private String linha;
	private String categoria;
	private String marca;
	private String respValidacao;
	private String respAprov;
	private Integer sku;
	private Integer ptn;
	private String descMercadoria;
	private String conserto;
	private String local;
	private String statusTsm;
	private String sala;
	private String responsabilidade;
	private String dataEntradaDat;
	private String posicaoAtual;
	private String dataPosicao;
	private Integer diasPosicao;
	private String rangeDiasPosi;
	private Integer filialOrigem;
	private Integer numPosto;
	private Integer regRemessa;
	private Integer diasPosto;
	private String rangeDiasPosto;
	private Integer nfRetorno;
	private String serieNfRet;
	private String dataNfRet;
	private String dataCadastro;
	private Integer codFornecedor;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCodRet() {
		return codRet;
	}

	public void setCodRet(String codRet) {
		this.codRet = codRet;
	}

	public String getMsgRet() {
		return msgRet;
	}

	public void setMsgRet(String msgRet) {
		this.msgRet = msgRet;
	}

	public Integer getCodEmp() {
		return codEmp;
	}

	public void setCodEmp(Integer codEmp) {
		this.codEmp = codEmp;
	}

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public String getRazao() {
		return razao;
	}

	public void setRazao(String razao) {
		this.razao = razao;
	}

	public Integer getNfRemessa() {
		return nfRemessa;
	}

	public void setNfRemessa(Integer nfRemessa) {
		this.nfRemessa = nfRemessa;
	}

	public String getSerieNf() {
		return serieNf;
	}

	public void setSerieNf(String serieNf) {
		this.serieNf = serieNf;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getNatOp() {
		return natOp;
	}

	public void setNatOp(String natOp) {
		this.natOp = natOp;
	}

	public Integer getNfFaturamento() {
		return nfFaturamento;
	}

	public void setNfFaturamento(Integer nfFaturamento) {
		this.nfFaturamento = nfFaturamento;
	}

	public String getSerieFaturamento() {
		return serieFaturamento;
	}

	public void setSerieFaturamento(String serieFaturamento) {
		this.serieFaturamento = serieFaturamento;
	}

	public String getDataEmiFat() {
		return dataEmiFat;
	}

	public void setDataEmiFat(String dataEmiFat) {
		this.dataEmiFat = dataEmiFat;
	}

	public String getGarantia() {
		return garantia;
	}

	public void setGarantia(String garantia) {
		this.garantia = garantia;
	}

	public Integer getCodOrcamento() {
		return codOrcamento;
	}

	public void setCodOrcamento(Integer codOrcamento) {
		this.codOrcamento = codOrcamento;
	}

	public Integer getSeqOrcamento() {
		return seqOrcamento;
	}

	public void setSeqOrcamento(Integer seqOrcamento) {
		this.seqOrcamento = seqOrcamento;
	}

	public String getAprovacao() {
		return aprovacao;
	}

	public void setAprovacao(String aprovacao) {
		this.aprovacao = aprovacao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDtAprovOrc() {
		return dtAprovOrc;
	}

	public void setDtAprovOrc(String dtAprovOrc) {
		this.dtAprovOrc = dtAprovOrc;
	}

	public String getSituSatex() {
		return situSatex;
	}

	public void setSituSatex(String situSatex) {
		this.situSatex = situSatex;
	}

	public BigDecimal getCustoMerc() {
		return custoMerc;
	}

	public void setCustoMerc(BigDecimal custoMerc) {
		this.custoMerc = custoMerc;
	}

	public BigDecimal getValorOrc() {
		return valorOrc;
	}

	public void setValorOrc(BigDecimal valorOrc) {
		this.valorOrc = valorOrc;
	}

	public String getDevSconserto() {
		return devSconserto;
	}

	public void setDevSconserto(String devSconserto) {
		this.devSconserto = devSconserto;
	}

	public BigDecimal getValPagamento() {
		return valPagamento;
	}

	public void setValPagamento(BigDecimal valPagamento) {
		this.valPagamento = valPagamento;
	}

	public BigDecimal getPercentual() {
		return percentual;
	}

	public void setPercentual(BigDecimal percentual) {
		this.percentual = percentual;
	}

	public Integer getNumPmr() {
		return numPmr;
	}

	public void setNumPmr(Integer numPmr) {
		this.numPmr = numPmr;
	}

	public String getStPrm() {
		return stPrm;
	}

	public void setStPrm(String stPrm) {
		this.stPrm = stPrm;
	}

	public Integer getRegSaida() {
		return regSaida;
	}

	public void setRegSaida(Integer regSaida) {
		this.regSaida = regSaida;
	}

	public Integer getPreNota() {
		return preNota;
	}

	public void setPreNota(Integer preNota) {
		this.preNota = preNota;
	}

	public String getStRegistro() {
		return stRegistro;
	}

	public void setStRegistro(String stRegistro) {
		this.stRegistro = stRegistro;
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getRespValidacao() {
		return respValidacao;
	}

	public void setRespValidacao(String respValidacao) {
		this.respValidacao = respValidacao;
	}

	public String getRespAprov() {
		return respAprov;
	}

	public void setRespAprov(String respAprov) {
		this.respAprov = respAprov;
	}

	public Integer getSku() {
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	public Integer getPtn() {
		return ptn;
	}

	public void setPtn(Integer ptn) {
		this.ptn = ptn;
	}

	public String getDescMercadoria() {
		return descMercadoria;
	}

	public void setDescMercadoria(String descMercadoria) {
		this.descMercadoria = descMercadoria;
	}

	public String getConserto() {
		return conserto;
	}

	public void setConserto(String conserto) {
		this.conserto = conserto;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getStatusTsm() {
		return statusTsm;
	}

	public void setStatusTsm(String statusTsm) {
		this.statusTsm = statusTsm;
	}

	public String getSala() {
		return sala;
	}

	public void setSala(String sala) {
		this.sala = sala;
	}

	public String getResponsabilidade() {
		return responsabilidade;
	}

	public void setResponsabilidade(String responsabilidade) {
		this.responsabilidade = responsabilidade;
	}

	public String getDataEntradaDat() {
		return dataEntradaDat;
	}

	public void setDataEntradaDat(String dataEntradaDat) {
		this.dataEntradaDat = dataEntradaDat;
	}

	public String getPosicaoAtual() {
		return posicaoAtual;
	}

	public void setPosicaoAtual(String posicaoAtual) {
		this.posicaoAtual = posicaoAtual;
	}

	public String getDataPosicao() {
		return dataPosicao;
	}

	public void setDataPosicao(String dataPosicao) {
		this.dataPosicao = dataPosicao;
	}

	public Integer getDiasPosicao() {
		return diasPosicao;
	}

	public void setDiasPosicao(Integer diasPosicao) {
		this.diasPosicao = diasPosicao;
	}

	public String getRangeDiasPosi() {
		return rangeDiasPosi;
	}

	public void setRangeDiasPosi(String rangeDiasPosi) {
		this.rangeDiasPosi = rangeDiasPosi;
	}

	public Integer getFilialOrigem() {
		return filialOrigem;
	}

	public void setFilialOrigem(Integer filialOrigem) {
		this.filialOrigem = filialOrigem;
	}

	public Integer getNumPosto() {
		return numPosto;
	}

	public void setNumPosto(Integer numPosto) {
		this.numPosto = numPosto;
	}

	public Integer getRegRemessa() {
		return regRemessa;
	}

	public void setRegRemessa(Integer regRemessa) {
		this.regRemessa = regRemessa;
	}

	public Integer getDiasPosto() {
		return diasPosto;
	}

	public void setDiasPosto(Integer diasPosto) {
		this.diasPosto = diasPosto;
	}

	public String getRangeDiasPosto() {
		return rangeDiasPosto;
	}

	public void setRangeDiasPosto(String rangeDiasPosto) {
		this.rangeDiasPosto = rangeDiasPosto;
	}

	public Integer getNfRetorno() {
		return nfRetorno;
	}

	public void setNfRetorno(Integer nfRetorno) {
		this.nfRetorno = nfRetorno;
	}

	public String getSerieNfRet() {
		return serieNfRet;
	}

	public void setSerieNfRet(String serieNfRet) {
		this.serieNfRet = serieNfRet;
	}

	public String getDataNfRet() {
		return dataNfRet;
	}

	public void setDataNfRet(String dataNfRet) {
		this.dataNfRet = dataNfRet;
	}

	public String getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Integer getCodFornecedor() {
		return codFornecedor;
	}

	public void setCodFornecedor(Integer codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	@Override
	public RelacaoCobranca map(ResultSet rs) throws SQLException {
		RelacaoCobranca model = new RelacaoCobranca();
		
		model.setStatus(StringUtils.trim(rs.getString("L1_ST_RET")));
		model.setCodRet(StringUtils.trim(rs.getString("L1_CD_RET")));
		model.setMsgRet(StringUtils.trim(rs.getString("L1_DS_MSG")));
		model.setCodEmp(rs.getInt("L1_CD_EMPGCB"));
		model.setFilial(rs.getInt("L1_CD_FIL"));
		model.setRazao(StringUtils.trim(rs.getString("L1_NM_EPSRV_RZS")));
		model.setNfRemessa(rs.getInt("L1_CD_NFS"));
		model.setSerieNf(StringUtils.trim(rs.getString("L1_CD_NFS_SER")));
		model.setDataEmissao(StringUtils.trim("L1_DT_OENFI_NFI_EMI"));
		model.setNatOp(StringUtils.trim(rs.getString("L1_NAT_OPER")));
		model.setNfFaturamento(rs.getInt("L1_CD_NFI"));;
		model.setSerieFaturamento(StringUtils.trim(rs.getString("L1_CD_NFI_SER")));
		model.setDataEmiFat(StringUtils.trim(rs.getString("L1_DT_OENFI_NFI_EMIF")));
		model.setGarantia(StringUtils.trim(rs.getString("L1_ST_OCSAST_GFA")));
		model.setCodOrcamento(rs.getInt("L1_CD_OCSAST"));
		model.setSeqOrcamento(rs.getInt("L1_CD_ORCAST"));
		model.setAprovacao(StringUtils.trim(rs.getString("L1_DS_ORCAST")));
		model.setTipo(StringUtils.trim(rs.getString("L1_DS_ORCAST_TIP")));
		model.setDtAprovOrc(StringUtils.trim(rs.getString("L1_DT_APROVACAO")));
		model.setSituSatex(StringUtils.trim(rs.getString("L1_DS_OAEXT")));
		model.setCustoMerc(rs.getBigDecimal("L1_VR_ITMPMR_UNT"));
		model.setValorOrc(rs.getBigDecimal("L1_VR_ORCAST_TOT"));
		model.setDevSconserto(StringUtils.trim(rs.getString("L1_ST_OCSAST_TIP_MCR")));
		model.setValPagamento(rs.getBigDecimal("L1_VR_PAGAMENTO"));
		model.setPercentual(rs.getBigDecimal("L1_PERCENTUAL"));
		model.setNumPmr(rs.getInt("L1_CD_PMR"));
		model.setStPrm(StringUtils.trim(rs.getString("L1_ST_PMR")));
		model.setRegSaida(rs.getInt("L1_CD_RSAST"));
		model.setPreNota(rs.getInt("L1_CD_PMAST"));
		model.setStRegistro(StringUtils.trim(rs.getString("L1_ST_NPSAST_CSI")));
		model.setLinha(StringUtils.trim(rs.getString("L1_LINHA")));
		model.setCategoria(StringUtils.trim(rs.getString("L1_NM_SETMCR")));
		model.setMarca(StringUtils.trim(rs.getString("L1_NM_MRCMCR")));
		model.setRespValidacao(StringUtils.trim(rs.getString("L1_VALIDACAO")));
		model.setRespAprov(StringUtils.trim(rs.getString("L1_APROVACAO")));
		model.setSku(rs.getInt("L1_CD_MCR"));
		model.setPtn(rs.getInt("L1_CD_OCSAST_BOS"));
		model.setDescMercadoria(StringUtils.trim(rs.getString("L1_DS_MCR")));
		model.setConserto(StringUtils.trim(rs.getString("L1_DS_OCSAST_ORI_RMS")));
		model.setLocal(StringUtils.trim(rs.getString("L1_LOC")));
		model.setStatusTsm(StringUtils.trim(rs.getString("L1_DS_TSMCR")));
		model.setSala(StringUtils.trim(rs.getString("L1_CD_EPSPTO_SLA")));
		model.setResponsabilidade(StringUtils.trim(rs.getString("L1_ST_LCAST_ORI_DFE")));
		model.setDataEntradaDat(StringUtils.trim(rs.getString("L1_DT_POAST")));
		model.setPosicaoAtual(StringUtils.trim(rs.getString("L1_DS_TPOAST")));
		model.setDataPosicao(StringUtils.trim(rs.getString("L1_DT_POSICAO_ATUAL")));
		model.setDiasPosicao(rs.getInt("L1_DIAS"));
		model.setRangeDiasPosi(StringUtils.trim(rs.getString("L1_RANGI")));
		model.setFilialOrigem(rs.getInt("L1_CD_FIL_ORI"));
		model.setNumPosto(rs.getInt("L1_CD_EPSRV"));
		model.setRegRemessa(rs.getInt("L1_CD_RRAST"));
		model.setDiasPosto(rs.getInt("L1_DIAS_POSTO"));
		model.setRangeDiasPosto(StringUtils.trim(rs.getString("L1_RANGI_POSTO")));
		model.setNfRetorno(rs.getInt("L1_CD_NFE"));
		model.setSerieNfRet(StringUtils.trim(rs.getString("L1_CD_NFE_SER")));
		model.setDataNfRet(StringUtils.trim(rs.getString("L1_DT_NFMCR_CAD")));
		model.setDataCadastro(StringUtils.trim(rs.getString("L1_DT_OCSAST")));
		model.setCodFornecedor(rs.getInt("L1_CD_FNE"));
		
		
		return model;
	}

}
