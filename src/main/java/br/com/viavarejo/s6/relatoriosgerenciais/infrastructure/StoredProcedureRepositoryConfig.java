package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.RegexPatternTypeFilter;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.repository.impl.StoredProcedureRepositoryImpl;

@Configuration
public class StoredProcedureRepositoryConfig implements BeanDefinitionRegistryPostProcessor {
	
	private void createStoredProcedureRepositoryBeans(BeanDefinitionRegistry registry) throws ClassNotFoundException {
		Map<String, Class<?>> allClass = getClassList();

		for (String key : allClass.keySet()) {
			GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
			beanDefinition.setBeanClass(StoredProcedureRepositoryImpl.class);
			beanDefinition.setAutowireCandidate(true);
			ConstructorArgumentValues constructor = new ConstructorArgumentValues();
			constructor.addGenericArgumentValue(allClass.get(key));
			beanDefinition.setConstructorArgumentValues(constructor);
			registry.registerBeanDefinition(key, beanDefinition);
		}
	}

	private Map<String, Class<?>> getClassList() throws ClassNotFoundException {
		Map<String, Class<?>> allClasses = new HashMap<String, Class<?>>();
		final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));
		
		final Set<BeanDefinition> classes = provider.findCandidateComponents("br.com.viavarejo.s6.relatoriosgerenciais.domain.model");
		
		for (BeanDefinition bean : classes) {
			Class<?> clazz = Class.forName(bean.getBeanClassName());
			allClasses.put(clazz.getSimpleName(), clazz);
		}
		
		return allClasses;
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		try {
			createStoredProcedureRepositoryBeans(registry);
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}