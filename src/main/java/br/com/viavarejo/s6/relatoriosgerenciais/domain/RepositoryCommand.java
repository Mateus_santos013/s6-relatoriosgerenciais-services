package br.com.viavarejo.s6.relatoriosgerenciais.domain;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.RepositoryException;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.ResponseError;

public class RepositoryCommand {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	private StoredProcedureRepository repository;
	
	public RepositoryCommand(StoredProcedureRepository repository) {
		this.repository = repository;
	}
	
	public ResponseEntity<?> execute(StoredProcedures procedure) {
		return this.execute(procedure, null);
	}
	
	public ResponseEntity<?> execute(StoredProcedures procedure, Map<String, Object> parametros) {
		try {
			final List<?> registros = parametros == null
				? repository.findAll(procedure)
				: repository.findAll(procedure, parametros);
			
	    	return new ResponseEntity<List<?>>(registros, HttpStatus.OK);
		}
		catch (RepositoryException e) {
			LOGGER.error(e.getMessage());
			final ResponseError response = new ResponseError(e.getMessage(), 500);
			return new ResponseEntity<ResponseError>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}