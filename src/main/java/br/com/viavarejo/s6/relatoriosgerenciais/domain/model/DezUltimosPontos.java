package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class DezUltimosPontos extends BaseModel<DezUltimosPontos> {

	private static final long serialVersionUID = 1L;

	private String numEntrega;
	private String pedidoOff;
	private String dsm;
	private String std;
	private String tipo;
	private String pedido;
	private String seqPedido;
	private String empresa;
	private String filial;
	private String status;
	private String ultimaCol;
	private String codOcorr;
	private String descOcorr;
	private String dataOcorr;
	private String dataProc;
	private String codOcorrDois;
	private String descOcorrDois;
	private String dataOcorrDois;
	private String dataProcDois;
	private String codOcorrTres;
	private String descOcorrTres;
	private String dataOcorrTres;
	private String dataProcTres;
	private String codOcorrQuatro;
	private String descOcorrQuatro;
	private String dataOcorrQuatro;
	private String dataProcQuatro;
	private String codOcorrCinco;
	private String descOcorrCinco;
	private String dataOcorrCinco;
	private String dataProcCinco;
	private String codOcorrSeis;
	private String descOcorrSeis;
	private String dataOcorrSeis;
	private String dataProcSeis;
	private String codOcorrSete;
	private String descOcorrSete;
	private String dataOcorrSete;
	private String dataProcSete;
	private String codOcorrOito;
	private String descOcorrOito;
	private String dataOcorrOito;
	private String dataProcOito;
	private String codOcorrNove;
	private String descOcorrNove;
	private String dataOcorrNove;
	private String dataProcNove;
	private String codOcorrDez;
	private String descOcorrDez;
	private String dataOcorrDez;
	private String dataProcDez;

	public String getNumEntrega() {
		return numEntrega;
	}

	public void setNumEntrega(String numEntrega) {
		this.numEntrega = numEntrega;
	}

	public String getPedidoOff() {
		return pedidoOff;
	}

	public void setPedidoOff(String pedidoOff) {
		this.pedidoOff = pedidoOff;
	}

	public String getDsm() {
		return dsm;
	}

	public void setDsm(String dsm) {
		this.dsm = dsm;
	}

	public String getStd() {
		return std;
	}

	public void setStd(String std) {
		this.std = std;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPedido() {
		return pedido;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public String getSeqPedido() {
		return seqPedido;
	}

	public void setSeqPedido(String seqPedido) {
		this.seqPedido = seqPedido;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUltimaCol() {
		return ultimaCol;
	}

	public void setUltimaCol(String ultimaCol) {
		this.ultimaCol = ultimaCol;
	}

	public String getCodOcorr() {
		return codOcorr;
	}

	public void setCodOcorr(String codOcorr) {
		this.codOcorr = codOcorr;
	}

	public String getDescOcorr() {
		return descOcorr;
	}

	public void setDescOcorr(String descOcorr) {
		this.descOcorr = descOcorr;
	}

	public String getDataOcorr() {
		return dataOcorr;
	}

	public void setDataOcorr(String dataOcorr) {
		this.dataOcorr = dataOcorr;
	}

	public String getDataProc() {
		return dataProc;
	}

	public void setDataProc(String dataProc) {
		this.dataProc = dataProc;
	}

	public String getCodOcorrDois() {
		return codOcorrDois;
	}

	public void setCodOcorrDois(String codOcorrDois) {
		this.codOcorrDois = codOcorrDois;
	}

	public String getDescOcorrDois() {
		return descOcorrDois;
	}

	public void setDescOcorrDois(String descOcorrDois) {
		this.descOcorrDois = descOcorrDois;
	}

	public String getDataOcorrDois() {
		return dataOcorrDois;
	}

	public void setDataOcorrDois(String dataOcorrDois) {
		this.dataOcorrDois = dataOcorrDois;
	}

	public String getDataProcDois() {
		return dataProcDois;
	}

	public void setDataProcDois(String dataProcDois) {
		this.dataProcDois = dataProcDois;
	}

	public String getCodOcorrTres() {
		return codOcorrTres;
	}

	public void setCodOcorrTres(String codOcorrTres) {
		this.codOcorrTres = codOcorrTres;
	}

	public String getDescOcorrTres() {
		return descOcorrTres;
	}

	public void setDescOcorrTres(String descOcorrTres) {
		this.descOcorrTres = descOcorrTres;
	}

	public String getDataOcorrTres() {
		return dataOcorrTres;
	}

	public void setDataOcorrTres(String dataOcorrTres) {
		this.dataOcorrTres = dataOcorrTres;
	}

	public String getDataProcTres() {
		return dataProcTres;
	}

	public void setDataProcTres(String dataProcTres) {
		this.dataProcTres = dataProcTres;
	}

	public String getCodOcorrQuatro() {
		return codOcorrQuatro;
	}

	public void setCodOcorrQuatro(String codOcorrQuatro) {
		this.codOcorrQuatro = codOcorrQuatro;
	}

	public String getDescOcorrQuatro() {
		return descOcorrQuatro;
	}

	public void setDescOcorrQuatro(String descOcorrQuatro) {
		this.descOcorrQuatro = descOcorrQuatro;
	}

	public String getDataOcorrQuatro() {
		return dataOcorrQuatro;
	}

	public void setDataOcorrQuatro(String dataOcorrQuatro) {
		this.dataOcorrQuatro = dataOcorrQuatro;
	}

	public String getDataProcQuatro() {
		return dataProcQuatro;
	}

	public void setDataProcQuatro(String dataProcQuatro) {
		this.dataProcQuatro = dataProcQuatro;
	}

	public String getCodOcorrCinco() {
		return codOcorrCinco;
	}

	public void setCodOcorrCinco(String codOcorrCinco) {
		this.codOcorrCinco = codOcorrCinco;
	}

	public String getDescOcorrCinco() {
		return descOcorrCinco;
	}

	public void setDescOcorrCinco(String descOcorrCinco) {
		this.descOcorrCinco = descOcorrCinco;
	}

	public String getDataOcorrCinco() {
		return dataOcorrCinco;
	}

	public void setDataOcorrCinco(String dataOcorrCinco) {
		this.dataOcorrCinco = dataOcorrCinco;
	}

	public String getDataProcCinco() {
		return dataProcCinco;
	}

	public void setDataProcCinco(String dataProcCinco) {
		this.dataProcCinco = dataProcCinco;
	}

	public String getCodOcorrSeis() {
		return codOcorrSeis;
	}

	public void setCodOcorrSeis(String codOcorrSeis) {
		this.codOcorrSeis = codOcorrSeis;
	}

	public String getDescOcorrSeis() {
		return descOcorrSeis;
	}

	public void setDescOcorrSeis(String descOcorrSeis) {
		this.descOcorrSeis = descOcorrSeis;
	}

	public String getDataOcorrSeis() {
		return dataOcorrSeis;
	}

	public void setDataOcorrSeis(String dataOcorrSeis) {
		this.dataOcorrSeis = dataOcorrSeis;
	}

	public String getDataProcSeis() {
		return dataProcSeis;
	}

	public void setDataProcSeis(String dataProcSeis) {
		this.dataProcSeis = dataProcSeis;
	}

	public String getCodOcorrSete() {
		return codOcorrSete;
	}

	public void setCodOcorrSete(String codOcorrSete) {
		this.codOcorrSete = codOcorrSete;
	}

	public String getDescOcorrSete() {
		return descOcorrSete;
	}

	public void setDescOcorrSete(String descOcorrSete) {
		this.descOcorrSete = descOcorrSete;
	}

	public String getDataOcorrSete() {
		return dataOcorrSete;
	}

	public void setDataOcorrSete(String dataOcorrSete) {
		this.dataOcorrSete = dataOcorrSete;
	}

	public String getDataProcSete() {
		return dataProcSete;
	}

	public void setDataProcSete(String dataProcSete) {
		this.dataProcSete = dataProcSete;
	}

	public String getCodOcorrOito() {
		return codOcorrOito;
	}

	public void setCodOcorrOito(String codOcorrOito) {
		this.codOcorrOito = codOcorrOito;
	}

	public String getDescOcorrOito() {
		return descOcorrOito;
	}

	public void setDescOcorrOito(String descOcorrOito) {
		this.descOcorrOito = descOcorrOito;
	}

	public String getDataOcorrOito() {
		return dataOcorrOito;
	}

	public void setDataOcorrOito(String dataOcorrOito) {
		this.dataOcorrOito = dataOcorrOito;
	}

	public String getDataProcOito() {
		return dataProcOito;
	}

	public void setDataProcOito(String dataProcOito) {
		this.dataProcOito = dataProcOito;
	}

	public String getCodOcorrNove() {
		return codOcorrNove;
	}

	public void setCodOcorrNove(String codOcorrNove) {
		this.codOcorrNove = codOcorrNove;
	}

	public String getDescOcorrNove() {
		return descOcorrNove;
	}

	public void setDescOcorrNove(String descOcorrNove) {
		this.descOcorrNove = descOcorrNove;
	}

	public String getDataOcorrNove() {
		return dataOcorrNove;
	}

	public void setDataOcorrNove(String dataOcorrNove) {
		this.dataOcorrNove = dataOcorrNove;
	}

	public String getDataProcNove() {
		return dataProcNove;
	}

	public void setDataProcNove(String dataProcNove) {
		this.dataProcNove = dataProcNove;
	}

	public String getCodOcorrDez() {
		return codOcorrDez;
	}

	public void setCodOcorrDez(String codOcorrDez) {
		this.codOcorrDez = codOcorrDez;
	}

	public String getDescOcorrDez() {
		return descOcorrDez;
	}

	public void setDescOcorrDez(String descOcorrDez) {
		this.descOcorrDez = descOcorrDez;
	}

	public String getDataOcorrDez() {
		return dataOcorrDez;
	}

	public void setDataOcorrDez(String dataOcorrDez) {
		this.dataOcorrDez = dataOcorrDez;
	}

	public String getDataProcDez() {
		return dataProcDez;
	}

	public void setDataProcDez(String dataProcDez) {
		this.dataProcDez = dataProcDez;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public DezUltimosPontos map(ResultSet rs) throws SQLException {
		DezUltimosPontos model = new DezUltimosPontos();
		model.setNumEntrega(StringUtils.trim(rs.getString("ENTREGA")));
		model.setPedidoOff(StringUtils.trim(rs.getString("PED_CD")));
		model.setDsm(StringUtils.trim(rs.getString("PED_DSM")));
		model.setStd(StringUtils.trim(rs.getString("STD")));
		model.setTipo(StringUtils.trim(rs.getString("TIPO")));
		model.setPedido(StringUtils.trim(rs.getString("PED_ON")));
		model.setSeqPedido(StringUtils.trim(rs.getString("PED_ONSEQ")));
		model.setEmpresa(StringUtils.trim(rs.getString("EMPRESA")));
		model.setFilial(StringUtils.trim(rs.getString("FILIAL")));
		model.setStatus(StringUtils.trim(rs.getString("STATUS1")));
		model.setCodOcorr(StringUtils.trim(rs.getString("CDOCORR1")));
		model.setDescOcorr(StringUtils.trim(rs.getString("DSOCORR1")));
		model.setDataOcorr(StringUtils.trim(rs.getString("DTOCORR1")));
		model.setDataProc(StringUtils.trim(rs.getString("DTPROCES1")));
		model.setCodOcorrDois(StringUtils.trim(rs.getString("CDOCORR2")));
		model.setDescOcorrDois(StringUtils.trim(rs.getString("DSOCORR2")));
		model.setDataOcorrDois(StringUtils.trim(rs.getString("DTOCORR2")));
		model.setDataProcDois(StringUtils.trim(rs.getString("DTPROCES2")));
		model.setCodOcorrTres(StringUtils.trim(rs.getString("CDOCORR3")));
		model.setDescOcorrTres(StringUtils.trim(rs.getString("DSOCORR3")));
		model.setDataOcorrTres(StringUtils.trim(rs.getString("DTOCORR3")));
		model.setDataProcTres(StringUtils.trim(rs.getString("DTPROCES3")));
		model.setCodOcorrQuatro(StringUtils.trim(rs.getString("CDOCORR4")));
		model.setDescOcorrQuatro(StringUtils.trim(rs.getString("DSOCORR4")));
		model.setDataOcorrQuatro(StringUtils.trim(rs.getString("DTOCORR4")));
		model.setDataProcQuatro(StringUtils.trim(rs.getString("DTPROCES4")));
		model.setCodOcorrCinco(StringUtils.trim(rs.getString("CDOCORR5")));
		model.setDescOcorrCinco(StringUtils.trim(rs.getString("DSOCORR5")));
		model.setDataOcorrCinco(StringUtils.trim(rs.getString("DTOCORR5")));
		model.setDataProcCinco(StringUtils.trim(rs.getString("DTPROCES5")));
		model.setCodOcorrSeis(StringUtils.trim(rs.getString("CDOCORR6")));
		model.setDescOcorrSeis(StringUtils.trim(rs.getString("DSOCORR6")));
		model.setDataOcorrSeis(StringUtils.trim(rs.getString("DTOCORR6")));
		model.setDataProcSeis(StringUtils.trim(rs.getString("DTPROCES6")));
		model.setCodOcorrSete(StringUtils.trim(rs.getString("CDOCORR7")));
		model.setDescOcorrSete(StringUtils.trim(rs.getString("DSOCORR7")));
		model.setDataOcorrSete(StringUtils.trim(rs.getString("DTOCORR7")));
		model.setDataProcSete(StringUtils.trim(rs.getString("DTPROCES7")));
		model.setCodOcorrOito(StringUtils.trim(rs.getString("CDOCORR8")));
		model.setDescOcorrOito(StringUtils.trim(rs.getString("DSOCORR8")));
		model.setDataOcorrOito(StringUtils.trim(rs.getString("DTOCORR8")));
		model.setDataProcOito(StringUtils.trim(rs.getString("DTPROCES8")));
		model.setCodOcorrNove(StringUtils.trim(rs.getString("CDOCORR9")));
		model.setDescOcorrNove(StringUtils.trim(rs.getString("DSOCORR9")));
		model.setDataOcorrNove(StringUtils.trim(rs.getString("DTOCORR9")));
		model.setDataProcNove(StringUtils.trim(rs.getString("DTPROCES9")));
		model.setCodOcorrDez(StringUtils.trim(rs.getString("CDOCORR10")));
		model.setDescOcorrDez(StringUtils.trim(rs.getString("DSOCORR10")));
		model.setDataOcorrDez(StringUtils.trim(rs.getString("DTOCORR10")));
		model.setDataProcDez(StringUtils.trim(rs.getString("DTPROCES10")));
		return model;
	}
}