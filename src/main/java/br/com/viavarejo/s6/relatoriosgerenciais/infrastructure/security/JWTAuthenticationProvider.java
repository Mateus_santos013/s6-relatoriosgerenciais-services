package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityCredentials;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityUser;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.service.SimpleSecurityPermissionService;

@Component
public class JWTAuthenticationProvider implements AuthenticationProvider {

	@Value("${auth.url}")
	private String SERVICE_URL;
	
	@Value("${auth.app}")
	private String APPLICATION_SYSTEM;
	
	@Value("${auth.sistemas}")
	private String[] APPLICATION_SYSTEMS;
	
	@Value("${auth.transacao}")
	private String APPLICATION_TRANSACTION;
	
	@Autowired
	private SimpleSecurityPermissionService service;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		SecurityCredentials credentials = (SecurityCredentials) authentication.getPrincipal();
		credentials.setSistema(APPLICATION_SYSTEM);
		credentials.setTransacao(APPLICATION_TRANSACTION);

		
		ObjectMapper mapper = new ObjectMapper();
		RestTemplate client = new RestTemplate();	
		
		SecurityUser securityUser = null;
		
		try {
			securityUser = client.postForEntity(SERVICE_URL, mapper.writeValueAsString(credentials), SecurityUser.class).getBody();
		} 
		catch (RestClientException | JsonProcessingException e) {
			e.printStackTrace();
		}
		
		if(securityUser == null) {
			throw new BadCredentialsException("Usuário e/ou senha inválidos!");
		}
		
		credentials.setFilial(securityUser.getFilial());
		credentials.setNome(securityUser.getNome());
		credentials.setUsername(String.valueOf(credentials.getEmpresa()) + String.valueOf(credentials.getMatricula()));
		List<SimpleGrantedAuthority> authorities = null;
		try {			
			authorities = service.getRoles(credentials.getEmpresa(), credentials.getMatricula());
		} 
		catch (SQLException e) {
			throw new BadCredentialsException("Erro ao recuperar permissões do usuário!");	
		}
		
		return new UsernamePasswordAuthenticationToken(credentials, credentials.getSenha(), authorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}