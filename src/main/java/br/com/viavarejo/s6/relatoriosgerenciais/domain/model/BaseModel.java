package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public abstract class BaseModel<TModel> implements RowMapper<TModel>, Serializable {

	private static final long serialVersionUID = -4088161999928874820L;
	public abstract TModel map(ResultSet rs) throws SQLException;
	private boolean isValid = false;

	@Override
	public TModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		if(!isValid) verifyErrors(rs);
		return map(rs);
	}

	protected boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		
		for (int x = 1; x <= columns; x++) {
			if (columnName.equals(rsmd.getColumnLabel(x))) {
				return true;
			}
		}
		
		return false;
	}
	
	private void verifyErrors(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData(); 
		
		if(rsmd.getColumnName(1).equals("ID") && rsmd.getColumnName(2).equals("COD")) {
			if(!rs.getString("COD").equals("00")) {
				throw new SQLException(rs.getString("MSG"));
			}
		}
		
		this.isValid = true;
	}
}