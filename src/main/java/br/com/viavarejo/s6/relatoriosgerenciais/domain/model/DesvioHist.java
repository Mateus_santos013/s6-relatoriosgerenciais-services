
package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DesvioHist extends BaseModel<DesvioHist> {

	private static final long serialVersionUID = 195873230631267155L;
	
	
	private Integer id;
	private String std;
	private Integer documento;
	private String status;
	private Date dataInclusao;
	private Date dataRetorno;
	private Long pedidoEntrega;
	private Integer contrOrigem;
	private Integer empresaTransportadora;
	private Integer fornecTransportOrigem;
	private Integer transportadoraOrigem;
	private Integer contrDesviado;
	private Integer empTransportDesviada;
	private Integer fornecTransportDesviada;
	private String transpDesv;
	private Integer notaOrigem;
	private String serieOrigem;
	
	private String filial;
	private String nomeTransportadora;
	private Integer totalDesvios;
	private Integer empTransOrigem;
	private String transpOrigem;
	private String serieDesvio;
	private Integer notaDesvio;

	public Integer getEmpresaTransportadora() {
		return empresaTransportadora;
	}

	public void setEmpresaTransportadora(Integer empresaTransportadora) {
		this.empresaTransportadora = empresaTransportadora;
	}

	public Integer getTransportadoraOrigem() {
		return transportadoraOrigem;
	}

	public void setTransportadoraOrigem(Integer transportadoraOrigem) {
		this.transportadoraOrigem = transportadoraOrigem;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public String getNomeTransportadora() {
		return nomeTransportadora;
	}

	public void setNomeTransportadora(String nomeTransportadora) {
		this.nomeTransportadora = nomeTransportadora;
	}

	public Integer getTotalDesvios() {
		return totalDesvios;
	}

	public void setTotalDesvios(Integer totalDesvios) {
		this.totalDesvios = totalDesvios;
	}

	public Integer getEmpTransOrigem() {
		return empTransOrigem;
	}

	public void setEmpTransOrigem(Integer empTransOrigem) {
		this.empTransOrigem = empTransOrigem;
	}

	public Integer getFornecTransportOrigem() {
		return fornecTransportOrigem;
	}

	public void setFornecTransportOrigem(Integer fornecTransportOrigem) {
		this.fornecTransportOrigem = fornecTransportOrigem;
	}

	public Integer getContrOrigem() {
		return contrOrigem;
	}

	public void setContrOrigem(Integer contrOrigem) {
		this.contrOrigem = contrOrigem;
	}

	public Integer getEmpTransportDesviada() {
		return empTransportDesviada;
	}

	public void setEmpTransportDesviada(Integer empTransportDesviada) {
		this.empTransportDesviada = empTransportDesviada;
	}

	public Integer getFornecTransportDesviada() {
		return fornecTransportDesviada;
	}

	public void setFornecTransportDesviada(Integer fornecTransportDesviada) {
		this.fornecTransportDesviada = fornecTransportDesviada;
	}

	public Integer getContrDesviado() {
		return contrDesviado;
	}

	public void setContrDesviado(Integer contrDesviado) {
		this.contrDesviado = contrDesviado;
	}

	public String getTranspOrigem() {
		return transpOrigem;
	}

	public void setTranspOrigem(String transpOrigem) {
		this.transpOrigem = transpOrigem;
	}

	public String getTranspDesv() {
		return transpDesv;
	}

	public void setTranspDesv(String transpDesv) {
		this.transpDesv = transpDesv;
	}

	public Long getPedidoEntrega() {
		return pedidoEntrega;
	}

	public void setPedidoEntrega(Long pedidoEntrega) {
		this.pedidoEntrega = pedidoEntrega;
	}

	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDocumento() {
		return documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getStd() {
		return std;
	}

	public void setStd(String std) {
		this.std = std;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNotaOrigem() {
		return notaOrigem;
	}

	public void setNotaOrigem(Integer notaOrigem) {
		this.notaOrigem = notaOrigem;
	}

	public String getSerieOrigem() {
		return serieOrigem;
	}

	public void setSerieOrigem(String serieOrigem) {
		this.serieOrigem = serieOrigem;
	}

	public String getSerieDesvio() {
		return serieDesvio;
	}

	public void setSerieDesvio(String serieDesvio) {
		this.serieDesvio = serieDesvio;
	}

	public Integer getNotaDesvio() {
		return notaDesvio;
	}

	public void setNotaDesvio(Integer notaDesvio) {
		this.notaDesvio = notaDesvio;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public DesvioHist map(ResultSet rs) throws SQLException {
		DesvioHist model = new DesvioHist();
		
		if (hasColumn(rs, "STATUS")) {
			model.setStatus(StringUtils.trim(rs.getString("STATUS")));
			model.setDocumento(rs.getInt("DOCUMENTO"));
			model.setDataRetorno(rs.getDate("DATA_RETORNO"));
			model.setDataInclusao(rs.getDate("DATA_INCLUSAO"));
			model.setPedidoEntrega(rs.getLong("PEDIDO_ENTREGA"));

			model.setTranspOrigem(StringUtils.trim(rs.getString("TRANSPORTADORA_ORIGEM")));
			model.setTranspDesv(StringUtils.trim(rs.getString("TRANSPORTADORA_DESVIADA")));

			model.setEmpTransOrigem(rs.getInt("EMPRESA_TRANSPORT_ORIGEM"));
			model.setFornecTransportOrigem(rs.getInt("FORNECEDOR_TRANSPORT_ORIGEM"));
			model.setContrOrigem(rs.getInt("CONTRATO_ORIGEM"));

			model.setEmpTransportDesviada(rs.getInt("EMPRESA_TRANSPORT_DESVIADA"));
			model.setFornecTransportDesviada(rs.getInt("FORNECEDOR_TRANSPORT_DESVIADA"));
			model.setContrDesviado(rs.getInt("CONTRATO_DESVIADO"));

			model.setFilial(StringUtils.trim(rs.getString("FILIAL")));
			model.setSerieOrigem(StringUtils.trim(rs.getString("SERIE_ORIGEM")));
			model.setNotaOrigem(rs.getInt("NOTA_ORIGEM"));
			model.setSerieDesvio(StringUtils.trim(rs.getString("SERIE_DESVIO")));
			model.setNotaDesvio(rs.getInt("NOTA_DESVIO"));
		}

		return model;
	}

	@Override
	public String toString() {
		return "DesvioHist [std=" + std + ", documento=" + documento + ", empresaTransportadora="
				+ empresaTransportadora + ", transportadoraOrigem=" + transportadoraOrigem + ", filial=" + filial
				+ ", dataInclusao=" + dataInclusao + ", nomeTransportadora=" + nomeTransportadora + ", totalDesvios="
				+ totalDesvios + ", id=" + id + ", dataRetorno=" + dataRetorno + ", status=" + status
				+ ", pedidoEntrega=" + pedidoEntrega + ", empTransOrigem=" + empTransOrigem + ", fornecTransportOrigem="
				+ fornecTransportOrigem + ", contrOrigem=" + contrOrigem + ", empTransportDesviada="
				+ empTransportDesviada + ", fornecTransportDesviada=" + fornecTransportDesviada + ", contrDesviado="
				+ contrDesviado + ", transpOrigem=" + transpOrigem + ", transpDesv=" + transpDesv + "]";
	}
}