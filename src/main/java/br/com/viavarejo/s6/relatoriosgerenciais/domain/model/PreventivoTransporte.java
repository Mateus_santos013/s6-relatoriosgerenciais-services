package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class PreventivoTransporte extends BaseModel<PreventivoTransporte> {

	private static final long serialVersionUID = -4001620260496850637L;

	private Date solicColeta;
	private Long pedido;
	private Date dataPedido;
	private Date aprovPed;
	private Integer estabelec;
	private Integer cnpj1;
	private Integer cnpj2;
	private Integer cnpj3;
	private String unidNeg;
	private Integer ordDev;
	private Date datOrdDev;
	private Long entregaVenda;
	private Long entregaDevol;
	private Long entregaTroca;
	private String meioPagto;
	private String tipoEntrega;
	private String origemOrdFrete;
	private String categoria;
	private String assunto;
	private String motivoColeta;
	private Date dataCancel;
	private String forcada;
	private Date dataForcada;
	private String liberado;
	private String situacaoAtend;
	private Date dataColetaProm;
	private String statusColeta;
	private Date dataStatusColeta;
	private String statusDevol;
	private Date dataStatusDevol;
	private Date dataRetPromet;
	private String transportEntrega;
	private String transportColeta;
	private String apelidoTransportColeta;
	private String cnpjTransportColeta;
	private String cnpjTransportSufixo;
	private String cnpjTransportDigito;
	private String clienteColeta;
	private BigDecimal cpfCliente;
	private Integer cpfClienteDig;
	private Integer cep;
	private String endereco;
	private BigDecimal numero;
	private String complemento;
	private String bairro;
	private String referencia;
	private String cidade;
	private String uf;
	private String ddd;
	private String telefone;
	private String ddd2;
	private String celular2;
	private String ddd3;
	private String telefone2;
	private Integer item;
	private String descriItem;
	private String departamento;
	private String setor;
	private String fabricante;
	private Integer qtdeItem;
	private BigDecimal peso;
	private BigDecimal cubagem;
	private Integer refFiscal;
	private Date dataEmissaoNfSaida;
	private Integer notaFiscalSaida;
	private String serieSaida;
	private Integer nfEntrada;
	private String serieEntrada;
	private BigDecimal vlProduto;
	private BigDecimal vlTotalNf;
	private BigDecimal valorFreteSite;
	private BigDecimal valorTotItem;
	private Integer contrato;
	private String descriContrato;
	private String etiqueta;
	private String ocorrencia;
	private Date dataOcorrencia;
	private String pendDevolucao;
	private String pendenteColeta;
	private String chaveDanfe;
	private String statusNf;
	private Date dtOcorrencia;
	private Date dtInsercaoReg;
	private String etiqTransport;
	private String statusDoc;

	public Date getSolicColeta() {
		return solicColeta;
	}

	public void setSolicColeta(Date solicColeta) {
		this.solicColeta = solicColeta;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Date getAprovPed() {
		return aprovPed;
	}

	public void setAprovPed(Date aprovPed) {
		this.aprovPed = aprovPed;
	}

	public String getUnidNeg() {
		return unidNeg;
	}

	public void setUnidNeg(String unidNeg) {
		this.unidNeg = unidNeg;
	}

	public Integer getOrdDev() {
		return ordDev;
	}

	public void setOrdDev(Integer ordDev) {
		this.ordDev = ordDev;
	}

	public Date getDatOrdDev() {
		return datOrdDev;
	}

	public void setDatOrdDev(Date datOrdDev) {
		this.datOrdDev = datOrdDev;
	}

	public Long getEntregaVenda() {
		return entregaVenda;
	}

	public void setEntregaVenda(Long entregaVenda) {
		this.entregaVenda = entregaVenda;
	}

	public Long getEntregaDevol() {
		return entregaDevol;
	}

	public void setEntregaDevol(Long entregaDevol) {
		this.entregaDevol = entregaDevol;
	}

	public Long getEntregaTroca() {
		return entregaTroca;
	}

	public void setEntregaTroca(Long entregaTroca) {
		this.entregaTroca = entregaTroca;
	}

	public String getMeioPagto() {
		return meioPagto;
	}

	public void setMeioPagto(String meioPagto) {
		this.meioPagto = meioPagto;
	}

	public String getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public String getOrigemOrdFrete() {
		return origemOrdFrete;
	}

	public void setOrigemOrdFrete(String origemOrdFrete) {
		this.origemOrdFrete = origemOrdFrete;
	}

	public String getMotivoColeta() {
		return motivoColeta;
	}

	public void setMotivoColeta(String motivoColeta) {
		this.motivoColeta = motivoColeta;
	}

	public Date getDataCancel() {
		return dataCancel;
	}

	public void setDataCancel(Date dataCancel) {
		this.dataCancel = dataCancel;
	}

	public Date getDataForcada() {
		return dataForcada;
	}

	public void setDataForcada(Date dataForcada) {
		this.dataForcada = dataForcada;
	}

	public String getSituacaoAtend() {
		return situacaoAtend;
	}

	public void setSituacaoAtend(String situacaoAtend) {
		this.situacaoAtend = situacaoAtend;
	}

	public Date getDataColetaProm() {
		return dataColetaProm;
	}

	public void setDataColetaProm(Date dataColetaProm) {
		this.dataColetaProm = dataColetaProm;
	}

	public String getStatusColeta() {
		return statusColeta;
	}

	public void setStatusColeta(String statusColeta) {
		this.statusColeta = statusColeta;
	}

	public Date getDataStatusColeta() {
		return dataStatusColeta;
	}

	public void setDataStatusColeta(Date dataStatusColeta) {
		this.dataStatusColeta = dataStatusColeta;
	}

	public String getStatusDevol() {
		return statusDevol;
	}

	public void setStatusDevol(String statusDevol) {
		this.statusDevol = statusDevol;
	}

	public Date getDataStatusDevol() {
		return dataStatusDevol;
	}

	public void setDataStatusDevol(Date dataStatusDevol) {
		this.dataStatusDevol = dataStatusDevol;
	}

	public Date getDataRetPromet() {
		return dataRetPromet;
	}

	public void setDataRetPromet(Date dataRetPromet) {
		this.dataRetPromet = dataRetPromet;
	}

	public String getTransportEntrega() {
		return transportEntrega;
	}

	public void setTransportEntrega(String transportEntrega) {
		this.transportEntrega = transportEntrega;
	}

	public String getTransportColeta() {
		return transportColeta;
	}

	public void setTransportColeta(String transportColeta) {
		this.transportColeta = transportColeta;
	}

	public String getApelidoTransportColeta() {
		return apelidoTransportColeta;
	}

	public void setApelidoTransportColeta(String apelidoTransportColeta) {
		this.apelidoTransportColeta = apelidoTransportColeta;
	}

	public String getCnpjTransportColeta() {
		return cnpjTransportColeta;
	}

	public void setCnpjTransportColeta(String cnpjTransportColeta) {
		this.cnpjTransportColeta = cnpjTransportColeta;
	}

	public String getCnpjTransportSufixo() {
		return cnpjTransportSufixo;
	}

	public void setCnpjTransportSufixo(String cnpjTransportSufixo) {
		this.cnpjTransportSufixo = cnpjTransportSufixo;
	}

	public String getCnpjTransportDigito() {
		return cnpjTransportDigito;
	}

	public void setCnpjTransportDigito(String cnpjTransportDigito) {
		this.cnpjTransportDigito = cnpjTransportDigito;
	}

	public String getClienteColeta() {
		return clienteColeta;
	}

	public void setClienteColeta(String clienteColeta) {
		this.clienteColeta = clienteColeta;
	}

	public BigDecimal getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(BigDecimal cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public Integer getCpfClienteDig() {
		return cpfClienteDig;
	}

	public void setCpfClienteDig(Integer cpfClienteDig) {
		this.cpfClienteDig = cpfClienteDig;
	}

	public String getDescriItem() {
		return descriItem;
	}

	public void setDescriItem(String descriItem) {
		this.descriItem = descriItem;
	}

	public Integer getQtdeItem() {
		return qtdeItem;
	}

	public void setQtdeItem(Integer qtdeItem) {
		this.qtdeItem = qtdeItem;
	}

	public Integer getRefFiscal() {
		return refFiscal;
	}

	public void setRefFiscal(Integer refFiscal) {
		this.refFiscal = refFiscal;
	}

	public Date getDataEmissaoNfSaida() {
		return dataEmissaoNfSaida;
	}

	public void setDataEmissaoNfSaida(Date dataEmissaoNfSaida) {
		this.dataEmissaoNfSaida = dataEmissaoNfSaida;
	}

	public Integer getNotaFiscalSaida() {
		return notaFiscalSaida;
	}

	public void setNotaFiscalSaida(Integer notaFiscalSaida) {
		this.notaFiscalSaida = notaFiscalSaida;
	}

	public String getSerieSaida() {
		return serieSaida;
	}

	public void setSerieSaida(String serieSaida) {
		this.serieSaida = serieSaida;
	}

	public Integer getNfEntrada() {
		return nfEntrada;
	}

	public void setNfEntrada(Integer nfEntrada) {
		this.nfEntrada = nfEntrada;
	}

	public String getSerieEntrada() {
		return serieEntrada;
	}

	public void setSerieEntrada(String serieEntrada) {
		this.serieEntrada = serieEntrada;
	}

	public BigDecimal getVlProduto() {
		return vlProduto;
	}

	public void setVlProduto(BigDecimal vlProduto) {
		this.vlProduto = vlProduto;
	}

	public BigDecimal getVlTotalNf() {
		return vlTotalNf;
	}

	public void setVlTotalNf(BigDecimal vlTotalNf) {
		this.vlTotalNf = vlTotalNf;
	}

	public BigDecimal getValorFreteSite() {
		return valorFreteSite;
	}

	public void setValorFreteSite(BigDecimal valorFreteSite) {
		this.valorFreteSite = valorFreteSite;
	}

	public BigDecimal getValorTotItem() {
		return valorTotItem;
	}

	public void setValorTotItem(BigDecimal valorTotItem) {
		this.valorTotItem = valorTotItem;
	}

	public String getDescriContrato() {
		return descriContrato;
	}

	public void setDescriContrato(String descriContrato) {
		this.descriContrato = descriContrato;
	}

	public Date getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(Date dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public String getPendDevolucao() {
		return pendDevolucao;
	}

	public void setPendDevolucao(String pendDevolucao) {
		this.pendDevolucao = pendDevolucao;
	}

	public String getPendenteColeta() {
		return pendenteColeta;
	}

	public void setPendenteColeta(String pendenteColeta) {
		this.pendenteColeta = pendenteColeta;
	}

	public String getChaveDanfe() {
		return chaveDanfe;
	}

	public void setChaveDanfe(String chaveDanfe) {
		this.chaveDanfe = chaveDanfe;
	}

	public String getStatusNf() {
		return statusNf;
	}

	public void setStatusNf(String statusNf) {
		this.statusNf = statusNf;
	}

	public Date getDtOcorrencia() {
		return dtOcorrencia;
	}

	public void setDtOcorrencia(Date dtOcorrencia) {
		this.dtOcorrencia = dtOcorrencia;
	}

	public Date getDtInsercaoReg() {
		return dtInsercaoReg;
	}

	public void setDtInsercaoReg(Date dtInsercaoReg) {
		this.dtInsercaoReg = dtInsercaoReg;
	}

	public String getEtiqTransport() {
		return etiqTransport;
	}

	public void setEtiqTransport(String etiqTransport) {
		this.etiqTransport = etiqTransport;
	}

	public String getStatusDoc() {
		return statusDoc;
	}

	public void setStatusDoc(String statusDoc) {
		this.statusDoc = statusDoc;
	}

	public Long getPedido() {
		return pedido;
	}

	public void setPedido(Long pedido) {
		this.pedido = pedido;
	}

	public Integer getEstabelec() {
		return estabelec;
	}

	public void setEstabelec(Integer estabelec) {
		this.estabelec = estabelec;
	}

	public Integer getCnpj1() {
		return cnpj1;
	}

	public void setCnpj1(Integer cnpj1) {
		this.cnpj1 = cnpj1;
	}

	public Integer getCnpj2() {
		return cnpj2;
	}

	public void setCnpj2(Integer cnpj2) {
		this.cnpj2 = cnpj2;
	}

	public Integer getCnpj3() {
		return cnpj3;
	}

	public void setCnpj3(Integer cnpj3) {
		this.cnpj3 = cnpj3;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getForcada() {
		return forcada;
	}

	public void setForcada(String forcada) {
		this.forcada = forcada;
	}

	public String getLiberado() {
		return liberado;
	}

	public void setLiberado(String liberado) {
		this.liberado = liberado;
	}

	public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public BigDecimal getNumero() {
		return numero;
	}

	public void setNumero(BigDecimal numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getDdd2() {
		return ddd2;
	}

	public void setDdd2(String ddd2) {
		this.ddd2 = ddd2;
	}

	public String getCelular2() {
		return celular2;
	}

	public void setCelular2(String celular2) {
		this.celular2 = celular2;
	}

	public String getDdd3() {
		return ddd3;
	}

	public void setDdd3(String ddd3) {
		this.ddd3 = ddd3;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getCubagem() {
		return cubagem;
	}

	public void setCubagem(BigDecimal cubagem) {
		this.cubagem = cubagem;
	}

	public Integer getContrato() {
		return contrato;
	}

	public void setContrato(Integer contrato) {
		this.contrato = contrato;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	@Override
	public PreventivoTransporte map(ResultSet rs) throws SQLException {
		PreventivoTransporte model = new PreventivoTransporte();
		model.setSolicColeta(rs.getDate("SOLIC_COLETA"));
		model.setPedido(rs.getLong("PEDIDO"));
		model.setDataPedido(rs.getDate("DATA_PEDIDO"));
		model.setAprovPed(rs.getDate("APROV_PED"));
		model.setEstabelec(rs.getInt("ESTABELEC"));
		model.setCnpj1(rs.getInt("CNPJ1"));
		model.setCnpj2(rs.getInt("CNPJ2"));
		model.setCnpj3(rs.getInt("CNPJ3"));
		model.setUnidNeg(StringUtils.trim(rs.getString("UNID_NEG")));
		model.setOrdDev(rs.getInt("ORD_DEV"));
		model.setDatOrdDev(rs.getDate("DAT_ORD_DEV"));
		model.setEntregaVenda(rs.getLong("ENTREGA_VENDA"));
		model.setEntregaDevol(rs.getLong("ENTREGA_DEVOL"));
		model.setEntregaTroca(rs.getLong("ENTREGA_TROCA"));
		model.setMeioPagto(StringUtils.trim(rs.getString("MEIO_PAGTO")));
		model.setTipoEntrega(StringUtils.trim(rs.getString("TIPO_DE_ENTREGA")));
		model.setOrigemOrdFrete(StringUtils.trim(rs.getString("ORIGEM_ORD_FRETE")));
		model.setCategoria(StringUtils.trim(rs.getString("CATEGORIA")));
		model.setAssunto(StringUtils.trim(rs.getString("ASSUNTO")));
		model.setMotivoColeta(StringUtils.trim(rs.getString("MOTIVO_COLETA")));
		model.setDataCancel(rs.getDate("DATA_CANCEL"));
		model.setForcada(StringUtils.trim(rs.getString("FORCADO")));
		model.setDataForcada(rs.getDate("DATA_FORCADO"));
		model.setLiberado(StringUtils.trim(rs.getString("LIBERADO")));
		model.setSituacaoAtend(StringUtils.trim(rs.getString("SITUACAO_ATENDIMENTO")));
		model.setDataColetaProm(rs.getDate("DATA_COLETA_PROM"));
		//model.setStatus_coleta(StringUtils.trim(rs.getString("STATUS_COLETA"));
		//model.setData_status_coleta(rs.getDate("DATA_STATUS_COLETA"));
		//model.setStatus_devol(StringUtils.trim(rs.getString("STATUS_DEVOL"));
		model.setDataStatusDevol(rs.getDate("DATA_STATUS_DEVOL"));
		model.setDataRetPromet(rs.getDate("DATA_RET_PROMET"));
		model.setTransportEntrega(StringUtils.trim(rs.getString("TRANSPORT_ENTREGA")));
		model.setTransportColeta(StringUtils.trim(rs.getString("TRANSPORT_COLETA")));
		model.setApelidoTransportColeta(StringUtils.trim(rs.getString("APELIDO_TRANSPORT_COLETA")));
		model.setCnpjTransportColeta(StringUtils.trim(rs.getString("CNPJ_TRANSPORT_COLETA")));
		model.setCnpjTransportSufixo(StringUtils.trim(rs.getString("CNPJ_TRANSPORT_SUFIXO")));
		model.setCnpjTransportDigito(StringUtils.trim(rs.getString("CNPJ_TRANSPORT_DIGITO")));
		model.setClienteColeta(StringUtils.trim(rs.getString("CLIENTE_COLETA")));
		model.setCpfCliente(rs.getBigDecimal("CPF_CLIENTE"));
		model.setCpfClienteDig(rs.getInt("CPF_CLIENTE_DIG"));
		model.setCep(rs.getInt("CEP"));
		model.setEndereco(StringUtils.trim(rs.getString("ENDERECO")));
		model.setNumero(rs.getBigDecimal("NUMERO"));
		model.setComplemento(StringUtils.trim(rs.getString("COMPLEMENTO")));
		model.setBairro(StringUtils.trim(rs.getString("BAIRRO")));
		model.setReferencia(StringUtils.trim(rs.getString("REFERENCIA")));
		model.setCidade(StringUtils.trim(rs.getString("CIDADE")));
		model.setUf(StringUtils.trim(rs.getString("UF")));
		model.setDdd(StringUtils.trim(rs.getString("DDD1")));
		model.setTelefone(StringUtils.trim(rs.getString("TELEFONE1")));
		model.setDdd2(StringUtils.trim(rs.getString("DDD2")));
		model.setCelular2(StringUtils.trim(rs.getString("CELULAR2")));
		model.setDdd3(StringUtils.trim(rs.getString("DDD3")));
		model.setTelefone2(StringUtils.trim(rs.getString("TELEFONE3")));
		model.setItem(rs.getInt("ITEM"));
		model.setDescriItem(StringUtils.trim(rs.getString("DESCRI_ITEM")));
		model.setDepartamento(StringUtils.trim(rs.getString("DEPARTAMENTO")));
		model.setSetor(StringUtils.trim(rs.getString("SETOR")));
		model.setFabricante(StringUtils.trim(rs.getString("FABRICANTE")));
		model.setQtdeItem(rs.getInt("QTDE_ITEM"));
		model.setPeso(rs.getBigDecimal("PESO"));
		model.setCubagem(rs.getBigDecimal("CUBAGEM"));
		model.setRefFiscal(rs.getInt("REF_FISCAL"));
		model.setDataEmissaoNfSaida(rs.getDate("DATA_EMISSAO_NF_ENT"));
		model.setNotaFiscalSaida(rs.getInt("NOTA_FISCAL_SAIDA"));
		model.setSerieSaida(StringUtils.trim(rs.getString("SERIE_SAIDA")));
		model.setNfEntrada(rs.getInt("NF_ENTRADA"));
		model.setSerieEntrada(StringUtils.trim(rs.getString("SERIE_ENTRADA")));
		model.setVlProduto(rs.getBigDecimal("VL_PRODUTO"));
		model.setVlTotalNf(rs.getBigDecimal("VL_TOTAL_NF"));
		model.setValorFreteSite(rs.getBigDecimal("VALOR_FRETE_SITE"));
		model.setValorTotItem(rs.getBigDecimal("VL_TOTAL_ITEM"));
		model.setContrato(rs.getInt("CONTRATO"));
		model.setDescriContrato(StringUtils.trim(rs.getString("DESCRICAO_CONTRATO")));
		model.setEtiqueta(StringUtils.trim(rs.getString("ETIQUETA")));
		model.setOcorrencia(StringUtils.trim(rs.getString("OCORRENCIA")));
		model.setDataOcorrencia(rs.getDate("DATA_OCORRENCIA"));
		model.setPendDevolucao(StringUtils.trim(rs.getString("PEND_DEVOLUCAO")));
		model.setPendenteColeta(StringUtils.trim(rs.getString("PENDENTE_COLETA")));
		model.setChaveDanfe(StringUtils.trim(rs.getString("CHAVE_DANFE")));
		model.setStatusNf(StringUtils.trim(rs.getString("STATUS_NF")));
		//model.setDt_ocorrencia(rs.getDate("DT_OCORRENCIA"));
		model.setDtInsercaoReg(rs.getDate("DT_INSERCAO_REG"));
		model.setEtiqTransport(StringUtils.trim(rs.getString("ETIQ_TRANSPORT")));
		model.setStatusDoc(StringUtils.trim(rs.getString("STATUS_DOCUMENTO")));
		return model;
	}
}