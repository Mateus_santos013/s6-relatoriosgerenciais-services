package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class AgendamentoInsucesso extends BaseModel<AgendamentoInsucesso> {

	private static final long serialVersionUID = -4006471240503296836L;

	private Integer filial;
	private String transportadora;
	private Long entrega;
	private Integer nota;
	private String serie;
	private Integer notaVenda;
	private String serieVenda;
	private String nomeCliente;
	private Integer item;
	private String descricaoItem;
	private Integer qtdItem;
	private BigDecimal valorItem;
	private BigDecimal freteItem;
	private BigDecimal descontoItem;

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public String getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}

	public Long getEntrega() {
		return entrega;
	}

	public void setEntrega(Long entrega) {
		this.entrega = entrega;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getNotaVenda() {
		return notaVenda;
	}

	public void setNotaVenda(Integer notaVenda) {
		this.notaVenda = notaVenda;
	}

	public String getSerieVenda() {
		return serieVenda;
	}

	public void setSerieVenda(String serieVenda) {
		this.serieVenda = serieVenda;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getDescricaoItem() {
		return descricaoItem;
	}

	public void setDescricaoItem(String descricaoItem) {
		this.descricaoItem = descricaoItem;
	}

	public Integer getQtdItem() {
		return qtdItem;
	}

	public void setQtdItem(Integer qtdItem) {
		this.qtdItem = qtdItem;
	}

	public BigDecimal getValorItem() {
		return valorItem;
	}

	public void setValorItem(BigDecimal valorItem) {
		this.valorItem = valorItem;
	}

	public BigDecimal getFreteItem() {
		return freteItem;
	}

	public void setFreteItem(BigDecimal freteItem) {
		this.freteItem = freteItem;
	}

	public BigDecimal getDescontoItem() {
		return descontoItem;
	}

	public void setDescontoItem(BigDecimal descontoItem) {
		this.descontoItem = descontoItem;
	}

	@Override
	public AgendamentoInsucesso map(ResultSet rs) throws SQLException {
		AgendamentoInsucesso model = new AgendamentoInsucesso();
		model.setFilial(rs.getInt("FILIAL"));
		model.setTransportadora(StringUtils.trim(rs.getString("TRANSPORTADORA")));
		model.setEntrega(rs.getLong("ENTREGA"));
		model.setNota(rs.getInt("NOTA"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setNotaVenda(rs.getInt("NOTA VENDA"));
		model.setSerieVenda(StringUtils.trim(rs.getString("SERIE VENDA")));
		model.setNomeCliente(StringUtils.trim(rs.getString("NOME CLIENTE")));
		model.setItem(rs.getInt("ITEM"));
		model.setDescricaoItem(StringUtils.trim(rs.getString("DESCRICAO ITEM")));
		model.setQtdItem(rs.getInt("QTD ITEM"));
		model.setValorItem(rs.getBigDecimal("VALOR ITEM"));
		model.setFreteItem(rs.getBigDecimal("FRETE ITEM"));
		model.setDescontoItem(rs.getBigDecimal("DESCONTO ITEM"));
		return model;
	}
}