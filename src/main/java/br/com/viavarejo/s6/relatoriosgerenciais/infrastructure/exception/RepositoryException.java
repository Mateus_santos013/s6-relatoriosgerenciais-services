package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception;

public class RepositoryException extends Exception {

	private static final long serialVersionUID = 1L;

	public RepositoryException() {
	}

	public RepositoryException(String msg) {
		super(msg);
	}

	public RepositoryException(Throwable throwable) {
		super(throwable);
	}

	public RepositoryException(String msg, Throwable throwable) {
		super(msg, throwable);
	}
}