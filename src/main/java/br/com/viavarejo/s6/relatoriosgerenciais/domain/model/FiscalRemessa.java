package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class FiscalRemessa extends BaseModel<FiscalRemessa> {

	private static final long serialVersionUID = 6552006558150458433L;

	private int nf;
	private String serie;
	private String chaveNmFilial;
	private Date data;
	private String cnpj;
	private int cnpjRai;
	private int cnpjFil;
	private int cnpjDvr;
	private int codFornecedor;
	private String nomeFornecedor;
	private String codCfop;
	private String descCfop;
	private String seqCfop;
	private String situacaoNf;
	private int idItem;
	private String descItem;
	private int qtd;
	private BigDecimal precoUnit;
	private BigDecimal vlTotal;
	private String tipo;

	public int getNf() {
		return nf;
	}

	public void setNf(int nf) {
		this.nf = nf;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getChaveNmFilial() {
		return chaveNmFilial;
	}

	public void setChaveNmFilial(String chaveNmFilial) {
		this.chaveNmFilial = chaveNmFilial;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public int getCodFornecedor() {
		return codFornecedor;
	}

	public void setCodFornecedor(int codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getCodCfop() {
		return codCfop;
	}

	public void setCodCfop(String codCfop) {
		this.codCfop = codCfop;
	}

	public String getDescCfop() {
		return descCfop;
	}

	public void setDescCfop(String descCfop) {
		this.descCfop = descCfop;
	}

	public String getSeqCfop() {
		return seqCfop;
	}

	public void setSeqCfop(String seqCfop) {
		this.seqCfop = seqCfop;
	}

	public String getSituacaoNf() {
		return situacaoNf;
	}

	public void setSituacaoNf(String situacaoNf) {
		this.situacaoNf = situacaoNf;
	}

	public int getIdItem() {
		return idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

	public BigDecimal getPrecoUnit() {
		return precoUnit;
	}

	public void setPrecoUnit(BigDecimal precoUnit) {
		this.precoUnit = precoUnit;
	}

	public BigDecimal getVlTotal() {
		return vlTotal;
	}

	public void setVlTotal(BigDecimal vlTotal) {
		this.vlTotal = vlTotal;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getCnpjRai() {
		return cnpjRai;
	}

	public void setCnpjRai(int cnpjRai) {
		this.cnpjRai = cnpjRai;
	}

	public int getCnpjFil() {
		return cnpjFil;
	}

	public void setCnpjFil(int cnpjFil) {
		this.cnpjFil = cnpjFil;
	}

	public int getCnpjDvr() {
		return cnpjDvr;
	}

	public void setCnpjDvr(int cnpjDvr) {
		this.cnpjDvr = cnpjDvr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public FiscalRemessa map(ResultSet rs) throws SQLException {
		FiscalRemessa model = new FiscalRemessa();
		model.setNf(rs.getInt("NF"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setChaveNmFilial(StringUtils.trim(rs.getString("CHAVE_NM_FILIAL")));
		model.setData(rs.getDate("DATA"));
		//model.setCnpj(StringUtils.trim(rs.getString("CNPJ")));
		model.setCnpjRai(rs.getInt("CD_CNPJ_RAI"));
		model.setCnpjFil(rs.getInt("CD_CNPJ_FIL"));
		model.setCnpjDvr(rs.getInt("CD_CNPJ_DVR"));
		model.setCodCfop(StringUtils.trim(rs.getString("COD_FORNECEDOR")));
		model.setNomeFornecedor(StringUtils.trim(rs.getString("NOME_FORNECEDOR")));
		model.setCodCfop(StringUtils.trim(rs.getString("COD_CFOP")));
		model.setDescCfop(StringUtils.trim(rs.getString("DESC_CFOP")));
		model.setSeqCfop(StringUtils.trim(rs.getString("SEQ_CFOP")));
		model.setSituacaoNf(StringUtils.trim(rs.getString("SITUACAO_NF")));
		model.setIdItem(rs.getInt("ID_ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DESC_ITEM")));
		model.setQtd(rs.getInt("QTD"));
		model.setPrecoUnit(rs.getBigDecimal("PRECO_UNIT"));
		model.setVlTotal(rs.getBigDecimal("VL_TOTAL"));
		model.setTipo(StringUtils.trim(rs.getString("TIPO")));
		return model;
	}
}