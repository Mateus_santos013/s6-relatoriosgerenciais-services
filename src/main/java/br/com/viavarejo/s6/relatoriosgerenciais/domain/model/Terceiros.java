package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Terceiros extends BaseModel<Terceiros> {

	private static final long serialVersionUID = 916988627048723128L;

	private Integer companhia;
	private Date data_fiscal;
	private Integer ref_faturamento;
	private String docSerieRms;
	private String docSerieFne;
	private String tipo_doc;
	private String tipo_doc_fiscal;
	private String origem_transacao;
	private Integer nota_fiscal;
	private String parceiro_negocio;
	private String item_fatur;
	private Integer saldo_disponivel;
	private Integer retorno_remessa;
	private Integer qtd_retorno;
	private BigDecimal valor_mercadoria;

	public Integer getCompanhia() {
		return companhia;
	}

	public void setCompanhia(Integer companhia) {
		this.companhia = companhia;
	}

	public Date getData_fiscal() {
		return data_fiscal;
	}

	public void setData_fiscal(Date data_fiscal) {
		this.data_fiscal = data_fiscal;
	}

	public Integer getRef_faturamento() {
		return ref_faturamento;
	}

	public void setRef_faturamento(Integer ref_faturamento) {
		this.ref_faturamento = ref_faturamento;
	}

	public String getDocSerieRms() {
		return docSerieRms;
	}

	public void setDocSerieRms(String docSerieRms) {
		this.docSerieRms = docSerieRms;
	}

	public String getDocSerieFne() {
		return docSerieFne;
	}

	public void setDocSerieFne(String docSerieFne) {
		this.docSerieFne = docSerieFne;
	}

	public String getTipo_doc() {
		return tipo_doc;
	}

	public void setTipo_doc(String tipo_doc) {
		this.tipo_doc = tipo_doc;
	}

	public String getTipo_doc_fiscal() {
		return tipo_doc_fiscal;
	}

	public void setTipo_doc_fiscal(String tipo_doc_fiscal) {
		this.tipo_doc_fiscal = tipo_doc_fiscal;
	}

	public String getOrigem_transacao() {
		return origem_transacao;
	}

	public void setOrigem_transacao(String origem_transacao) {
		this.origem_transacao = origem_transacao;
	}

	public Integer getNota_fiscal() {
		return nota_fiscal;
	}

	public void setNota_fiscal(Integer nota_fiscal) {
		this.nota_fiscal = nota_fiscal;
	}

	public String getParceiro_negocio() {
		return parceiro_negocio;
	}

	public void setParceiro_negocio(String parceiro_negocio) {
		this.parceiro_negocio = parceiro_negocio;
	}

	public String getItem_fatur() {
		return item_fatur;
	}

	public void setItem_fatur(String item_fatur) {
		this.item_fatur = item_fatur;
	}

	public Integer getSaldo_disponivel() {
		return saldo_disponivel;
	}

	public void setSaldo_disponivel(Integer saldo_disponivel) {
		this.saldo_disponivel = saldo_disponivel;
	}

	public Integer getRetorno_remessa() {
		return retorno_remessa;
	}

	public void setRetorno_remessa(Integer retorno_remessa) {
		this.retorno_remessa = retorno_remessa;
	}

	public Integer getQtd_retorno() {
		return qtd_retorno;
	}

	public void setQtd_retorno(Integer qtd_retorno) {
		this.qtd_retorno = qtd_retorno;
	}

	public BigDecimal getValor_mercadoria() {
		return valor_mercadoria;
	}

	public void setValor_mercadoria(BigDecimal valor_mercadoria) {
		this.valor_mercadoria = valor_mercadoria;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public Terceiros map(ResultSet rs) throws SQLException {
		Terceiros model = new Terceiros();
		model.setCompanhia(rs.getInt("COMPANHIA"));
		model.setData_fiscal(rs.getDate("DATA_FISCAL"));
		model.setRef_faturamento(rs.getInt("REF_FATURAMENTO"));
		model.setDocSerieRms(rs.getString("DOC_SERIE_RMS"));
		model.setTipo_doc(rs.getString("TIPO_DOC"));
		model.setTipo_doc_fiscal(rs.getString("TIPO_DOC_FISCAL"));
		model.setOrigem_transacao(rs.getString("ORIGEM_TRANSACAO"));
		model.setNota_fiscal(rs.getInt("NOTA_FISCAL"));
		model.setDocSerieFne(rs.getString("DOC_SERIE_FNE"));
		model.setParceiro_negocio(rs.getString("PARCEIRO_NEGOCIO"));
		model.setItem_fatur(rs.getString("ITEM_FATUR"));
		model.setSaldo_disponivel(rs.getInt("SALDO_DISPONIVEL"));
		model.setRetorno_remessa(rs.getInt("RETORNO_REMESSA_SIMB"));
		model.setQtd_retorno(rs.getInt("QUANTIDADE_RETORNO"));
		model.setValor_mercadoria(rs.getBigDecimal("VALOR_MERCADORIA"));
		return model;
	}
}