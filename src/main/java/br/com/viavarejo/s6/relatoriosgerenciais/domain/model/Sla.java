package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class Sla extends BaseModel<Sla> {

	private static final long serialVersionUID = 5210065353977633727L;

	private BigDecimal entrega;
	private Integer sequencial;
	private Integer nota;
	private String serie;
	private Integer filial;
	private String descFilial;
	private Integer transportador;
	private String descTransp;
	private String dataSaida;
	private Date dataPrometida;
	private Date dataProcessamento;
	private String ocorrencia;
	private String descOcorrencia;
	private Date dataOcorrencia;
	private Time horaOcorrencia;
	private String baixaManual;
	private String descUnNegocio;
	private String ordemVenda;
	private String uf;
	private Integer cep;
	private String cidade;
	private String regiao;
	private String descTipoEntrega;
	private String finalizadoPendente;
	private Date dataCompra;
	private Date dataLimite;
	private Date dataEntrega;
	private Date dataCorrigida;
	private Date dataPrevista;
	private String tipoEntrega;
	private String tipoColeta;
	private Integer item;
	private String descItem;
	private BigDecimal vlProduto;
	private BigDecimal freteTransportadora;
	private BigDecimal freteCobradoSite;
	private Integer qtdItens;
	private String capitalInterior;
	private String origemOrdemFrete;
	private Integer empresa;
	private Integer pedidoOff;
	private Integer std;
	private Date dataSolic;
	private Integer empTransp;
	private Integer fornTransp;
	private Integer pedido;
	private Integer pedidoDsm;
	private Date dataPedido;
	private Date dataLimiteExp;
	private String produtoNaoProduto;
	private Integer mercadoria;
	private String descMercadoria;
	private BigDecimal valorLiquido;
	private String valorCobradoFrete;
	private BigDecimal valorFrete;
	private String rota;
	private String funcionario;
	private String reversaStd;
	private String observacao;

	public String getDescFilial() {
		return descFilial;
	}

	public void setDescFilial(String descFilial) {
		this.descFilial = descFilial;
	}

	public String getDescTransp() {
		return descTransp;
	}

	public void setDescTransp(String descTransp) {
		this.descTransp = descTransp;
	}

	public String getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public String getDescOcorrencia() {
		return descOcorrencia;
	}

	public void setDescOcorrencia(String descOcorrencia) {
		this.descOcorrencia = descOcorrencia;
	}

	public Date getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(Date dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public Time getHoraOcorrencia() {
		return horaOcorrencia;
	}

	public void setHoraOcorrencia(Time horaOcorrencia) {
		this.horaOcorrencia = horaOcorrencia;
	}

	public String getBaixaManual() {
		return baixaManual;
	}

	public void setBaixaManual(String baixaManual) {
		this.baixaManual = baixaManual;
	}

	public String getDescUnNegocio() {
		return descUnNegocio;
	}

	public void setDescUnNegocio(String descUnNegocio) {
		this.descUnNegocio = descUnNegocio;
	}

	public String getOrdemVenda() {
		return ordemVenda;
	}

	public void setOrdemVenda(String ordemVenda) {
		this.ordemVenda = ordemVenda;
	}

	public BigDecimal getVlProduto() {
		return vlProduto;
	}

	public void setVlProduto(BigDecimal vlProduto) {
		this.vlProduto = vlProduto;
	}

	public BigDecimal getFreteTransportadora() {
		return freteTransportadora;
	}

	public void setFreteTransportadora(BigDecimal freteTransportadora) {
		this.freteTransportadora = freteTransportadora;
	}

	public BigDecimal getFreteCobradoSite() {
		return freteCobradoSite;
	}

	public void setFreteCobradoSite(BigDecimal freteCobradoSite) {
		this.freteCobradoSite = freteCobradoSite;
	}

	public Integer getQtdItens() {
		return qtdItens;
	}

	public void setQtdItens(Integer qtdItens) {
		this.qtdItens = qtdItens;
	}

	public String getCapitalInterior() {
		return capitalInterior;
	}

	public void setCapitalInterior(String capitalInterior) {
		this.capitalInterior = capitalInterior;
	}

	public Date getDataSolic() {
		return dataSolic;
	}

	public void setDataSolic(Date dataSolic) {
		this.dataSolic = dataSolic;
	}

	public Integer getEmpTransp() {
		return empTransp;
	}

	public void setEmpTransp(Integer empTransp) {
		this.empTransp = empTransp;
	}

	public Integer getFornTransp() {
		return fornTransp;
	}

	public void setFornTransp(Integer fornTransp) {
		this.fornTransp = fornTransp;
	}

	public Integer getPedidoDsm() {
		return pedidoDsm;
	}

	public void setPedidoDsm(Integer pedidoDsm) {
		this.pedidoDsm = pedidoDsm;
	}

	public Date getDataPedido() {
		return dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public Date getDataLimiteExp() {
		return dataLimiteExp;
	}

	public void setDataLimiteExp(Date dataLimiteExp) {
		this.dataLimiteExp = dataLimiteExp;
	}

	public String getProdutoNaoProduto() {
		return produtoNaoProduto;
	}

	public void setProdutoNaoProduto(String produtoNaoProduto) {
		this.produtoNaoProduto = produtoNaoProduto;
	}

	public String getDescMercadoria() {
		return descMercadoria;
	}

	public void setDescMercadoria(String descMercadoria) {
		this.descMercadoria = descMercadoria;
	}

	public BigDecimal getValorLiquido() {
		return valorLiquido;
	}

	public void setValorLiquido(BigDecimal valorLiquido) {
		this.valorLiquido = valorLiquido;
	}

	public String getValorCobradoFrete() {
		return valorCobradoFrete;
	}

	public void setValorCobradoFrete(String valorCobradoFrete) {
		this.valorCobradoFrete = valorCobradoFrete;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public String getReversaStd() {
		return reversaStd;
	}

	public void setReversaStd(String reversaStd) {
		this.reversaStd = reversaStd;
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getMercadoria() {
		return mercadoria;
	}

	public void setMercadoria(Integer mercadoria) {
		this.mercadoria = mercadoria;
	}

	public Integer getPedido() {
		return pedido;
	}

	public void setPedido(Integer pedido) {
		this.pedido = pedido;
	}

	public String getTipoColeta() {
		return tipoColeta;
	}

	public void setTipoColeta(String tipoColeta) {
		this.tipoColeta = tipoColeta;
	}

	public Integer getStd() {
		return std;
	}

	public void setStd(Integer std) {
		this.std = std;
	}

	public Integer getPedidoOff() {
		return pedidoOff;
	}

	public void setPedidoOff(Integer pedidoOff) {
		this.pedidoOff = pedidoOff;
	}

	public Integer getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public Integer getTransportador() {
		return transportador;
	}

	public void setTransportador(Integer transportador) {
		this.transportador = transportador;
	}

	public String getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getDescTipoEntrega() {
		return descTipoEntrega;
	}

	public void setDescTipoEntrega(String descTipoEntrega) {
		this.descTipoEntrega = descTipoEntrega;
	}

	public String getFinalizadoPendente() {
		return finalizadoPendente;
	}

	public void setFinalizadoPendente(String finalizadoPendente) {
		this.finalizadoPendente = finalizadoPendente;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Date getDataLimite() {
		return dataLimite;
	}

	public void setDataLimite(Date dataLimite) {
		this.dataLimite = dataLimite;
	}

	public Date getDataPrometida() {
		return dataPrometida;
	}

	public void setDataPrometida(Date dataPrometida) {
		this.dataPrometida = dataPrometida;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataCorrigida() {
		return dataCorrigida;
	}

	public void setDataCorrigida(Date dataCorrigida) {
		this.dataCorrigida = dataCorrigida;
	}

	public Date getDataPrevista() {
		return dataPrevista;
	}

	public void setDataPrevista(Date dataPrevista) {
		this.dataPrevista = dataPrevista;
	}

	public String getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public String getOrigemOrdemFrete() {
		return origemOrdemFrete;
	}

	public void setOrigemOrdemFrete(String origemOrdemFrete) {
		this.origemOrdemFrete = origemOrdemFrete;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public Sla map(ResultSet rs) throws SQLException {
		Sla model = new Sla();
		model.setEntrega(rs.getBigDecimal("PED_ENTREGA"));
		model.setSequencial(rs.getInt("SEQUENCIAL"));
		model.setStd(rs.getInt("STD"));
		model.setDataSolic(rs.getDate("DATA_SOLIC"));
		model.setNota(rs.getInt("NOTA_ENTRADA"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE_ENTRADA")));
		model.setFilial(rs.getInt("FILIAL"));
		model.setEmpTransp(rs.getInt("EMP_TRANSP"));
		model.setFornTransp(rs.getInt("FORN_TRANSP"));
		model.setDescFilial(StringUtils.trim(rs.getString("DESC_FILIAL")));
		model.setDescTransp(StringUtils.trim(rs.getString("NOME_TRANSP")));
		model.setDataSaida(StringUtils.trim(rs.getString("DATA_SAIDA")));
		model.setDataPrometida(rs.getDate("DATA_PROMETIDA"));
		model.setDataProcessamento(rs.getDate("DATA_PROCES"));
		model.setOcorrencia(StringUtils.trim(rs.getString("OCORRENCIA")));
		model.setDescOcorrencia(StringUtils.trim(rs.getString("DESCRICAO")));
		model.setDataOcorrencia(rs.getDate("DATA_OCORRENCIA"));
		model.setHoraOcorrencia(rs.getTime("HORA_OCORRENCIA"));
		model.setBaixaManual(StringUtils.trim(rs.getString("MANUAL_BAIXA_EDI")));
		model.setDescUnNegocio(StringUtils.trim(rs.getString("DESC_UN_NEG")));
		model.setUf(rs.getString("UF"));
		model.setCep(rs.getInt("CEP"));
		model.setCidade(StringUtils.trim(rs.getString("CIDADE")));
		model.setRegiao(StringUtils.trim(rs.getString("REGIAO")));
		model.setTipoColeta(StringUtils.trim(rs.getString("TIPO_COLETA")));
		model.setFinalizadoPendente(StringUtils.trim(rs.getString("FINALIZADO_PENDENTE")));
		model.setPedido(rs.getInt("PEDIDO"));
		model.setPedidoDsm(rs.getInt("PEDIDO_DSM"));
		model.setDataPedido(rs.getDate("DATA_PEDIDO"));
		model.setDataLimiteExp(rs.getDate("DATA_LIMITE_EXP"));
		model.setDataEntrega(rs.getDate("DATA_ENTREGA"));
		model.setDataCorrigida(rs.getDate("DATA_CORRIGIDA"));
		model.setTipoEntrega(StringUtils.trim(rs.getString("TIPO")));
		model.setProdutoNaoProduto(StringUtils.trim(rs.getString("PRODUTO_NAO_PRODUTO")));
		model.setMercadoria(rs.getInt("MERCADORIA"));
		model.setDescMercadoria(StringUtils.trim(rs.getString("DESC_MERCADORIA")));
		model.setValorLiquido(rs.getBigDecimal("VALOR_LIQUIDO"));
		model.setValorCobradoFrete(StringUtils.trim(rs.getString("VALOR_COBRADO_FRETE")));
		model.setValorFrete(rs.getBigDecimal("VALOR_FRETE"));
		model.setQtdItens(rs.getInt("QTDE_MCR"));
		model.setRota(StringUtils.trim(rs.getString("ROTA")));
		model.setFuncionario(StringUtils.trim(rs.getString("FUNCIONARIO")));
		model.setReversaStd(StringUtils.trim(rs.getString("REVERSA_STD")));
		model.setObservacao(StringUtils.trim(rs.getString("OBSERVACAO")));
		return model;
	}
}