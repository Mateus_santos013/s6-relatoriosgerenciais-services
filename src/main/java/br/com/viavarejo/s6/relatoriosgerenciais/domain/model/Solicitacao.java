package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class Solicitacao extends BaseModel<Solicitacao> {

	private static final long serialVersionUID = 8899762035823141515L;

	private Integer numSolic;
	private String relatorio;
	private String descRelatorio;
	private Date dataSolic;
	private String filtro;
	private String status;

	public Integer getNumSolic() {
		return numSolic;
	}

	public void setNumSolic(Integer numSolic) {
		this.numSolic = numSolic;
	}

	public String getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}

	public Date getDataSolic() {
		return dataSolic;
	}

	public void setDataSolic(Date dataSolic) {
		this.dataSolic = dataSolic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDescRelatorio() {
		return descRelatorio;
	}

	public void setDescRelatorio(String descRelatorio) {
		this.descRelatorio = descRelatorio;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	@Override
	public Solicitacao map(ResultSet rs) throws SQLException {
		Solicitacao model = new Solicitacao();
		model.setNumSolic(rs.getInt("SOLICITACAO"));
		model.setRelatorio(StringUtils.trim(rs.getString("RELATORIO")));
		model.setDescRelatorio(StringUtils.trim(rs.getString("DESC_RELATORIO")));
		model.setFiltro(StringUtils.trim(rs.getString("FILTRO")));
		model.setStatus(StringUtils.trim(rs.getString("STATUS")));
		model.setDataSolic(rs.getDate("DATA_SOLICITACAO"));
		return model;
	}
}