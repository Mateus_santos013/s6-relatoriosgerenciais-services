package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security;

public class JWTSecurityConstants {
	
	//public static final long	EXPIRATION_TIME 	= 1_800_000;
	public static final long	EXPIRATION_TIME 	= 1_800_000;
	public static final String 	SECRET 				= "E0EUI7ikgmh67iun6sYQdsU5Rqm7NLuQ";
	public static final String 	TOKEN_PREFIX 		= "Bearer";
	public static final String 	HEADER_STRING		= "Authorization";
}
