package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.file;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.model.RelatorioCSV;

public class XLSFormatter {

    public void formatarXLS(final Object document, final String relatorio) {

            HSSFWorkbook wb = (HSSFWorkbook) document;
            HSSFSheet sheet = wb.getSheetAt(0);

            /// ----- Atribuindo tipo de dados para as celulas --------------------

            this.cellDataFormat(wb, sheet);

            wb.setSheetName(0, relatorio);

            HSSFRow row = sheet.getRow(0);
            row.setHeightInPoints(28);

            Font font = wb.createFont();
            font.setColor(IndexedColors.WHITE.getIndex());
            font.setBold(true);

            HSSFCellStyle cellStyle = wb.createCellStyle();

            cellStyle.setFont(font);

            cellStyle.setBorderBottom(BorderStyle.MEDIUM);
            cellStyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
            cellStyle.setBorderLeft(BorderStyle.THIN);
            cellStyle.setLeftBorderColor(IndexedColors.WHITE.getIndex());
            cellStyle.setBorderRight(BorderStyle.THIN);
            cellStyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
            cellStyle.setBorderTop(BorderStyle.THIN);
            cellStyle.setTopBorderColor(IndexedColors.WHITE.getIndex());

            cellStyle.setAlignment(HorizontalAlignment.CENTER);
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            HSSFPalette palette = wb.getCustomPalette();
            palette.setColorAtIndex((short)31, (byte)35, (byte)100, (byte)10);

            cellStyle.setFillForegroundColor(palette.getColor(31).getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            for(int i=0; i < sheet.getRow(0).getPhysicalNumberOfCells();i++) {
                row.getCell(i).setCellStyle(cellStyle);
                sheet.autoSizeColumn(i);
            }

            // ADD FILTROS
            sheet.setAutoFilter(new CellRangeAddress(0,1,sheet.getRow(0).getFirstCellNum(),sheet.getRow(0).getLastCellNum()-1));

            int WIDTH_ARROW_BUTTON = 1300;
            for (int i = 0; i < row.getLastCellNum(); i++) {
                sheet.autoSizeColumn(i);
                // For filter additional arrow width
                sheet.setColumnWidth(i, sheet.getColumnWidth(i) + WIDTH_ARROW_BUTTON);
            }

            // ADD LINHA DE TITULO
            sheet.shiftRows(0, sheet.getLastRowNum(), 1);
            if(sheet.getRow(1).getLastCellNum()-1 == 1) {            	
            	sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 1));
            } else {
            	sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, sheet.getRow(1).getLastCellNum()-1));
            }

            Cell cell = CellUtil.createCell(sheet.getRow(0), 0, relatorio);

            Font fontTitulo = wb.createFont();
            fontTitulo.setColor(palette.getColor(31).getIndex());
            fontTitulo.setBold(true);
            fontTitulo.setFontHeightInPoints((short)16);

            CellUtil.setFont(cell,fontTitulo);
            CellUtil.setAlignment(cell, HorizontalAlignment.LEFT);
            CellUtil.setVerticalAlignment(cell, VerticalAlignment.CENTER);

            HSSFRow row1 = sheet.getRow(1);
            row1.setHeightInPoints(18);

     }


    @SuppressWarnings("deprecation")
	private void cellDataFormat(final HSSFWorkbook wb, final HSSFSheet sheet) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        CreationHelper createHelper = wb.getCreationHelper();

        CellStyle dateCellStyle = wb.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yy"));

        CellStyle valueCellStyle = wb.createCellStyle();
        valueCellStyle.setDataFormat((short)4);
        
        CellStyle longCellStyle = wb.createCellStyle();
        longCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("0"));

        Cell aCell;

        for(int rowInd = 1; rowInd <= sheet.getLastRowNum(); rowInd++) {
            HSSFRow row = sheet.getRow(rowInd);
            for(int cellInd = 0; cellInd < row.getLastCellNum(); cellInd++) {
                aCell = row.getCell(cellInd);
                String strVal = aCell.getStringCellValue();
                if(NumberUtils.isNumber(strVal) &&  strVal.length() < 19) // Parser numero numerico nao decimal // chave danfe > 19 caracteres
                {
                    aCell.setCellType(CellType.BLANK);
                    aCell.setCellType(CellType.NUMERIC);                 
               
                    	long longVal;
                    	double doubleVal;
						try 
						{
							longVal = Long.parseLong(strVal);
							aCell.setCellValue(longVal);
	                    	aCell.setCellStyle(longCellStyle);
	                    	
						} catch (NumberFormatException e) {
							
							doubleVal = NumberUtils.toDouble(strVal);
							aCell.setCellValue(doubleVal);
						}                	
                }
                else if(strVal.length() == 10  && strVal.indexOf('/') == 2 && strVal.lastIndexOf("/") == 5) // parser  de data
                {
                    Date dateCell;
                    try {
                        dateCell = sdf.parse(strVal);
                        aCell.setCellValue(dateCell);
                        aCell.setCellStyle(dateCellStyle);
                    } catch (ParseException e) {
                    	continue;
                    }

                }
                else if(strVal.indexOf("R$ ") > -1) // Parser valor com duas casas decimais
                {
                    strVal = strVal.substring(3);
                    strVal = strVal.replaceAll("\\.","");
                    strVal = strVal.replaceAll("\\,",".");
                    try {
                        aCell.setCellValue(Double.parseDouble(strVal));
                        aCell.setCellStyle(valueCellStyle);
                    } catch (NumberFormatException e) {
                    	continue;
                    }
                }             
            }
        }
    }
 
    
    public HSSFWorkbook geraXLS(List<RelatorioCSV> listaRelatorio, String nomeRel ) {
       
    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(nomeRel);
    	    	
    	for(int RowNum=0; RowNum<listaRelatorio.size();RowNum++){
    		
    	    HSSFRow row = sheet.createRow(RowNum);
    	    RelatorioCSV rel = listaRelatorio.get(RowNum);
    	    String[] linha = StringUtils.split(rel.getConteudo(),';'); 
    	    
    	    for(int ColNum=0; ColNum<linha.length; ColNum++){
    	        HSSFCell cell = row.createCell(ColNum);
    	        cell.setCellValue(linha[ColNum].trim());
    	     }
    	 }
    	
    	
    	
    	formatarXLS(wb, nomeRel);
    	
    	return wb;
    	
    }
        

}
