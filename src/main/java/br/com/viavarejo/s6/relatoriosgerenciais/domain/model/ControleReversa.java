package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class ControleReversa extends BaseModel<ControleReversa> {

	private static final long serialVersionUID = 956490009780460931L;

	private Integer documento;
	private BigDecimal entrega;
	private String ponto;
	private Date dtPonto;
	private Date dtProc;
	private Integer notaFiscal;
	private String serie;
	private Integer filial;
	private String transportadora;
	private Integer cdEmpgcb;
	private Integer cdFun;
	private String nome;

	public Date getDtPonto() {
		return dtPonto;
	}

	public void setDtPonto(Date dtPonto) {
		this.dtPonto = dtPonto;
	}

	public Date getDtProc() {
		return dtProc;
	}

	public void setDtProc(Date dtProc) {
		this.dtProc = dtProc;
	}

	public Integer getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(Integer notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public Integer getCdEmpgcb() {
		return cdEmpgcb;
	}

	public void setCdEmpgcb(Integer cdEmpgcb) {
		this.cdEmpgcb = cdEmpgcb;
	}

	public Integer getCdFun() {
		return cdFun;
	}

	public void setCdFun(Integer cdFun) {
		this.cdFun = cdFun;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getDocumento() {
		return documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public String getPonto() {
		return ponto;
	}

	public void setPonto(String ponto) {
		this.ponto = ponto;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public String getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}

	@Override
	public ControleReversa map(ResultSet rs) throws SQLException {
		ControleReversa model = new ControleReversa();
		model.setDocumento(rs.getInt("DOCUMENTO"));
		model.setEntrega(rs.getBigDecimal("ENTREGA"));
		model.setPonto(StringUtils.trim(rs.getString("PONTO")));
		model.setDtPonto(rs.getDate("DT_PONTO"));
		model.setDtProc(rs.getDate("DT_PROC"));
		model.setNotaFiscal(rs.getInt("NOTA_FISCAL"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setFilial(rs.getInt("FILIAL"));
		model.setTransportadora(StringUtils.trim(rs.getString("TRANSPORTADORA")));
		model.setCdEmpgcb(rs.getInt("CD_EMPGCB_FUN"));
		model.setCdFun(rs.getInt("CD_FUN"));
		model.setNome(StringUtils.trim(rs.getString("NOME")));
		return model;
	}
}