package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class EstoqueArmazenagem extends BaseModel<EstoqueArmazenagem> {

	private static final long serialVersionUID = -110267771473701257L;

	private int planta;
	private String idOm;
	private String romaIdOperador;
	private String usuaNome;
	private String fornecedor;
	private BigDecimal idItem;
	private String descItem;
	private String setor;
	private String dpto;
	private int qt;
	private BigDecimal valor;
	private String claOrigem;
	private String idLocOrig;
	private String statusOrigem;
	private String idLocal;
	private String statusDestino;
	private String idClaLoc;
	private String idSit;
	private Date dtsit;
	private String dtSit1;

	public int getPlanta() {
		return planta;
	}

	public String getIdOm() {
		return idOm;
	}

	public void setIdOm(String idOm) {
		this.idOm = idOm;
	}

	public String getRomaIdOperador() {
		return romaIdOperador;
	}

	public void setRomaIdOperador(String romaIdOperador) {
		this.romaIdOperador = romaIdOperador;
	}

	public String getUsuaNome() {
		return usuaNome;
	}

	public void setUsuaNome(String usuaNome) {
		this.usuaNome = usuaNome;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public BigDecimal getIdItem() {
		return idItem;
	}

	public void setIdItem(BigDecimal idItem) {
		this.idItem = idItem;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public int getQt() {
		return qt;
	}

	public void setQt(int qt) {
		this.qt = qt;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getClaOrigem() {
		return claOrigem;
	}

	public void setClaOrigem(String claOrigem) {
		this.claOrigem = claOrigem;
	}

	public String getIdLocOrig() {
		return idLocOrig;
	}

	public void setIdLocOrig(String idLocOrig) {
		this.idLocOrig = idLocOrig;
	}

	public String getStatusOrigem() {
		return statusOrigem;
	}

	public void setStatusOrigem(String statusOrigem) {
		this.statusOrigem = statusOrigem;
	}

	public String getIdLocal() {
		return idLocal;
	}

	public void setIdLocal(String idLocal) {
		this.idLocal = idLocal;
	}

	public String getStatusDestino() {
		return statusDestino;
	}

	public void setStatusDestino(String statusDestino) {
		this.statusDestino = statusDestino;
	}

	public String getIdClaLoc() {
		return idClaLoc;
	}

	public void setIdClaLoc(String idClaLoc) {
		this.idClaLoc = idClaLoc;
	}

	public String getIdSit() {
		return idSit;
	}

	public void setIdSit(String idSit) {
		this.idSit = idSit;
	}

	public Date getDtsit() {
		return dtsit;
	}

	public void setDtsit(Date dtsit) {
		this.dtsit = dtsit;
	}

	public String getDtSit1() {
		return dtSit1;
	}

	public void setDtSit1(String dtSit1) {
		this.dtSit1 = dtSit1;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setPlanta(int planta) {
		this.planta = planta;
	}

	@Override
	public EstoqueArmazenagem map(ResultSet rs) throws SQLException {
		EstoqueArmazenagem model = new EstoqueArmazenagem();
		model.setPlanta(rs.getInt("PLANTA"));
		model.setIdOm(StringUtils.trim(rs.getString("ID_OM")));
		model.setRomaIdOperador(StringUtils.trim(rs.getString("ROMA_ID_OPERADOR")));
		model.setUsuaNome(StringUtils.trim(rs.getString("USUA_NOME")));
		model.setFornecedor(StringUtils.trim(rs.getString("FORNECEDOR")));
		model.setIdItem(rs.getBigDecimal("ID_ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DESC_ITEM")));
		model.setSetor(StringUtils.trim(rs.getString("SETOR")));
		model.setDpto(StringUtils.trim(rs.getString("DEPARTAMENTO")));
		model.setQt(rs.getInt("QT"));
		model.setValor(rs.getBigDecimal("VALOR"));
		model.setClaOrigem(StringUtils.trim(rs.getString("CLA_ORIGEM")));
		model.setIdLocOrig(StringUtils.trim(rs.getString("ID_LOC_ORIG")));
		model.setStatusOrigem(StringUtils.trim(rs.getString("STATUS_ORIGEM")));
		model.setIdLocal(StringUtils.trim(rs.getString("ID_LOCAL")));
		model.setStatusDestino(StringUtils.trim(rs.getString("STATUS_DESTINO")));
		model.setIdClaLoc(StringUtils.trim(rs.getString("ID_CLA_LOC")));
		model.setIdSit(StringUtils.trim(rs.getString("ID_SIT")));
		model.setDtsit(rs.getDate("DT_SIT"));
		model.setDtSit1(StringUtils.trim(rs.getString("DT_SIT1")));
		return model;
	}
}