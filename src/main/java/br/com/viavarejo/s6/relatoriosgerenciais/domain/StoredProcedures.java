package br.com.viavarejo.s6.relatoriosgerenciais.domain;


/**
 * @author Kleiton Brito
 * @description Parametrização de StoredProcedures
 */
public enum StoredProcedures {

	// DAT
	// ...
	
	// MIS
	DEPARTAMENTOS			("A1S019", 0),
	DEVOLUCAO_CLIENTE		("A1S016", 4),
	ESTOQUE_ARMAZENAGEM		("A1S014", 5),
	ESTOQUE_SIGE			("A1S023", 5),
	FILIAIS					("A1S017", 2),
	FISCAL_REMESSA			("A1S012", 6),
	FISCAL_RETORNO			("A1S018", 5),
	NOTAS_FISCAIS			("A1S015", 7),
	OBSOLESCENCIA			("A1S022", 2),
	RECEBIMENTO_FISCAL		("A1S020", 6),
	TERCEIROS				("A1S021", 5),
	VALOR_PLANTA			("A1S013", 5),	
	RELACAO_COBRANCA		("A1S025", 9),
	
	
	// LOGISTICA REVERSA
	AGENDAMENTO_INSUCESSO	("S6S308", 2),
	AGENDAMENTO_MASSIVO		("S6S315", 5),
	COMBOS_LOGISTICA		("S6S312", 1),
	CONTROLE_REVERSA		("S6S302", 5),
	DADOS_CLIENTE			("S6S303", 4),
	DESVIO_HIST				("S6S309", 3),
	DESVIO_PENDENTE			("S6S310", 3),
	DEZ_ULTIMOS				("S6S316", 2),
	DEVOLUCOES_PENDENTES	("S6S307", 2),
	KIT_COLETA				("S6S300", 8),
	PREVENTIVO_TRANSPORTE	("S6S301", 6),
	RELATORIO_CSV			("S6S314", 2),
	SLA						("S6S306", 6),
	SOLICITACOES			("S6S313", 2),
	SUBIDA_LOTE				("S6S434", 8),
	ULTIMO_PONTO			("S6S305", 4),

	//CRL
	ULTIMO_PONTO_CRL		("S6S317",2),
	DADOS_CLIENTE_CRL		("S6S319",2),
	EXP_REGIONAL			("S6S320",2),
	INST_PENDENTE			("S6S321",2);
	
	
	// Propriedades
	private String	storedProcedureName;
	private int		storedProcedureParams;
	
	
	// Constructores
	private StoredProcedures(String storedProcedureName, int storedProcedureParams) {
		this.storedProcedureName 	= storedProcedureName;
		this.storedProcedureParams 	= storedProcedureParams;
	}
	
	
	// Getters && Setters
	public String getStoredProcedureName() {
		return this.storedProcedureName;
	}
	
	public int getStoredProcedureParams() {
		return this.storedProcedureParams;
	}
}
