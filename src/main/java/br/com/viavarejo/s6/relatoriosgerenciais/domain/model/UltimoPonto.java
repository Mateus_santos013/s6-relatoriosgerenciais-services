package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class UltimoPonto extends BaseModel<UltimoPonto> {

	private static final long serialVersionUID = -23749143335550968L;

	private BigDecimal entrega;
	private BigDecimal pedido;
	private Integer seqPedido;
	private Long numEntrega;
	private String ordemVenda;
	private String ordemVendaSeq;
	private String nf;
	private String serie;
	private String unidNegocio;
	private String tipoVenda;
	private String tipoDevolucao;
	private String motivo;
	private String formaResti;
	private String situacao;
	private BigDecimal valorNota;
	private String codOcorrencia;
	private Date dataOcorrencia;
	private Date dataProcessamento;
	private Date dataProcSeq;
	private Date dataSaida;
	private Date dataPrometida;
	private BigDecimal transportadora;
	private String descTransp;
	private String contrato;
	private String descContrato;
	private BigDecimal valorEntrega;
	private String filial;
	private String descFilial;
	private String cep;
	private String cidade;
	private String uf;
	private String emailCliente;
	private String telefoneResidenc;
	private String telefoneCel;
	private String matriculaFunc;
	private String nomeFunc;
	private Date dataPrevista;
	private Date dataCorrigida;
	private String dddCel;
	private String dddTel;
	private Integer cdEmpTranspOln;
	private Integer cdFneTranspOln;
	private Integer codOcorrenciaStd;

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public BigDecimal getPedido() {
		return pedido;
	}

	public void setPedido(BigDecimal pedido) {
		this.pedido = pedido;
	}

	public Integer getSeqPedido() {
		return seqPedido;
	}

	public void setSeqPedido(Integer seqPedido) {
		this.seqPedido = seqPedido;
	}

	public Long getNumEntrega() {
		return numEntrega;
	}

	public void setNumEntrega(Long numEntrega) {
		this.numEntrega = numEntrega;
	}

	public String getOrdemVenda() {
		return ordemVenda;
	}

	public void setOrdemVenda(String ordemVenda) {
		this.ordemVenda = ordemVenda;
	}

	public String getOrdemVendaSeq() {
		return ordemVendaSeq;
	}

	public void setOrdemVendaSeq(String ordemVendaSeq) {
		this.ordemVendaSeq = ordemVendaSeq;
	}

	public String getNf() {
		return nf;
	}

	public void setNf(String nf) {
		this.nf = nf;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getUnidNegocio() {
		return unidNegocio;
	}

	public void setUnidNegocio(String unidNegocio) {
		this.unidNegocio = unidNegocio;
	}

	public String getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public String getTipoDevolucao() {
		return tipoDevolucao;
	}

	public void setTipoDevolucao(String tipoDevolucao) {
		this.tipoDevolucao = tipoDevolucao;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getFormaResti() {
		return formaResti;
	}

	public void setFormaResti(String formaResti) {
		this.formaResti = formaResti;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getValorNota() {
		return valorNota;
	}

	public void setValorNota(BigDecimal valorNota) {
		this.valorNota = valorNota;
	}

	public String getCodOcorrencia() {
		return codOcorrencia;
	}

	public void setCodOcorrencia(String codOcorrencia) {
		this.codOcorrencia = codOcorrencia;
	}

	public Date getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(Date dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public Date getDataProcessamento() {
		return dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Date getDataProcSeq() {
		return dataProcSeq;
	}

	public void setDataProcSeq(Date dataProcSeq) {
		this.dataProcSeq = dataProcSeq;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	public Date getDataPrometida() {
		return dataPrometida;
	}

	public void setDataPrometida(Date dataPrometida) {
		this.dataPrometida = dataPrometida;
	}

	public BigDecimal getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(BigDecimal transportadora) {
		this.transportadora = transportadora;
	}

	public String getDescTransp() {
		return descTransp;
	}

	public void setDescTransp(String descTransp) {
		this.descTransp = descTransp;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getDescContrato() {
		return descContrato;
	}

	public void setDescContrato(String descContrato) {
		this.descContrato = descContrato;
	}

	public BigDecimal getValorEntrega() {
		return valorEntrega;
	}

	public void setValorEntrega(BigDecimal valorEntrega) {
		this.valorEntrega = valorEntrega;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getDescFilial() {
		return descFilial;
	}

	public void setDescFilial(String descFilial) {
		this.descFilial = descFilial;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getTelefoneResidenc() {
		return telefoneResidenc;
	}

	public void setTelefoneResidenc(String telefoneResidenc) {
		this.telefoneResidenc = telefoneResidenc;
	}

	public String getTelefoneCel() {
		return telefoneCel;
	}

	public void setTelefoneCel(String telefoneCel) {
		this.telefoneCel = telefoneCel;
	}

	public String getMatriculaFunc() {
		return matriculaFunc;
	}

	public void setMatriculaFunc(String matriculaFunc) {
		this.matriculaFunc = matriculaFunc;
	}

	public String getNomeFunc() {
		return nomeFunc;
	}

	public void setNomeFunc(String nomeFunc) {
		this.nomeFunc = nomeFunc;
	}

	public Date getDataPrevista() {
		return dataPrevista;
	}

	public void setDataPrevista(Date dataPrevista) {
		this.dataPrevista = dataPrevista;
	}

	public Date getDataCorrigida() {
		return dataCorrigida;
	}

	public void setDataCorrigida(Date dataCorrigida) {
		this.dataCorrigida = dataCorrigida;
	}

	public String getDddCel() {
		return dddCel;
	}

	public void setDddCel(String dddCel) {
		this.dddCel = dddCel;
	}

	public String getDddTel() {
		return dddTel;
	}

	public void setDddTel(String dddTel) {
		this.dddTel = dddTel;
	}

	public Integer getCdEmpTranspOln() {
		return cdEmpTranspOln;
	}

	public void setCdEmpTranspOln(Integer cdEmpTranspOln) {
		this.cdEmpTranspOln = cdEmpTranspOln;
	}

	public Integer getCdFneTranspOln() {
		return cdFneTranspOln;
	}

	public void setCdFneTranspOln(Integer cdFneTranspOln) {
		this.cdFneTranspOln = cdFneTranspOln;
	}

	public Integer getCodOcorrenciaStd() {
		return codOcorrenciaStd;
	}

	public void setCodOcorrenciaStd(Integer codOcorrenciaStd) {
		this.codOcorrenciaStd = codOcorrenciaStd;
	}

	@Override
	public UltimoPonto map(ResultSet rs) throws SQLException {
		UltimoPonto model = new UltimoPonto();
		model.setPedido(rs.getBigDecimal("PEDIDO"));
		model.setSeqPedido(rs.getInt("PEDIDO_SEQ"));
		model.setEntrega(rs.getBigDecimal("NR_ENTREGA"));
		model.setCodOcorrenciaStd(rs.getInt("COD_OCORR_STD"));				
		model.setOrdemVenda(StringUtils.trim(rs.getString("ORDEM_VENDA")));
		model.setOrdemVendaSeq(StringUtils.trim(rs.getString("ORDEM_VENDA_SEQ")));
		model.setNf(StringUtils.trim(rs.getString("NF")));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setUnidNegocio(StringUtils.trim(rs.getString("UNID_NEGOCIO")));
		model.setTipoVenda(rs.getString("TP_VENDA"));
		model.setTipoDevolucao(StringUtils.trim(rs.getString("TP_DEVOLUCAO")));				
		model.setMotivo(StringUtils.trim(rs.getString("MOTIVO")));
		model.setCodOcorrencia(StringUtils.trim(rs.getString("CD_OCORRENCIA")));		
		model.setDataOcorrencia(rs.getDate("DT_OCORRENCIA"));
		model.setDataSaida(rs.getDate("DT_SAIDA"));
		model.setDataPrometida(rs.getDate("DT_PROMETIDA"));		
		model.setCdEmpTranspOln(rs.getInt("CD_EMP_TRANSP_OLN"));
		model.setCdFneTranspOln(rs.getInt("CD_FNE_TRANSP_OLN"));
		model.setDescTransp(StringUtils.trim(rs.getString("DS_TRANSPORTAD")));				
		model.setContrato(StringUtils.trim(rs.getString("CONTRATO")));  
		model.setDescContrato(StringUtils.trim(rs.getString("DS_CONTRATO")));
		model.setValorEntrega(rs.getBigDecimal("VL_ENTREGA"));
		model.setFilial(StringUtils.trim(rs.getString("CD_FILIAL")));
		model.setDescFilial(StringUtils.trim(rs.getString("NM_FILIAL")));
		model.setCep(StringUtils.trim(rs.getString("CEP")));
		model.setCidade(StringUtils.trim(rs.getString("CIDADE")));
		model.setUf(StringUtils.trim(rs.getString("UF")));	
		model.setEmailCliente(StringUtils.trim(rs.getString("EMAIL_CLI")));
		model.setDddTel(StringUtils.trim(rs.getString("DDD_TEL")));
		model.setTelefoneResidenc(StringUtils.trim(rs.getString("NUM_TEL")));
		model.setDddCel(StringUtils.trim(rs.getString("DDD_CEL")));
		model.setTelefoneCel(StringUtils.trim(rs.getString("NUM_CEL")));
		model.setMatriculaFunc(StringUtils.trim(rs.getString("MATRICULA")));
		model.setNomeFunc(StringUtils.trim(rs.getString("NM_FUN")));
		model.setDataPrevista(rs.getDate("DATA_PREVISTA"));
		model.setDataCorrigida(rs.getDate("DATA_CORRIGIDA"));
		return model;
	}
}