package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class ExpRegional extends BaseModel<ExpRegional> {

	private static final long serialVersionUID = 1L;

	private BigDecimal entrega;
	private Integer pedCd;
	private Integer pedDsm;
	private String tipo;
	private String nota;
	private String serie;
	private String carga;
	private String empFil;
	private String fil;
	private String dtExp;
	private String dtProt;
	private String dtGajut;
	private String nomeTrp;
	private String cnpjTrp;
	private String item;
	private String dsItem;
	private String qtItem;
	private BigDecimal vlItem;
	private BigDecimal vlTotalNf;
	private BigDecimal alt;
	private BigDecimal larg;
	private BigDecimal compr;
	private BigDecimal peso;
	private String danfe;
	private String mun;
	private String uf;
	private String cep;
	private String endereco;
	private String num;
	private String bairro;
	private String ptoRef;
	private String tel1;
	private String tel2;
	private String nome;
	private String cpfCnpj;
	private String email;

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public Integer getPedCd() {
		return pedCd;
	}

	public void setPedCd(Integer pedCd) {
		this.pedCd = pedCd;
	}

	public Integer getPedDsm() {
		return pedDsm;
	}

	public void setPedDsm(Integer pedDsm) {
		this.pedDsm = pedDsm;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getCarga() {
		return carga;
	}

	public void setCarga(String carga) {
		this.carga = carga;
	}

	public String getEmpFil() {
		return empFil;
	}

	public void setEmpFil(String empFil) {
		this.empFil = empFil;
	}

	public String getFil() {
		return fil;
	}

	public void setFil(String fil) {
		this.fil = fil;
	}

	public String getDtExp() {
		return dtExp;
	}

	public void setDtExp(String dtExp) {
		this.dtExp = dtExp;
	}

	public String getDtProt() {
		return dtProt;
	}

	public void setDtProt(String dtProt) {
		this.dtProt = dtProt;
	}

	public String getDtGajut() {
		return dtGajut;
	}

	public void setDtGajut(String dtGajut) {
		this.dtGajut = dtGajut;
	}

	public String getNomeTrp() {
		return nomeTrp;
	}

	public void setNomeTrp(String nomeTrp) {
		this.nomeTrp = nomeTrp;
	}

	public String getCnpjTrp() {
		return cnpjTrp;
	}

	public void setCnpjTrp(String cnpjTrp) {
		this.cnpjTrp = cnpjTrp;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getDsItem() {
		return dsItem;
	}

	public void setDsItem(String dsItem) {
		this.dsItem = dsItem;
	}

	public String getQtItem() {
		return qtItem;
	}

	public void setQtItem(String qtItem) {
		this.qtItem = qtItem;
	}

	public BigDecimal getVlItem() {
		return vlItem;
	}

	public void setVlItem(BigDecimal vlItem) {
		this.vlItem = vlItem;
	}

	public BigDecimal getVlTotalNf() {
		return vlTotalNf;
	}

	public void setVlTotalNf(BigDecimal vlTotalNf) {
		this.vlTotalNf = vlTotalNf;
	}

	public BigDecimal getAlt() {
		return alt;
	}

	public void setAlt(BigDecimal alt) {
		this.alt = alt;
	}

	public BigDecimal getLarg() {
		return larg;
	}

	public void setLarg(BigDecimal larg) {
		this.larg = larg;
	}

	public BigDecimal getCompr() {
		return compr;
	}

	public void setCompr(BigDecimal compr) {
		this.compr = compr;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public String getDanfe() {
		return danfe;
	}

	public void setDanfe(String danfe) {
		this.danfe = danfe;
	}

	public String getMun() {
		return mun;
	}

	public void setMun(String mun) {
		this.mun = mun;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getPtoRef() {
		return ptoRef;
	}

	public void setPtoRef(String ptoRef) {
		this.ptoRef = ptoRef;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public ExpRegional map(ResultSet rs) throws SQLException {
		ExpRegional model = new ExpRegional();
		
		model.setEntrega(rs.getBigDecimal("REL01_ENTREGA"));
		model.setPedCd(rs.getInt("REL01_PED_CD"));
		model.setPedDsm(rs.getInt("REL01_PED_DSM"));
		model.setTipo(StringUtils.trim(rs.getString("REL01_TIPO")));
		model.setNota(StringUtils.trim(rs.getString("REL01_NOTA")));
		model.setSerie(StringUtils.trim(rs.getString("REL01_SERIE")));
		model.setCarga(StringUtils.trim(rs.getString("REL01_CARGA")));
		model.setEmpFil(StringUtils.trim(rs.getString("REL01_EMPFIL")));
		model.setFil(StringUtils.trim(rs.getString("REL01_FIL")));
		model.setDtExp(StringUtils.trim(rs.getString("REL01_DTEXPEDIC")));
		model.setDtProt(StringUtils.trim(rs.getString("REL01_DTETGPROT")));
		model.setDtGajut(StringUtils.trim(rs.getString("REL01_DTETGAJUT")));
		model.setNomeTrp(StringUtils.trim(rs.getString("REL01_NOMETRP")));
		model.setCnpjTrp(StringUtils.trim(rs.getString("REL01_CNPJTRP")));
		model.setItem(StringUtils.trim(rs.getString("REL01_ITEM")));
		model.setDsItem(StringUtils.trim(rs.getString("REL01_DS_ITEM")));
		model.setQtItem(StringUtils.trim(rs.getString("REL01_QT_ITEM")));
		model.setVlItem(rs.getBigDecimal("REL01_VL_ITEM"));
		model.setVlTotalNf(rs.getBigDecimal("REL01_VL_TOTAL_NF"));
		model.setAlt(rs.getBigDecimal("REL01_ALT"));
		model.setLarg(rs.getBigDecimal("REL01_LARG"));
		model.setCompr(rs.getBigDecimal("REL01_COMPR"));
		model.setPeso(rs.getBigDecimal("REL01_PESO"));
		model.setDanfe(StringUtils.trim(rs.getString("REL01_DANFE")));
		model.setMun(StringUtils.trim(rs.getString("REL01_MUN")));
		model.setUf(StringUtils.trim(rs.getString("REL01_UF")));
		model.setCep(StringUtils.trim(rs.getString("REL01_CEP")));
		model.setEndereco(StringUtils.trim(rs.getString("REL01_ENDERECO")));
		model.setNum(StringUtils.trim(rs.getString("REL01_NUM")));
		model.setBairro(StringUtils.trim(rs.getString("REL01_BAIRRO")));
		model.setPtoRef(StringUtils.trim(rs.getString("REL01_PTOREF")));
		model.setTel1(StringUtils.trim(rs.getString("REL01_TEL01")));
		model.setTel2(StringUtils.trim(rs.getString("REL01_TEL02")));
		model.setNome(StringUtils.trim(rs.getString("REL01_NOME")));
		model.setCpfCnpj(StringUtils.trim(rs.getString("REL01_CPFCNPJ")));
		model.setCpfCnpj(StringUtils.trim(rs.getString("REL01_EMAIL")));
		
		
		return model;
	}
}
