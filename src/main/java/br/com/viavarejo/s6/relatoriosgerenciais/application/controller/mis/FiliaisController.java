package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.mis;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/mis")
public class FiliaisController {

	@Autowired
	@Qualifier("Filial")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/filiais")
	public ResponseEntity<?> listar(@RequestParam(name="empresa") Integer empresa, @RequestParam(name="codRel") String codRel) {    	
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("empresa", empresa);
		params.put("codRel", codRel);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.FILIAIS, params);
    }
}