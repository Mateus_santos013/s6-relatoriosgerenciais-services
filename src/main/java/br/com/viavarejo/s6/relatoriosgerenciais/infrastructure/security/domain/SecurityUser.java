package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SecurityUser implements Serializable {
	
	private static final long serialVersionUID = -6812362636134013193L;
	private int empresa;
	private int matricula;
	private int filial;
	private String nome;
	private List<SimpleGrantedAuthority> permissoes;
	private Date expirationTime;
	
	
	// Getters && Setters
	public int getEmpresa() {
		return empresa;
	}
	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public int getFilial() {
		return filial;
	}
	public void setFilial(int filial) {
		this.filial = filial;
	}
	@JsonAlias("nomeFuncionario")
	public String getNome() {
		return nome;
	}
	@JsonProperty("nome")
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<SimpleGrantedAuthority> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<SimpleGrantedAuthority> permissoes) {
		this.permissoes = permissoes;
	}
	public Date getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	@Override
	public String toString() {
		return "SecurityUser [empresa=" + empresa + ", matricula=" + matricula + ", filial="
				+ filial + ", nome=" + nome + ", permissoes=" + permissoes + "]";
	}
	
	
}
