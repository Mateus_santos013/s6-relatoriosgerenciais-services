package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.model.AgendamentoMassivo;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.RepositoryException;

@RestController
@RequestMapping("/logistica-reversa")
public class AgendamentoMassivoController {
	
	@Autowired
	@Qualifier("AgendamentoMassivo")
	private StoredProcedureRepository repository;
	
	@SuppressWarnings("unchecked")
	@PostMapping("/agendamento-massivo")
	public ResponseEntity<?> listar(@RequestBody List<AgendamentoMassivo> lista) {
		for (AgendamentoMassivo item : lista) {
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("tipo", item.getTipo());
			params.put("documento", item.getNumDocumento20());
			params.put("dsm", item.getDsm());
			params.put("empresa", item.getEmpresa());
			params.put("matricula", item.getMatricula());

			try {
				AgendamentoMassivo tmp = ((List<AgendamentoMassivo>)repository.findAll(StoredProcedures.AGENDAMENTO_MASSIVO, params)).get(0);
				item.setCodRet(tmp.getCodRet());
				item.setMsgRet(tmp.getMsgRet());
			} 
			catch (RepositoryException e) {
				item.setCodRet(99);
				item.setMsgRet(e.getLocalizedMessage());
			}
		}
		
		return new ResponseEntity<List<AgendamentoMassivo>>(lista, HttpStatus.OK);
	}
}