package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class ValorPlanta extends BaseModel<ValorPlanta> {

	private static final long serialVersionUID = 548551001136372891L;

	private Integer armazem;
	private Integer codForn;
	private String fornecedor;
	private String agrupad;
	private Integer idFamilia;
	private String nomeFamilia;
	private BigDecimal itemLn;
	private BigDecimal item;
	private String descItem;
	private BigDecimal ean;
	private String situacao;
	private String dpto;
	private String setor;
	private String clal;
	private String loca;
	private BigDecimal preco;
	private String warr;
	private Integer qtdEstoq;
	private BigDecimal valor;
	private BigDecimal peso;
	private BigDecimal m3;
	private String kitTik21;
	private String componenteMestre;

	public Integer getArmazem() {
		return armazem;
	}

	public void setArmazem(Integer armazem) {
		this.armazem = armazem;
	}

	public Integer getCodForn() {
		return codForn;
	}

	public void setCodForn(Integer codForn) {
		this.codForn = codForn;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getAgrupad() {
		return agrupad;
	}

	public void setAgrupad(String agrupad) {
		this.agrupad = agrupad;
	}

	public Integer getIdFamilia() {
		return idFamilia;
	}

	public void setIdFamilia(Integer idFamilia) {
		this.idFamilia = idFamilia;
	}

	public String getNomeFamilia() {
		return nomeFamilia;
	}

	public void setNomeFamilia(String nomeFamilia) {
		this.nomeFamilia = nomeFamilia;
	}

	public BigDecimal getItemLn() {
		return itemLn;
	}

	public void setItemLn(BigDecimal itemLn) {
		this.itemLn = itemLn;
	}

	public BigDecimal getItem() {
		return item;
	}

	public void setItem(BigDecimal item) {
		this.item = item;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public BigDecimal getEan() {
		return ean;
	}

	public void setEan(BigDecimal ean) {
		this.ean = ean;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getClal() {
		return clal;
	}

	public void setClal(String clal) {
		this.clal = clal;
	}

	public String getLoca() {
		return loca;
	}

	public void setLoca(String loca) {
		this.loca = loca;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public String getWarr() {
		return warr;
	}

	public void setWarr(String warr) {
		this.warr = warr;
	}

	public Integer getQtdEstoq() {
		return qtdEstoq;
	}

	public void setQtdEstoq(Integer qtdEstoq) {
		this.qtdEstoq = qtdEstoq;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getM3() {
		return m3;
	}

	public void setM3(BigDecimal m3) {
		this.m3 = m3;
	}

	public String getKitTik21() {
		return kitTik21;
	}

	public void setKitTik21(String kitTik21) {
		this.kitTik21 = kitTik21;
	}

	public String getComponenteMestre() {
		return componenteMestre;
	}

	public void setComponenteMestre(String componenteMestre) {
		this.componenteMestre = componenteMestre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public ValorPlanta map(ResultSet rs) throws SQLException {
		ValorPlanta model = new ValorPlanta();
		model.setArmazem(rs.getInt("ARMAZEM"));
		model.setCodForn(rs.getInt("COD_FORN"));
		model.setFornecedor(StringUtils.trim(rs.getString("FORNECEDOR")));
		model.setAgrupad(StringUtils.trim(rs.getString("AGRUPADOR")));
		model.setNomeFamilia(StringUtils.trim(rs.getString("NOME_FAMILIA")));
		model.setItemLn(rs.getBigDecimal("ITEM_LN"));
		model.setItem(rs.getBigDecimal("ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DECR_ITEM")));
		model.setEan(rs.getBigDecimal("EAN"));
		model.setDpto(StringUtils.trim(rs.getString("DEPTO")));
		model.setSetor(StringUtils.trim(rs.getString("SETOR")));
		model.setClal(StringUtils.trim(rs.getString("CLAL")));
		model.setLoca(StringUtils.trim(rs.getString("LOCA")));
		model.setPreco(rs.getBigDecimal("PRECO"));
		model.setWarr(StringUtils.trim(rs.getString("WARR")));
		model.setQtdEstoq(rs.getInt("QTD_EST"));
		model.setValor(rs.getBigDecimal("VALOR"));
		model.setPeso(rs.getBigDecimal("PESO"));
		model.setM3(rs.getBigDecimal("M3"));
		model.setKitTik21(StringUtils.trim(rs.getString("KIT_TIK21")));
		model.setComponenteMestre(StringUtils.trim(rs.getString("COMPONENTE_MESTRE")));
		return model;
	}
}