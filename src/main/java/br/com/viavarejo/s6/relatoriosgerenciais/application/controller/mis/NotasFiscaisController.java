package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.mis;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/mis")
public class NotasFiscaisController {

	@Autowired
	@Qualifier("NotaFiscal")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/notas-fiscais")
	public ResponseEntity<?> listar(
    		@RequestParam(name="dataInicio") String dataInicio,
    		@RequestParam(name="dataFim") String dataFim,
    		@RequestParam(name="filial") String filial,
    		@RequestParam(name="natureza") Integer natureza,
    		@RequestParam(name="empresa") Integer empresa,
    		@RequestParam(name="cnpj") Long cnpj,   	
			@RequestParam(name="tipoOp") String tipoOp) {
		
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("dataInicio", dataInicio);
		params.put("dataFim", dataFim);
		params.put("filial", filial);
		params.put("natureza", natureza);
		params.put("empresa", empresa);
		params.put("cnpj", cnpj);
		params.put("tipoOp", tipoOp);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.NOTAS_FISCAIS, params);
    }
}