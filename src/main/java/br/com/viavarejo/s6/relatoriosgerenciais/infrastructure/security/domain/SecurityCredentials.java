package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Kleiton Brito
 * Credenciais Utilizadas para realizar login
 */
public class SecurityCredentials implements Serializable {
	
	private static final long serialVersionUID = -3538825827290118730L;
	
	private int empresa;
	private int matricula;
	private String username;
	private String senha;
	private String tipoAtividade = "";
	private String tipoSeguranca = "S";
	private String sistema;
	private String transacao;
	private String ip;

	@JsonIgnore
	private String nome;
	@JsonIgnore
	private int filial;
	@JsonIgnore
	private Date expirationTime;
	
	// Construtores
	public SecurityCredentials() {
		
	}
	
	public SecurityCredentials(int empresa, int matricula, String senha, String tipoAtividade, String tipoSeguranca, String sistema, String transacao, String ip) {
		this.empresa = empresa;
		this.matricula = matricula;
		this.senha = senha;
		this.tipoAtividade = tipoAtividade;
		this.tipoSeguranca = tipoSeguranca;
		this.sistema = sistema;
		this.transacao = transacao;
		this.ip = ip;
	}

	// Getters && Setters
	@JsonAlias("empresa")
	@JsonProperty("codigoEmpresaOrigem")
	public int getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	@JsonProperty(value="tipoAtividadeFilial", defaultValue="tipoAtividade")
	public String getTipoAtividade() {
		return tipoAtividade;
	}
	public void setTipoAtividade(String tipoAtividade) {
		this.tipoAtividade = tipoAtividade;
	}
	public String getTipoSeguranca() {
		return tipoSeguranca;
	}
	public void setTipoSeguranca(String tipoSeguranca) {
		this.tipoSeguranca = tipoSeguranca;
	}
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getTransacao() {
		return transacao;
	}
	public void setTransacao(String transacao) {
		this.transacao = transacao;
	}
	@JsonProperty(value="ipEndereco", defaultValue="ip")
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getFilial() {
		return filial;
	}

	public void setFilial(int filial) {
		this.filial = filial;
	}
	

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	@Override
	public String toString() {
		return "SecurityCredentials [empresa=" + empresa + ", matricula=" + matricula + ", senha=" + senha
				+ ", tipoAtividade=" + tipoAtividade + ", tipoSeguranca=" + tipoSeguranca + ", sistema=" + sistema
				+ ", transacao=" + transacao + ", ip=" + ip + "]";
	}
}
