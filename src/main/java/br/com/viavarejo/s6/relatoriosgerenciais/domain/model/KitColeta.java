package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class KitColeta extends BaseModel<KitColeta> {

	private static final long serialVersionUID = 2980809813705196205L;

	private Integer codEmpresa;
	private Integer codtransport;
	private String nometransport;
	private Integer codfilial;
	private String tipoDoc;
	private Integer numDoc;
	private String codDocReenv;
	private Integer codSeqReenvKit;
	private String statusReenvKit;
	private Integer quantDanfe;
	private Integer quantNotfis;
	private Integer quantLaudo;
	private String nomeArqEnv;
	private Integer codRaizCgcTransp;
	private Integer codFilialCgcTransp;
	private Integer codDigitCgcTransp;
	private Integer quantDocKit;
	private Date dtIncKit;
	private String observ;
	private Date dtReenvKit;
	private Long nrEntrega;
	private Integer nota;
	private String serie;
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCodEmpresa() {
		return codEmpresa;
	}

	public void setCodEmpresa(Integer codEmpresa) {
		this.codEmpresa = codEmpresa;
	}

	public Integer getCodtransport() {
		return codtransport;
	}

	public void setCodtransport(Integer codtransport) {
		this.codtransport = codtransport;
	}

	public String getNometransport() {
		return nometransport;
	}

	public void setNometransport(String nometransport) {
		this.nometransport = nometransport;
	}

	public Integer getCodfilial() {
		return codfilial;
	}

	public void setCodfilial(Integer codfilial) {
		this.codfilial = codfilial;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public Integer getNumDoc() {
		return numDoc;
	}

	public void setNumDoc(Integer numDoc) {
		this.numDoc = numDoc;
	}

	public String getCodDocReenv() {
		return codDocReenv;
	}

	public void setCodDocReenv(String codDocReenv) {
		this.codDocReenv = codDocReenv;
	}

	public Integer getCodSeqReenvKit() {
		return codSeqReenvKit;
	}

	public void setCodSeqReenvKit(Integer codSeqReenvKit) {
		this.codSeqReenvKit = codSeqReenvKit;
	}

	public String getStatusReenvKit() {
		return statusReenvKit;
	}

	public void setStatusReenvKit(String statusReenvKit) {
		this.statusReenvKit = statusReenvKit;
	}

	public Date getDtReenvKit() {
		return dtReenvKit;
	}

	public void setDtReenvKit(Date dtReenvKit) {
		this.dtReenvKit = dtReenvKit;
	}

	public Integer getQuantDanfe() {
		return quantDanfe;
	}

	public void setQuantDanfe(Integer quantDanfe) {
		this.quantDanfe = quantDanfe;
	}

	public Integer getQuantNotfis() {
		return quantNotfis;
	}

	public void setQuantNotfis(Integer quantNotfis) {
		this.quantNotfis = quantNotfis;
	}

	public Integer getQuantLaudo() {
		return quantLaudo;
	}

	public void setQuantLaudo(Integer quantLaudo) {
		this.quantLaudo = quantLaudo;
	}

	public String getNomeArqEnv() {
		return nomeArqEnv;
	}

	public void setNomeArqEnv(String nomeArqEnv) {
		this.nomeArqEnv = nomeArqEnv;
	}

	public Integer getCodRaizCgcTransp() {
		return codRaizCgcTransp;
	}

	public void setCodRaizCgcTransp(Integer codRaizCgcTransp) {
		this.codRaizCgcTransp = codRaizCgcTransp;
	}

	public Integer getCodFilialCgcTransp() {
		return codFilialCgcTransp;
	}

	public void setCodFilialCgcTransp(Integer codFilialCgcTransp) {
		this.codFilialCgcTransp = codFilialCgcTransp;
	}

	public Integer getCodDigitCgcTransp() {
		return codDigitCgcTransp;
	}

	public void setCodDigitCgcTransp(Integer codDigitCgcTransp) {
		this.codDigitCgcTransp = codDigitCgcTransp;
	}

	public Integer getQuantDocKit() {
		return quantDocKit;
	}

	public void setQuantDocKit(Integer quantDocKit) {
		this.quantDocKit = quantDocKit;
	}

	public Date getDtIncKit() {
		return dtIncKit;
	}

	public void setDtIncKit(Date dtIncKit) {
		this.dtIncKit = dtIncKit;
	}

	public String getObserv() {
		return observ;
	}

	public void setObserv(String observ) {
		this.observ = observ;
	}

	public Long getNrEntrega() {
		return nrEntrega;
	}

	public void setNrEntrega(Long nrEntrega) {
		this.nrEntrega = nrEntrega;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	@Override
	public KitColeta map(ResultSet rs) throws SQLException {
		KitColeta model = new KitColeta();
		model.setId(rs.getRow());

		if (hasColumn(rs, "QTDE_TOT_DOCTOS")) {
			model.setNometransport(StringUtils.trim(rs.getString("NM_EMPFNE_RZS_SAI")));
			model.setCodtransport(rs.getInt("CD_FNE_OLN_SAI"));
			model.setCodEmpresa(rs.getInt("CD_EMPGCB_FNE_OLN_SAI"));
			model.setCodfilial(rs.getInt("CD_FIL_EMI_SAI"));
			model.setCodRaizCgcTransp(rs.getInt("CD_EMPFNE_CGC_RAI_SAI"));
			model.setCodFilialCgcTransp(rs.getInt("CD_FNE_CGC_FIL_SAI"));
			model.setCodDigitCgcTransp(rs.getInt("CD_FNE_CGC_DVR_SAI"));
			model.setQuantDocKit(rs.getInt("QTDE_TOT_DOCTOS"));
		} 
		else if (hasColumn(rs, "NR_ENTREGA")) {
			model.setCodtransport(rs.getInt("CD_FNE_OLN_SAI"));
			model.setNometransport(StringUtils.trim(rs.getString("NM_EMPFNE_RZS_SAI")));
			model.setCodEmpresa(rs.getInt("CD_EMPGCB_FNE_OLN_SAI"));
			model.setCodfilial(rs.getInt("CD_FIL_EMI_SAI"));
			model.setTipoDoc(rs.getString("ST_EKCLT_TIP_DOC_SAI"));
			model.setNumDoc(rs.getInt("CD_EKCLT_NUM_DOC_SAI"));
			model.setDtIncKit(rs.getDate("TS_EKCLT_INC_SAI"));
			model.setQuantDanfe(rs.getInt("QT_EKCLT_DAN_SAI"));
			model.setQuantNotfis(rs.getInt("QT_EKCLT_NFI_SAI"));
			model.setQuantLaudo(rs.getInt("QT_EKCLT_LAU_SAI"));
			model.setNrEntrega(rs.getLong("NR_ENTREGA"));
			model.setNota(rs.getInt("NOTA"));
			model.setSerie(rs.getString("NOTA_SERIE"));
		} 
		else if (hasColumn(rs, "ST_RKCLT_REN")) {
			model.setStatusReenvKit(rs.getString("ST_RKCLT_REN"));
			model.setDtReenvKit(rs.getDate("TS_RKCLT_STC_REN_SAI"));
			model.setCodDocReenv(rs.getString("CD_DOCREN_SAI"));
			model.setCodSeqReenvKit(rs.getInt("CD_RKCLT_SEQ_SAI"));
			model.setNomeArqEnv(rs.getString("NM_RKCLT_ARQ_SAI"));
			model.setObserv(rs.getString("DS_RKCLT_OBS_SAI"));
		}

		return model;
	}
}