package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class SubidaLote extends BaseModel<SubidaLote> {

	private static final long serialVersionUID = -8304698398054317676L;

	private Integer id;
	private String opcao;
	private String tipo;
	private Integer dsm;
	private String numDocumento;
	private String ocorrPControle;
	private String dataProc;
	private String horaProc;
	private String obs;
	private String msgRet;
	private int codRet;
	private boolean disableSel;
	private String func;
	private String hashCode;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOcorrPControle() {
		return ocorrPControle;
	}

	public void setOcorrPControle(String ocorrPControle) {
		this.ocorrPControle = ocorrPControle;
	}

	public String getDataProc() {
		return dataProc;
	}

	public String getDataProcPonto() {
		return this.dataProc != null ? this.dataProc.replace('/', '.') : this.dataProc;
	}

	public void setDataProc(String dataProc) {
		this.dataProc = dataProc;
	}

	public String getHoraProc() {
		return horaProc;
	}

	public void setHoraProc(String horaProc) {
		this.horaProc = horaProc;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public String getMsgRet() {
		return msgRet;
	}

	public void setMsgRet(String msgRet) {
		this.msgRet = msgRet;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public String getNumDocumento18() {
		return StringUtils.leftPad(this.getNumDocumento(), 18, '0');

	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public String getOpcao() {
		return opcao;
	}

	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getDsm() {
		return dsm;
	}

	public void setDsm(Integer dsm) {
		this.dsm = dsm;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public int getCodRet() {
		return codRet;
	}

	public void setCodRet(int codRet) {
		this.codRet = codRet;
	}

	public boolean isDisableSel() {
		return disableSel;
	}

	public void setDisableSel(boolean disableSel) {
		this.disableSel = disableSel;
	}

	public String getHashCode() {
		return this.hashCode;
	}
	
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public SubidaLote map(ResultSet rs) throws SQLException {
		SubidaLote model = new SubidaLote();
		model.setCodRet(rs.getInt("CD_RET"));
		model.setMsgRet(rs.getString("DS_MSG"));
		return model;
	}
}