package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DevolucaoCliente extends BaseModel<DevolucaoCliente> {

	private static final long serialVersionUID = 4156019672612751506L;

	private Integer filial;
	private String pedido;
	private Integer notaOriginal;
	private String serieOriginal;
	private Integer notaEntrada;
	private String serieEntrada;
	private Date dataFiscal;
	private Integer item;
	private String descricao;
	private String setor;
	private String descLaudoCategoria;
	private String descLaudoAssunto;
	private String motivoDevolucao;
	private Integer qtde;
	private BigDecimal cmvUnitario;
	private BigDecimal cmvTotal;

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	
	public String getPedido() {
		return pedido;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public Integer getNotaOriginal() {
		return notaOriginal;
	}

	public void setNotaOriginal(Integer notaOriginal) {
		this.notaOriginal = notaOriginal;
	}

	public String getSerieOriginal() {
		return serieOriginal;
	}

	public void setSerieOriginal(String serieOriginal) {
		this.serieOriginal = serieOriginal;
	}

	public Integer getNotaEntrada() {
		return notaEntrada;
	}

	public void setNotaEntrada(Integer notaEntrada) {
		this.notaEntrada = notaEntrada;
	}

	public String getSerieEntrada() {
		return serieEntrada;
	}

	public void setSerieEntrada(String serieEntrada) {
		this.serieEntrada = serieEntrada;
	}

	public Date getDataFiscal() {
		return dataFiscal;
	}

	public void setDataFiscal(Date dataFiscal) {
		this.dataFiscal = dataFiscal;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getDescLaudoCategoria() {
		return descLaudoCategoria;
	}

	public void setDescLaudoCategoria(String descLaudoCategoria) {
		this.descLaudoCategoria = descLaudoCategoria;
	}

	public String getDescLaudoAssunto() {
		return descLaudoAssunto;
	}

	public void setDescLaudoAssunto(String descLaudoAssunto) {
		this.descLaudoAssunto = descLaudoAssunto;
	}

	
	public String getMotivoDevolucao() {
		return motivoDevolucao;
	}

	public void setMotivoDevolucao(String motivoDevolucao) {
		this.motivoDevolucao = motivoDevolucao;
	}

	public Integer getQtde() {
		return qtde;
	}

	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}

	public BigDecimal getCmvUnitario() {
		return cmvUnitario;
	}

	public void setCmvUnitario(BigDecimal cmvUnitario) {
		this.cmvUnitario = cmvUnitario;
	}

	public BigDecimal getCmvTotal() {
		return cmvTotal;
	}

	public void setCmvTotal(BigDecimal cmvTotal) {
		this.cmvTotal = cmvTotal;
	}

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public DevolucaoCliente map(ResultSet rs) throws SQLException {
		DevolucaoCliente model = new DevolucaoCliente();
		model.setFilial(rs.getInt("FILIAL"));
		model.setPedido(StringUtils.trim(rs.getString("PEDIDO")));
		model.setNotaOriginal(rs.getInt("NOTA_ORIGINAL"));
		model.setSerieOriginal(StringUtils.trim(rs.getString("SERIE_ORIGINAL")));
		model.setNotaEntrada(rs.getInt("NOTA_ENTRADA"));
		model.setSerieEntrada(StringUtils.trim(rs.getString("SERIE_ENTRADA")));
		model.setDataFiscal(rs.getDate("DATA_FISCAL"));
		model.setItem(rs.getInt("ITEM"));
		model.setDescricao(StringUtils.trim(rs.getString("DESCRICAO")));
		model.setSetor(StringUtils.trim(rs.getString("SETOR")));
		model.setDescLaudoCategoria(StringUtils.trim(rs.getString("DSC_LAUDO_CATEGORIA")));
		//model.setDescLaudoAssunto(StringUtils.trim(rs.getString("DSC_LAUDO_ASSUNTO")));
		model.setMotivoDevolucao(StringUtils.trim(rs.getString("MOTIVO_DEVOLUCAO")));
		model.setQtde(rs.getInt("QTDE"));
		model.setCmvUnitario(rs.getBigDecimal("CMV_UNITARIO"));
		model.setCmvTotal(rs.getBigDecimal("CMV_TOTAL"));
		return model;
	}
}