package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/logistica-reversa")
public class DevolucoesPendentesController<T> {

	@Autowired
	@Qualifier("DevolucoesPendentes")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/devolucoes-pendentes")
	public ResponseEntity<?> listar(@RequestParam(name="dataInicio") String dataInicio, @RequestParam(name="dataFim") String dataFim) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
 		params.put("dataInicio", dataInicio);
 		params.put("dataFim", dataFim);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.DEVOLUCOES_PENDENTES, params);
    }
}