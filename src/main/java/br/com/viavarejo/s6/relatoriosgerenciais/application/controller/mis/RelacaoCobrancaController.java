package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.mis;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/mis")
public class RelacaoCobrancaController {
	
	@Autowired
	@Qualifier("RelacaoCobranca")
  	private StoredProcedureRepository repository;
	
	
	@GetMapping("/relacao-cobranca")
	public ResponseEntity<?> listar(
    		@RequestParam(name="acao") String acao, 
			@RequestParam(name="empresa") Integer empresa,
			@RequestParam(name="filial") Integer filial,
			@RequestParam(name="statusPmr") String statusPmr,
			@RequestParam(name="codPosto") Integer codPosto, 
			@RequestParam(name="qtdDias") Integer qtdDias,
			@RequestParam(name="dataInicio") String dataInicio,
			@RequestParam(name="dataFim") String dataFim,
			@RequestParam(name="codFornecedor") Integer codFornecedor
			
			
			) {
		
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("acao", acao);
		params.put("empresa", empresa);
		params.put("filial", filial);
		params.put("statusPmr", statusPmr);
		params.put("codPosto", codPosto);
		params.put("qtdDias", qtdDias);
		params.put("dataInicio", dataInicio);
		params.put("dataFim", dataFim);
		params.put("codFornecedor", codFornecedor);
		
		
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.RELACAO_COBRANCA, params);
    }	
}
