package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class RecebimentoFiscal extends BaseModel<RecebimentoFiscal> {

	private static final long serialVersionUID = -5688487300548192738L;

	private Integer armazem;
	private Integer sku;
	private Date dataLancto;
	private String asn;
	private String linhaAsn;
	private String operadorFiscal;
	private String descricao;
	private BigDecimal ean;
	private String nomeDepart;
	private String nomeSetor;
	private Date dataRecebto;
	private String qtdPcsLanc;
	private String qtdePcsRecebidas;
	private String pedidoSite;
	private String linhaRefFiscal;
	private String cnpj;
	private String fornecedor;
	private String estado;
	private Integer notaFiscal;
	private String serieNf;
	private Date dtEmissaoEnt;
	private String cfop;
	private String qtde;
	private BigDecimal valorUnitario;
	private String nfSaida;
	private String serieNfSaida;
	private String dtEmissaoSaida;
	private String descTrasp;
	private String placaFornecedor;

	public Integer getArmazem() {
		return armazem;
	}

	public void setArmazem(Integer armazem) {
		this.armazem = armazem;
	}

	public Integer getSku() {
		return sku;
	}

	public void setSku(Integer sku) {
		this.sku = sku;
	}

	public Date getDataLancto() {
		return dataLancto;
	}

	public void setDataLancto(Date dataLancto) {
		this.dataLancto = dataLancto;
	}

	public String getAsn() {
		return asn;
	}

	public void setAsn(String asn) {
		this.asn = asn;
	}

	public String getLinhaAsn() {
		return linhaAsn;
	}

	public void setLinhaAsn(String linhaAsn) {
		this.linhaAsn = linhaAsn;
	}

	public String getOperadorFiscal() {
		return operadorFiscal;
	}

	public void setOperadorFiscal(String operadorFiscal) {
		this.operadorFiscal = operadorFiscal;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getEan() {
		return ean;
	}

	public void setEan(BigDecimal ean) {
		this.ean = ean;
	}

	public String getNomeDepart() {
		return nomeDepart;
	}

	public void setNomeDepart(String nomeDepart) {
		this.nomeDepart = nomeDepart;
	}

	public String getNomeSetor() {
		return nomeSetor;
	}

	public void setNomeSetor(String nomeSetor) {
		this.nomeSetor = nomeSetor;
	}

	public Date getDataRecebto() {
		return dataRecebto;
	}

	public void setDataRecebto(Date dataRecebto) {
		this.dataRecebto = dataRecebto;
	}

	public String getQtdPcsLanc() {
		return qtdPcsLanc;
	}

	public void setQtdPcsLanc(String qtdPcsLanc) {
		this.qtdPcsLanc = qtdPcsLanc;
	}

	public String getQtdePcsRecebidas() {
		return qtdePcsRecebidas;
	}

	public void setQtdePcsRecebidas(String qtdePcsRecebidas) {
		this.qtdePcsRecebidas = qtdePcsRecebidas;
	}

	public String getPedidoSite() {
		return pedidoSite;
	}

	public void setPedidoSite(String pedidoSite) {
		this.pedidoSite = pedidoSite;
	}

	public String getLinhaRefFiscal() {
		return linhaRefFiscal;
	}

	public void setLinhaRefFiscal(String linhaRefFiscal) {
		this.linhaRefFiscal = linhaRefFiscal;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getNotaFiscal() {
		return notaFiscal;
	}

	public void setNotaFiscal(Integer notaFiscal) {
		this.notaFiscal = notaFiscal;
	}

	public String getSerieNf() {
		return serieNf;
	}

	public void setSerieNf(String serieNf) {
		this.serieNf = serieNf;
	}

	public Date getDtEmissaoEnt() {
		return dtEmissaoEnt;
	}

	public void setDtEmissaoEnt(Date dtEmissaoEnt) {
		this.dtEmissaoEnt = dtEmissaoEnt;
	}

	public String getCfop() {
		return cfop;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public String getQtde() {
		return qtde;
	}

	public void setQtde(String qtde) {
		this.qtde = qtde;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public String getNfSaida() {
		return nfSaida;
	}

	public void setNfSaida(String nfSaida) {
		this.nfSaida = nfSaida;
	}

	public String getSerieNfSaida() {
		return serieNfSaida;
	}

	public void setSerieNfSaida(String serieNfSaida) {
		this.serieNfSaida = serieNfSaida;
	}

	public String getDtEmissaoSaida() {
		return dtEmissaoSaida;
	}

	public void setDtEmissaoSaida(String dtEmissaoSaida) {
		this.dtEmissaoSaida = dtEmissaoSaida;
	}

	public String getDescTrasp() {
		return descTrasp;
	}

	public void setDescTrasp(String descTrasp) {
		this.descTrasp = descTrasp;
	}

	public String getPlacaFornecedor() {
		return placaFornecedor;
	}

	public void setPlacaFornecedor(String placaFornecedor) {
		this.placaFornecedor = placaFornecedor;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public RecebimentoFiscal map(ResultSet rs) throws SQLException {
		RecebimentoFiscal model = new RecebimentoFiscal();
		model.setArmazem(rs.getInt("DESC_ARMAZEM"));
		model.setSku(rs.getInt("SKU"));
		model.setDataLancto(rs.getDate("DATA_LANCTO"));
		model.setAsn(StringUtils.trim(rs.getString("ASN")));
		model.setLinhaAsn(StringUtils.trim(rs.getString("LINHA_ASN")));
		model.setOperadorFiscal(StringUtils.trim(rs.getString("OPERADOR_FISCAL")));
		model.setDescricao(StringUtils.trim(rs.getString("DESCRICAO")));
		model.setEan(rs.getBigDecimal("EAN"));
		model.setNomeDepart(StringUtils.trim(rs.getString("NOME_DEPART")));
		model.setNomeSetor(StringUtils.trim(rs.getString("NOME_SETOR")));
		model.setDataRecebto(rs.getDate("DATA_RECBTO"));
		model.setQtdPcsLanc(StringUtils.trim(rs.getString("QTDE_PCS_LANC")));
		model.setQtdePcsRecebidas(StringUtils.trim(rs.getString("QTDE_PCS_RECEBIDAS")));
		model.setPedidoSite(StringUtils.trim(rs.getString("PEDIDO_SITE")));
		model.setLinhaRefFiscal(StringUtils.trim(rs.getString("LINHA_REF_FISCAL")));
		model.setCnpj(StringUtils.trim(rs.getString("CNPJ")));
		model.setFornecedor(StringUtils.trim(rs.getString("FORNECEDOR")));
		model.setEstado(StringUtils.trim(rs.getString("ESTADO")));
		model.setNotaFiscal(rs.getInt("NOTA_FISCAL"));
		model.setSerieNf(StringUtils.trim(rs.getString("SERIE_NF")));
		model.setDtEmissaoEnt(rs.getDate("DT_EMISSAO_ENT"));
		model.setCfop(StringUtils.trim(rs.getString("CFOP")));
		model.setQtde(StringUtils.trim(rs.getString("QTDE")));
		model.setValorUnitario(rs.getBigDecimal("VALOR_UNITARIO"));
		model.setNfSaida(StringUtils.trim(rs.getString("NF_SAIDA")));
		model.setSerieNfSaida(StringUtils.trim(rs.getString("SERIE_NF_SAIDA")));
		model.setDtEmissaoSaida(StringUtils.trim(rs.getString("DT_EMISSAO_SAIDA")));
		model.setDescTrasp(StringUtils.trim(rs.getString("DESCR_TRANSP")));
		model.setPlacaFornecedor(StringUtils.trim(rs.getString("PLACA_FORNECEDOR")));
		return model;
	}
}