package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class InstanciaPendente extends BaseModel<InstanciaPendente> {

	private static final long serialVersionUID = 1L;

	private BigDecimal entrega;
	private Integer pedCd;
	private Integer pedCdDms;
	private Integer nf;
	private String nfSerie;
	private Integer empFil;
	private Integer fil;
	private String dtEmiss;
	private String tipo;
	private String atend;
	private String restituicao;
	private String marca;
	private String dsteg;
	private String cdOcorr;
	private String dsOcorr;
	private String dtOcorr;
	private String userBx;
	private String nomeTrp;

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public Integer getPedCd() {
		return pedCd;
	}

	public void setPedCd(Integer pedCd) {
		this.pedCd = pedCd;
	}

	public Integer getPedCdDms() {
		return pedCdDms;
	}

	public void setPedCdDms(Integer pedCdDms) {
		this.pedCdDms = pedCdDms;
	}

	public Integer getNf() {
		return nf;
	}

	public void setNf(Integer nf) {
		this.nf = nf;
	}

	public String getNfSerie() {
		return nfSerie;
	}

	public void setNfSerie(String nfSerie) {
		this.nfSerie = nfSerie;
	}

	public Integer getEmpFil() {
		return empFil;
	}

	public void setEmpFil(Integer empFil) {
		this.empFil = empFil;
	}

	public Integer getFil() {
		return fil;
	}

	public void setFil(Integer fil) {
		this.fil = fil;
	}

	public String getDtEmiss() {
		return dtEmiss;
	}

	public void setDtEmiss(String dtEmiss) {
		this.dtEmiss = dtEmiss;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAtend() {
		return atend;
	}

	public void setAtend(String atend) {
		this.atend = atend;
	}

	public String getRestituicao() {
		return restituicao;
	}

	public void setRestituicao(String restituicao) {
		this.restituicao = restituicao;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getDsteg() {
		return dsteg;
	}

	public void setDsteg(String dsteg) {
		this.dsteg = dsteg;
	}

	public String getCdOcorr() {
		return cdOcorr;
	}

	public void setCdOcorr(String cdOcorr) {
		this.cdOcorr = cdOcorr;
	}

	public String getDsOcorr() {
		return dsOcorr;
	}

	public void setDsOcorr(String dsOcorr) {
		this.dsOcorr = dsOcorr;
	}

	public String getDtOcorr() {
		return dtOcorr;
	}

	public void setDtOcorr(String dtOcorr) {
		this.dtOcorr = dtOcorr;
	}

	public String getUserBx() {
		return userBx;
	}

	public void setUserBx(String userBx) {
		this.userBx = userBx;
	}

	public String getNomeTrp() {
		return nomeTrp;
	}

	public void setNomeTrp(String nomeTrp) {
		this.nomeTrp = nomeTrp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public InstanciaPendente map(ResultSet rs) throws SQLException {
		InstanciaPendente model = new InstanciaPendente();
		
		model.setEntrega(rs.getBigDecimal("REL01_ENTREGA"));
		model.setPedCd(rs.getInt("REL01_PED_CD"));
		model.setPedCdDms(rs.getInt("REL01_PED_CD_DSM"));
		model.setNf(rs.getInt("REL01_NF"));
		model.setNfSerie(StringUtils.trim(rs.getString("REL01_NF_SERIE")));
		model.setEmpFil(rs.getInt("REL01_EMPFIL"));
		model.setFil(rs.getInt("REL01_FIL"));
		model.setDtEmiss(StringUtils.trim(rs.getString("REL01_DTEMISS")));
		model.setTipo(StringUtils.trim(rs.getString("REL01_TIPO")));
		model.setAtend(StringUtils.trim(rs.getString("REL01_ATEND")));
		model.setRestituicao(StringUtils.trim(rs.getString("REL01_RESTITUICAO")));
		model.setMarca(StringUtils.trim(rs.getString("REL01_MARCA")));
		model.setDsteg(StringUtils.trim(rs.getString("REL01_DSETG")));
		model.setCdOcorr(StringUtils.trim(rs.getString("REL01_CDOCORR")));
		model.setDsOcorr(StringUtils.trim(rs.getString("REL01_DSOCORR")));
		model.setDtOcorr(StringUtils.trim(rs.getString("REL01_DTOCORR")));
		model.setUserBx(StringUtils.trim(rs.getString("REL01_USERBX")));
		model.setNomeTrp(StringUtils.trim(rs.getString("REL01_NOMETRP")));
		
		
		return model;
	}

}
