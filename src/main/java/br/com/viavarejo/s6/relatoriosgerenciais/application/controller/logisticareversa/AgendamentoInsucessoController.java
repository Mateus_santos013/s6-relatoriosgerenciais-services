package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/logistica-reversa")
public class AgendamentoInsucessoController {
	
	@Autowired
	@Qualifier("AgendamentoInsucesso")
	private StoredProcedureRepository repository;
	
	@GetMapping("/agendamento-insucesso")
	public ResponseEntity<?> listar(@RequestParam(name="tipoSolicitacao") String tipoSolicitacao, @RequestParam(name="entregas") String entregas) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("tipoSolicitacao", tipoSolicitacao);
		params.put("entregas", entregas);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.AGENDAMENTO_INSUCESSO, params);
	}
}