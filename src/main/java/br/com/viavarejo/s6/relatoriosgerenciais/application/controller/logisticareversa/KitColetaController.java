package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/logistica-reversa/kit")
public class KitColetaController {

	@Autowired
	@Qualifier("KitColeta")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/coleta")
	public ResponseEntity<?> listar(
    		@RequestParam(name="opcao") String opcao,
    		@RequestParam(name="codEmpresa") Integer codEmpresa,
    		@RequestParam(name="transportador") Integer transportador,
    		@RequestParam(name="dataInicio") String dataInicio,
    		@RequestParam(name="dataFim") String dataFim,
    		@RequestParam(name="tipoSolic") String tipoSolic,
    		@RequestParam(name="numDoc") Integer numDoc,
    		@RequestParam(name="numFilial") Integer numFilial) {
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
 		params.put("opcao", opcao);
 		params.put("codEmpresa", codEmpresa);
 		params.put("transportador", transportador);
 		params.put("dataInicio", dataInicio);
 		params.put("dataFim", dataFim);
 		params.put("tipoSolic", tipoSolic);
 		params.put("numDoc", numDoc);
 		params.put("numFilial", numFilial);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.KIT_COLETA, params);
    }
}