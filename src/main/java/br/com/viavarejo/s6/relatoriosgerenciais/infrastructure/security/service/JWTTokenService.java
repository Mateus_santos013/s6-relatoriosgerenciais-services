package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.service;

import static br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTSecurityConstants.EXPIRATION_TIME;
import static br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTSecurityConstants.HEADER_STRING;
import static br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTSecurityConstants.SECRET;
import static br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.JWTSecurityConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.ApplicationContextProvider;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.security.domain.SecurityCredentials;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class JWTTokenService {
	
	public static void addAuthentication(HttpServletResponse response, Authentication authentication) {
		
		Map<String, Object> claims = new HashMap<String, Object>();
		SecurityCredentials creds = (SecurityCredentials) authentication.getPrincipal();
		
		Date expiration = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
		
		claims.put("username", creds.getUsername());
		claims.put("senha", creds.getSenha());
		claims.put("nome", creds.getNome());
		claims.put("matricula", creds.getMatricula());
		claims.put("empresa", creds.getEmpresa());
		claims.put("filial", creds.getFilial());
		claims.put("expirationTime", expiration.getTime());
		
		String JWT = Jwts.builder()
			.setClaims(claims)
			.setExpiration(expiration)
			.signWith(SignatureAlgorithm.HS512, SECRET).compact();

		String token = TOKEN_PREFIX + " " + JWT;
		response.addHeader(HEADER_STRING, token);
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.createObjectNode();
		node.put("token", token);
		
		try {
			response.getOutputStream().print(mapper.writeValueAsString(node));
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	public static Claims getClaims(String token) {
		return Jwts.parser()
				.setSigningKey(SECRET)
				.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
				.getBody();
	}
	
	public static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		
		if (token != null) {
			Claims claims = getClaims(token);
			
			String username = claims.get("username").toString();
			String password = claims.get("senha").toString();
			
			Integer codigoEmpresa = Integer.parseInt(claims.get("empresa").toString());
			Integer codigoFuncionario = Integer.parseInt(claims.get("matricula").toString());
			List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
			SimpleSecurityPermissionService service = ApplicationContextProvider.getApplicationContext().getBean(SimpleSecurityPermissionService.class);
			try {
				authorities = service.getRoles(codigoEmpresa, codigoFuncionario);	
			}
			catch(Exception e) {
				e.printStackTrace();
				// TODO TRATAR
			}
			
			return claims != null ? new UsernamePasswordAuthenticationToken(username, password, authorities) : null;
		}
		
		return null;
	}
}