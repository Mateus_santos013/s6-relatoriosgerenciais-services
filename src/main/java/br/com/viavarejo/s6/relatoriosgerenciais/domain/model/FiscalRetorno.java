package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class FiscalRetorno extends BaseModel<FiscalRetorno> {

	private static final long serialVersionUID = 2421890687144988239L;

	private Integer filial;
	private Date dataFiscal;
	private Integer nfRms;
	private String serieRms;
	private String cdCfop;
	private String dsCfop;
	private Integer nfRet;
	private String serieRet;
	private Integer cnpjRai;
	private Integer cnpjFil;
	private Integer cnpjDvr;
	private String dsFornecedor;
	private Integer cdItem;
	private String dsItem;
	private BigDecimal vrItem;
	private Integer qtdSaldoDisp;
	private Integer qtRms;
	private Integer qtRet;
	private BigDecimal vrTotal;
	
	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public Date getDataFiscal() {
		return dataFiscal;
	}

	public void setDataFiscal(Date dataFiscal) {
		this.dataFiscal = dataFiscal;
	}

	public Integer getNfRms() {
		return nfRms;
	}

	public void setNfRms(Integer nfRms) {
		this.nfRms = nfRms;
	}

	public String getSerieRms() {
		return serieRms;
	}

	public void setSerieRms(String serieRms) {
		this.serieRms = serieRms;
	}

	public String getCdCfop() {
		return cdCfop;
	}

	public void setCdCfop(String cdCfop) {
		this.cdCfop = cdCfop;
	}

	public String getDsCfop() {
		return dsCfop;
	}

	public void setDsCfop(String dsCfop) {
		this.dsCfop = dsCfop;
	}

	public Integer getNfRet() {
		return nfRet;
	}

	public void setNfRet(Integer nfRet) {
		this.nfRet = nfRet;
	}

	public String getSerieRet() {
		return serieRet;
	}

	public void setSerieRet(String serieRet) {
		this.serieRet = serieRet;
	}

	public Integer getCnpjRai() {
		return cnpjRai;
	}

	public void setCnpjRai(Integer cnpjRai) {
		this.cnpjRai = cnpjRai;
	}

	public Integer getCnpjFil() {
		return cnpjFil;
	}

	public void setCnpjFil(Integer cnpjFil) {
		this.cnpjFil = cnpjFil;
	}

	public Integer getCnpjDvr() {
		return cnpjDvr;
	}

	public void setCnpjDvr(Integer cnpjDvr) {
		this.cnpjDvr = cnpjDvr;
	}

	public String getDsFornecedor() {
		return dsFornecedor;
	}

	public void setDsFornecedor(String dsFornecedor) {
		this.dsFornecedor = dsFornecedor;
	}

	public Integer getCdItem() {
		return cdItem;
	}

	public void setCdItem(Integer cdItem) {
		this.cdItem = cdItem;
	}

	public String getDsItem() {
		return dsItem;
	}

	public void setDsItem(String dsItem) {
		this.dsItem = dsItem;
	}

	public BigDecimal getVrItem() {
		return vrItem;
	}

	public void setVrItem(BigDecimal vrItem) {
		this.vrItem = vrItem;
	}

	public Integer getQtdSaldoDisp() {
		return qtdSaldoDisp;
	}

	public void setQtdSaldoDisp(Integer qtdSaldoDisp) {
		this.qtdSaldoDisp = qtdSaldoDisp;
	}

	public Integer getQtRms() {
		return qtRms;
	}

	public void setQtRms(Integer qtRms) {
		this.qtRms = qtRms;
	}

	public Integer getQtRet() {
		return qtRet;
	}

	public void setQtRet(Integer qtRet) {
		this.qtRet = qtRet;
	}

	public BigDecimal getVrTotal() {
		return vrTotal;
	}

	public void setVrTotal(BigDecimal vrTotal) {
		this.vrTotal = vrTotal;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public FiscalRetorno map(ResultSet rs) throws SQLException {
		FiscalRetorno model = new FiscalRetorno();
		model.setFilial(rs.getInt("FILIAL"));
		model.setDataFiscal(rs.getDate("DT_FISCAL"));
		model.setNfRms(rs.getInt("NF_RMS"));
		model.setSerieRms(StringUtils.trim(rs.getString("SERIE_RMS")));
		model.setCdCfop(StringUtils.trim(rs.getString("CD_CFOP")));
		model.setDsCfop(StringUtils.trim(rs.getString("DS_CFOP")));
		model.setNfRet(rs.getInt("NF_RET"));
		model.setSerieRet(StringUtils.trim(rs.getString("SERIE_RET")));
		model.setCnpjRai(rs.getInt("CNPJ_RAI"));
		model.setCnpjFil(rs.getInt("CNPJ_FIL"));
		model.setCnpjDvr(rs.getInt("CNPJ_DVR"));
		model.setDsFornecedor(StringUtils.trim(rs.getString("DS_FORNECEDOR")));
		model.setCdItem(rs.getInt("CD_ITEM"));
		model.setDsItem(StringUtils.trim(rs.getString("DS_ITEM")));
		model.setVrItem(rs.getBigDecimal("VR_ITEM"));
		model.setQtdSaldoDisp(rs.getInt("QT_SDO_DISP"));
		model.setQtRms(rs.getInt("QT_RMS"));
		model.setQtRet(rs.getInt("QT_RET"));
		model.setVrTotal(rs.getBigDecimal("VR_TOTAL"));
		return model;
	}
}