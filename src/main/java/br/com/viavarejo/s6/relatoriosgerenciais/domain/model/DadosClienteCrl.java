package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class DadosClienteCrl extends BaseModel<DadosClienteCrl> {

	private static final long serialVersionUID = 1L;

	private Integer dpdEtg;
	private Integer filDpo;
	private String bnd;
	private String pedidoOn;
	private String entrega;
	private String seqPedOn;
	private String cdPvemcr;
	private String cdPveDsm;
	private String tipo;
	private String cdSetmcr;
	private String nmSetmcr;
	private String cdMcr;
	private String dsNmcr;
	private String qtdItem;
	private BigDecimal vlrMcr;
	private String dtPromet;
	private String nmCli;
	private String cpfCnpj;
	private String logadouro;
	private String logadouroNum;
	private String logadouroCpl;
	private String bairro;
	private String municipio;
	private String cep;
	private String uf;
	private String email;
	private String tel1;
	private String tel2;
	private String tel3;
	private String tel4;
	private String transTime;
	private String nf;
	private String nfSer;
	private String dataEmissao;
	private String cnpjTrp;
	private String transportador;
	private String ultimaOcorr;
	private String descOcorrencia;
	private String dataOcorrencia;
	private String horaOcorrencia;
	private String dtProc;
	private String hrProc;
	private String cdEmpTra;
	private String cdFilTra;
	private String cdNtabm;
	private String cdNtabmSer;
	private String cdNtabmAso;
	private String endFil;

	public Integer getDpdEtg() {
		return dpdEtg;
	}

	public void setDpdEtg(Integer dpdEtg) {
		this.dpdEtg = dpdEtg;
	}

	public Integer getFilDpo() {
		return filDpo;
	}

	public void setFilDpo(Integer filDpo) {
		this.filDpo = filDpo;
	}

	public String getBnd() {
		return bnd;
	}

	public void setBnd(String bnd) {
		this.bnd = bnd;
	}

	public String getPedidoOn() {
		return pedidoOn;
	}

	public void setPedidoOn(String pedidoOn) {
		this.pedidoOn = pedidoOn;
	}

	public String getEntrega() {
		return entrega;
	}

	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}

	public String getSeqPedOn() {
		return seqPedOn;
	}

	public void setSeqPedOn(String seqPedOn) {
		this.seqPedOn = seqPedOn;
	}

	public String getCdPvemcr() {
		return cdPvemcr;
	}

	public void setCdPvemcr(String cdPvemcr) {
		this.cdPvemcr = cdPvemcr;
	}

	public String getCdPveDsm() {
		return cdPveDsm;
	}

	public void setCdPveDsm(String cdPveDsm) {
		this.cdPveDsm = cdPveDsm;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCdSetmcr() {
		return cdSetmcr;
	}

	public void setCdSetmcr(String cdSetmcr) {
		this.cdSetmcr = cdSetmcr;
	}

	public String getNmSetmcr() {
		return nmSetmcr;
	}

	public void setNmSetmcr(String nmSetmcr) {
		this.nmSetmcr = nmSetmcr;
	}

	public String getCdMcr() {
		return cdMcr;
	}

	public void setCdMcr(String cdMcr) {
		this.cdMcr = cdMcr;
	}

	public String getDsNmcr() {
		return dsNmcr;
	}

	public void setDsNmcr(String dsNmcr) {
		this.dsNmcr = dsNmcr;
	}

	public String getQtdItem() {
		return qtdItem;
	}

	public void setQtdItem(String qtdItem) {
		this.qtdItem = qtdItem;
	}

	public BigDecimal getVlrMcr() {
		return vlrMcr;
	}

	public void setVlrMcr(BigDecimal vlrMcr) {
		this.vlrMcr = vlrMcr;
	}

	public String getDtPromet() {
		return dtPromet;
	}

	public void setDtPromet(String dtPromet) {
		this.dtPromet = dtPromet;
	}

	public String getNmCli() {
		return nmCli;
	}

	public void setNmCli(String nmCli) {
		this.nmCli = nmCli;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getLogadouro() {
		return logadouro;
	}

	public void setLogadouro(String logadouro) {
		this.logadouro = logadouro;
	}

	public String getLogadouroNum() {
		return logadouroNum;
	}

	public void setLogadouroNum(String logadouroNum) {
		this.logadouroNum = logadouroNum;
	}

	public String getLogadouroCpl() {
		return logadouroCpl;
	}

	public void setLogadouroCpl(String logadouroCpl) {
		this.logadouroCpl = logadouroCpl;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getTel3() {
		return tel3;
	}

	public void setTel3(String tel3) {
		this.tel3 = tel3;
	}

	public String getTel4() {
		return tel4;
	}

	public void setTel4(String tel4) {
		this.tel4 = tel4;
	}

	public String getTransTime() {
		return transTime;
	}

	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}

	public String getNf() {
		return nf;
	}

	public void setNf(String nf) {
		this.nf = nf;
	}

	public String getNfSer() {
		return nfSer;
	}

	public void setNfSer(String nfSer) {
		this.nfSer = nfSer;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getCnpjTrp() {
		return cnpjTrp;
	}

	public void setCnpjTrp(String cnpjTrp) {
		this.cnpjTrp = cnpjTrp;
	}

	public String getTransportador() {
		return transportador;
	}

	public void setTransportador(String transportador) {
		this.transportador = transportador;
	}

	public String getUltimaOcorr() {
		return ultimaOcorr;
	}

	public void setUltimaOcorr(String ultimaOcorr) {
		this.ultimaOcorr = ultimaOcorr;
	}

	public String getDescOcorrencia() {
		return descOcorrencia;
	}

	public void setDescOcorrencia(String descOcorrencia) {
		this.descOcorrencia = descOcorrencia;
	}

	public String getDataOcorrencia() {
		return dataOcorrencia;
	}

	public void setDataOcorrencia(String dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}

	public String getHoraOcorrencia() {
		return horaOcorrencia;
	}

	public void setHoraOcorrencia(String horaOcorrencia) {
		this.horaOcorrencia = horaOcorrencia;
	}

	public String getDtProc() {
		return dtProc;
	}

	public void setDtProc(String dtProc) {
		this.dtProc = dtProc;
	}

	public String getHrProc() {
		return hrProc;
	}

	public void setHrProc(String hrProc) {
		this.hrProc = hrProc;
	}

	public String getCdEmpTra() {
		return cdEmpTra;
	}

	public void setCdEmpTra(String cdEmpTra) {
		this.cdEmpTra = cdEmpTra;
	}

	public String getCdFilTra() {
		return cdFilTra;
	}

	public void setCdFilTra(String cdFilTra) {
		this.cdFilTra = cdFilTra;
	}

	public String getCdNtabm() {
		return cdNtabm;
	}

	public void setCdNtabm(String cdNtabm) {
		this.cdNtabm = cdNtabm;
	}

	public String getCdNtabmSer() {
		return cdNtabmSer;
	}

	public void setCdNtabmSer(String cdNtabmSer) {
		this.cdNtabmSer = cdNtabmSer;
	}

	public String getCdNtabmAso() {
		return cdNtabmAso;
	}

	public void setCdNtabmAso(String cdNtabmAso) {
		this.cdNtabmAso = cdNtabmAso;
	}

	public String getEndFil() {
		return endFil;
	}

	public void setEndFil(String endFil) {
		this.endFil = endFil;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public DadosClienteCrl map(ResultSet rs) throws SQLException {
		DadosClienteCrl model = new DadosClienteCrl();
		
		model.setDpdEtg(rs.getInt("CD_EMPGCB_DPO_ETG"));
		model.setFilDpo(rs.getInt("CD_FIL_DPO_ETG"));
		model.setBnd(StringUtils.trim(rs.getString("BND")));
		model.setPedidoOn(StringUtils.trim(rs.getString("ENTREGA")));
		model.setSeqPedOn(StringUtils.trim(rs.getString("SEQ_PED_ON")));
		model.setCdPvemcr(StringUtils.trim(rs.getString("CD_PVEMCR")));
		model.setCdPveDsm(StringUtils.trim(rs.getString("CD_PVEMCR_DSM")));
		model.setTipo(rs.getString("TIPO"));
		model.setCdSetmcr(StringUtils.trim(rs.getString("CD_SETMCR")));
		model.setNmSetmcr(StringUtils.trim(rs.getString("NM_SETMCR")));
		model.setCdMcr(StringUtils.trim(rs.getString("CD_MCR")));
		model.setDsNmcr(StringUtils.trim(rs.getString("DS_NM_MCR")));
		model.setQtdItem(StringUtils.trim(rs.getString("QTD_ITEM")));
		model.setVlrMcr(rs.getBigDecimal("VLR_MERC"));
		model.setDtPromet(StringUtils.trim(rs.getString("DT_PROMET")));
		model.setNmCli(StringUtils.trim(rs.getString("NMCLI")));
		model.setCpfCnpj(StringUtils.trim(rs.getString("CPFCNPJ")));
		model.setLogadouro(StringUtils.trim(rs.getString("LOGRADOURO")));
		model.setLogadouroNum(StringUtils.trim(rs.getString("LOGRADOURO_NUM")));
		model.setLogadouroCpl(StringUtils.trim(rs.getString("LOGRADOURO_CPL")));
		model.setBairro(StringUtils.trim(rs.getString("BAIRRO")));
		model.setMunicipio(StringUtils.trim(rs.getString("MUNICIPIO")));
		model.setCep(StringUtils.trim(rs.getString("CEP")));
		model.setUf(StringUtils.trim(rs.getString("UF")));
		model.setEmail(StringUtils.trim(rs.getString("EMAIL")));
		model.setTel1(StringUtils.trim(rs.getString("TEL1")));
		model.setTel2(StringUtils.trim(rs.getString("TEL2")));
		model.setTel3(StringUtils.trim(rs.getString("TEL3")));
		model.setTel4(StringUtils.trim(rs.getString("TEL4")));
		model.setTransTime(StringUtils.trim(rs.getString("TRANS_TIME")));
		model.setNf(StringUtils.trim(rs.getString("NF")));
		model.setNfSer(StringUtils.trim(rs.getString("NF_SER")));
		model.setDataEmissao(StringUtils.trim(rs.getString("DATA_EMISSAO_NF")));
		model.setCnpjTrp(StringUtils.trim(rs.getString("CNPJ_TRP")));
		model.setTransportador(StringUtils.trim(rs.getString("TRANSPORTADOR")));
		model.setUltimaOcorr(StringUtils.trim(rs.getString("ULTIMA_OCORR")));
		model.setDescOcorrencia(StringUtils.trim(rs.getString("DESC_OCORRENCIA")));
		model.setDataOcorrencia(StringUtils.trim(rs.getString("DATA_OCORRENCIA")));
		model.setHoraOcorrencia(StringUtils.trim(rs.getString("HORA_OCORRENCIA")));
		model.setDtProc(StringUtils.trim(rs.getString("DT_PROC")));
		model.setHrProc(StringUtils.trim(rs.getString("HR_PROC")));
		model.setCdEmpTra(StringUtils.trim(rs.getString("CD_EMPGCB_TRA")));
		model.setCdFilTra(StringUtils.trim(rs.getString("CD_FIL_TRA")));
		model.setCdNtabm(StringUtils.trim(rs.getString("CD_NTABM")));
		model.setCdNtabmSer(StringUtils.trim(rs.getString("CD_NTABM_SER")));
		model.setCdNtabmAso(StringUtils.trim(rs.getString("CD_NTABM_CHA_ASO")));
		model.setEndFil(StringUtils.trim(rs.getString("END_FIL")));
		
		return model;
	}
}
