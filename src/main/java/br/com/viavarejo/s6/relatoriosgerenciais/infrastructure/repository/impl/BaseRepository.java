package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;

public abstract class BaseRepository {

	@Value("${db2.schema}")
	protected String SCHEMA;
	
	protected Class<?> clazz;
	
	public BaseRepository(Class<?> clazz) {
		this.clazz = clazz;
	}
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	
	public BaseRepository() {
	}
	
	protected String buildCall(StoredProcedures storedProcedure) {
		StringBuilder call = new StringBuilder();
		call.append("{ CALL ");
		
		if(SCHEMA != null && SCHEMA.length() > 0) {
			call.append(SCHEMA);
			call.append(".");
		}
		
		call.append(storedProcedure.getStoredProcedureName());
		call.append(" (");
		
		for(int x = 0; x < storedProcedure.getStoredProcedureParams(); x++) {
			if(x < storedProcedure.getStoredProcedureParams() -1) {
				call.append(" ?,");
			} 
			else {
				call.append(" ?");
			}
		}
		
		call.append(" ) }");
		
		return call.toString();
	}
	
	protected Object getMapperInstance() throws Exception {
		return clazz.newInstance();
	}
}