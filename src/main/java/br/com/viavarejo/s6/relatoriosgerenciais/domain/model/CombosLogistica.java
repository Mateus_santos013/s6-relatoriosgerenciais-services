package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class CombosLogistica extends BaseModel<CombosLogistica> {

	private static final long serialVersionUID = -769987911823341628L;

	private String departamento;
	private Integer codigoSetor;
	private Integer codigoUnidade;
	private String unidadeNegocio;
	private Integer transportador;
	private String ocorrencia;
	private Integer codigoOcorrencia;
	private String ponto;

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Integer getCodigoSetor() {
		return codigoSetor;
	}

	public void setCodigoSetor(Integer codigoSetor) {
		this.codigoSetor = codigoSetor;
	}

	public Integer getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(Integer codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	public String getUnidadeNegocio() {
		return unidadeNegocio;
	}

	public void setUnidadeNegocio(String unidadeNegocio) {
		this.unidadeNegocio = unidadeNegocio;
	}

	public Integer getTransportador() {
		return transportador;
	}

	public void setTransportador(Integer transportador) {
		this.transportador = transportador;
	}

	public String getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public Integer getCodigoOcorrencia() {
		return codigoOcorrencia;
	}

	public void setCodigoOcorrencia(Integer codigoOcorrencia) {
		this.codigoOcorrencia = codigoOcorrencia;
	}

	public String getPonto() {
		return ponto;
	}

	public void setPonto(String ponto) {
		this.ponto = ponto;
	}

	@Override
	public CombosLogistica map(ResultSet rs) throws SQLException {
		CombosLogistica model = new CombosLogistica();

		if (hasColumn(rs, "DEPARTAMENTO")) {
			model.setDepartamento(StringUtils.trim(rs.getString("DEPARTAMENTO")));
			model.setCodigoSetor(rs.getInt("COD_SETOR"));
		} 
		else if (hasColumn(rs, "COD_UNID")) {
			model.setCodigoUnidade(rs.getInt("COD_UNID"));
			model.setUnidadeNegocio(StringUtils.trim(rs.getString("UNID_NEG")));
		} 
		else if (hasColumn(rs, "CD_FNE_OLN")) {
			model.setTransportador(rs.getInt("CD_FNE_OLN"));
		} 
		else if (hasColumn(rs, "CD_TRKOLN")) {
			model.setPonto(rs.getString("CD_TRKOLN"));
		} 
		else if (hasColumn(rs, "CD_MIDEXT_RET")) {
			model.setOcorrencia(StringUtils.trim(rs.getString("CD_MIDEXT_RET")));
			model.setCodigoOcorrencia(rs.getInt("CD_MIDEXT"));
		}

		return model;
	}
}