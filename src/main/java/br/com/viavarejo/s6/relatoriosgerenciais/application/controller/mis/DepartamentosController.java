package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.mis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/mis")
public class DepartamentosController {

	@Autowired
	@Qualifier("Departamentos")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/departamentos")
    public ResponseEntity<?> listar() {
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.DEPARTAMENTOS);
    }
}