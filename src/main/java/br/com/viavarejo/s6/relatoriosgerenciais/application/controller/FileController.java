package br.com.viavarejo.s6.relatoriosgerenciais.application.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.model.RelatorioCSV;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.file.XLSFormatter;

@RestController
public class FileController {

	@PostMapping("/download")
	public void download(HttpServletRequest request, HttpServletResponse response, @RequestBody String json) throws IOException {		
		response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=\"teste.xls\"");
        
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readValue(json, JsonNode.class);
        List<RelatorioCSV> report = createReportByJsonNode(root);
		HSSFWorkbook workbook = new XLSFormatter().geraXLS(report, root.get("title").asText());
		
		workbook.write(response.getOutputStream());
	}
	
	private List<RelatorioCSV> createReportByJsonNode(JsonNode report) {
		StringBuilder builder = new StringBuilder();
		ArrayNode headers = (ArrayNode)report.get("headers");
		ArrayNode items = (ArrayNode)report.get("data");
		Integer lineNumber = 1;
		List<RelatorioCSV> response = new ArrayList<>();
		Integer index = 0;
		
		for(JsonNode headerNode : headers) {
			builder.append(headerNode.get("header").asText());
			
			if(index < (headers.size() - 1) ) { 
				builder.append(";");
			}
			
			index++;
		}
		
		response.add(new RelatorioCSV(lineNumber++, builder.toString()));

		for(JsonNode item : items) {
			builder.setLength(0);
			index = 0;
			
			for(JsonNode header: headers) {
				if(verificarCampo(item, header)) {
					builder.append(item.get(header.get("field").asText()).asText());
				}
				else {
					builder.append(" ");
				}
				
				if(index < (headers.size() - 1) ) { 
					builder.append(";");
				}
				
				index++;
			}
			
			response.add(new RelatorioCSV(lineNumber++, builder.toString()));
		}
		
		return response;
	}

	private static boolean verificarCampo(JsonNode item, JsonNode header) {
		return item.get(header.get("field").asText()) != null
				&& !item.get(header.get("field").asText()).asText().equals("")
				&& !item.get(header.get("field").asText()).asText().equals("null");
	}
}