package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class Departamentos extends BaseModel<Departamentos> {

	private static final long serialVersionUID = 2946357788897812000L;

	private int codDepto;
	private String nomDepto;

	public int getCodDepto() {
		return this.codDepto;
	}

	public void setCodDepto(final int codDepto) {
		this.codDepto = codDepto;
	}

	public String getNomDepto() {
		return this.nomDepto;
	}

	public void setNomDepto(final String nomDepto) {
		this.nomDepto = nomDepto;
	}

	@Override
	public Departamentos map(ResultSet rs) throws SQLException {
		Departamentos model = new Departamentos();
		model.setCodDepto(rs.getInt("CD_DEPTO"));
		model.setNomDepto(StringUtils.trim(rs.getString("NM_DEPTO")));
		return model;
	}
}