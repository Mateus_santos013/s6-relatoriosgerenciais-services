package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class AgendamentoMassivo extends BaseModel<AgendamentoMassivo> {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String tipo;
	private String numDocumento;
	private String msgRet;
	private int codRet;
	private int empresa;
	private int matricula;
	private int dsm;
	private String hashCode;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumDocumento() {
		return numDocumento;
	}
	
	
	public String getNumDocumento20() {
		return StringUtils.leftPad(this.getNumDocumento(), 20, '0');
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	
	public String getMsgRet() {
		return msgRet;
	}

	public void setMsgRet(String msgRet) {
		this.msgRet = msgRet;
	}

	public int getCodRet() {
		return codRet;
	}

	public void setCodRet(int codRet) {
		this.codRet = codRet;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public int getDsm() {
		return dsm;
	}

	public void setDsm(int dsm) {
		this.dsm = dsm;
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override // Mapper
	public AgendamentoMassivo map(ResultSet rs) throws SQLException {
		AgendamentoMassivo model = new AgendamentoMassivo();
		model.setCodRet(rs.getInt("CD_RET"));
		model.setMsgRet(rs.getString("DS_MSG"));
		return model;
	}

}
