package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/logistica-reversa")
public class RelatorioCSVController {

	@Autowired
	@Qualifier("RelatorioCSV")
  	private StoredProcedureRepository repository;
	
	@GetMapping("/relatorioCSV")
	public ResponseEntity<?> listar(
    		@RequestParam(name="opcao") String opcao,
    		@RequestParam(name="numDoc") Integer numDoc) {
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
 		params.put("opcao", opcao);
 		params.put("numDoc", numDoc);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.RELATORIO_CSV, params);
    }
}