package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DadosCliente extends BaseModel<DadosCliente> {

	private static final long serialVersionUID = -1951089510756328642L;

	private Integer filial;
	private String bandeira;
	private Long entrega;
	private Integer numDpto;
	private String nomeDpto;
	private Integer numItem;
	private String descItem;
	private Date dataPrometida;
	private String uf;
	private String nomeCliente;
	private String email;
	private String ddd1;
	private String telefone1;
	private String ddd2;
	private String telefone2;
	private Integer transitTime;
	private Integer nf;
	private String serie;
	private String status;
	private String descStatus;
	private Date dataProc;
	private String entidade;
	private String cidade;
	private String cep;
	private String endereco;
	private String numero;
	private String complemento;

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public Long getEntrega() {
		return entrega;
	}

	public void setEntrega(Long entrega) {
		this.entrega = entrega;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Integer getNf() {
		return nf;
	}

	public void setNf(Integer nf) {
		this.nf = nf;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Integer getNumDpto() {
		return numDpto;
	}

	public void setNumDpto(Integer numDpto) {
		this.numDpto = numDpto;
	}

	public String getNomeDpto() {
		return nomeDpto;
	}

	public void setNomeDpto(String nomeDpto) {
		this.nomeDpto = nomeDpto;
	}

	public Integer getNumItem() {
		return numItem;
	}

	public void setNumItem(Integer numItem) {
		this.numItem = numItem;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public Date getDataPrometida() {
		return dataPrometida;
	}

	public void setDataPrometida(Date dataPrometida) {
		this.dataPrometida = dataPrometida;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getDdd1() {
		return ddd1;
	}

	public void setDdd1(String ddd1) {
		this.ddd1 = ddd1;
	}

	public String getDdd2() {
		return ddd2;
	}

	public void setDdd2(String ddd2) {
		this.ddd2 = ddd2;
	}

	public Integer getTransitTime() {
		return transitTime;
	}

	public void setTransitTime(Integer transitTime) {
		this.transitTime = transitTime;
	}

	public String getDescStatus() {
		return descStatus;
	}

	public void setDescStatus(String descStatus) {
		this.descStatus = descStatus;
	}

	public Date getDataProc() {
		return dataProc;
	}

	public void setDataProc(Date dataProc) {
		this.dataProc = dataProc;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	@Override
	public DadosCliente map(ResultSet rs) throws SQLException {
		DadosCliente model = new DadosCliente();	
		model.setFilial(rs.getInt("FILIAL"));
		model.setBandeira(StringUtils.trim(rs.getString("BANDEIRA")));
		model.setEntrega(rs.getLong("ENTREGA"));
		model.setNumDpto(rs.getInt("NRO_DEPTO"));
		model.setNomeDpto(StringUtils.trim(rs.getString("NOME_DEPTO")));
		model.setNumItem(rs.getInt("NRO_ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DESC_ITEM")));
		model.setDataPrometida(rs.getDate("DATA_COLETA"));			
		model.setNomeCliente(StringUtils.trim(rs.getString("NOME_CLIENTE")));
		model.setEmail(StringUtils.trim(rs.getString("E_MAIL")));
		model.setDdd1(StringUtils.trim(rs.getString("DDD1")));
		model.setTelefone1(StringUtils.trim(rs.getString("TELEFONE_1")));
		model.setDdd2(StringUtils.trim(rs.getString("DDD2")));
		model.setTelefone2(StringUtils.trim(rs.getString("TELEFONE_2")));
		model.setUf(StringUtils.trim(rs.getString("UF")));
		model.setCidade(StringUtils.trim(rs.getString("CIDADE")));
		model.setCep(StringUtils.trim(rs.getString("CEP")));
		model.setEndereco(StringUtils.trim(rs.getString("ENDERECO")));
		model.setNumero(StringUtils.trim(rs.getString("NUMERO")));
		model.setComplemento(StringUtils.trim(rs.getString("COMPLEMENTO")));			
		model.setTransitTime(rs.getInt("TRANSIT_TIME"));
		model.setNf(rs.getInt("NOTA_FISCAL"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setStatus(StringUtils.trim(rs.getString("STATUS_ATUAL")));
		model.setDescStatus(StringUtils.trim(rs.getString("DESCRI_STATUS_ATUAL")));
		model.setDataProc(rs.getDate("DATA_PROCESSAMENTO"));
		return model;
	}
}