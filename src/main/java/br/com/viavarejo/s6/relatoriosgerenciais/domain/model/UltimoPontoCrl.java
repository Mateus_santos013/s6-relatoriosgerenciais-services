package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;

public class UltimoPontoCrl extends BaseModel<UltimoPontoCrl> {

	private static final long serialVersionUID = 1L;

	private Integer pedCd;
	private Integer pedDsm;
	private String tipo;
	private String pedOn;
	private Integer pedOnseq;
	private BigDecimal entrega;
	private String sisOrig;
	private Integer nota;
	private String serie;
	private String unidNeg;
	private String dsTpEntrega;
	private String tpVenda;
	private String tpDevoluc;
	private String vlEntrega;
	private String vlTotalNf;
	private String cdOcorr;
	private String dsOcorr;
	private String dtOcorr;
	private String hrOcorr;
	private String dtProcess;
	private String hrProcess;
	private String dtSaida;
	private String dtPrometidaUm;
	private String tprCnpj;
	private String nomeTrp;
	private Integer contrato;
	private String dsContrato;
	private Integer empFil;
	private Integer fil;
	private String dsFilial;
	private BigDecimal cep;
	private String mun;
	private String uf;
	private String email;
	private String telo1;
	private String telo2;
	private String matricula;
	private String nomeFunc;
	private String dtPrev;
	private String dtCorrig;

	public Integer getPedCd() {
		return pedCd;
	}

	public void setPedCd(Integer pedCd) {
		this.pedCd = pedCd;
	}

	public Integer getPedDsm() {
		return pedDsm;
	}

	public void setPedDsm(Integer pedDsm) {
		this.pedDsm = pedDsm;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getPedOn() {
		return pedOn;
	}

	public void setPedOn(String pedOn) {
		this.pedOn = pedOn;
	}

	public Integer getPedOnseq() {
		return pedOnseq;
	}

	public void setPedOnseq(Integer pedOnseq) {
		this.pedOnseq = pedOnseq;
	}

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public String getSisOrig() {
		return sisOrig;
	}

	public void setSisOrig(String sisOrig) {
		this.sisOrig = sisOrig;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getUnidNeg() {
		return unidNeg;
	}

	public void setUnidNeg(String unidNeg) {
		this.unidNeg = unidNeg;
	}

	public String getDsTpEntrega() {
		return dsTpEntrega;
	}

	public void setDsTpEntrega(String dsTpEntrega) {
		this.dsTpEntrega = dsTpEntrega;
	}

	public String getTpVenda() {
		return tpVenda;
	}

	public void setTpVenda(String tpVenda) {
		this.tpVenda = tpVenda;
	}

	public String getTpDevoluc() {
		return tpDevoluc;
	}

	public void setTpDevoluc(String tpDevoluc) {
		this.tpDevoluc = tpDevoluc;
	}

	public String getVlEntrega() {
		return vlEntrega;
	}

	public void setVlEntrega(String vlEntrega) {
		this.vlEntrega = vlEntrega;
	}

	public String getVlTotalNf() {
		return vlTotalNf;
	}

	public void setVlTotalNf(String vlTotalNf) {
		this.vlTotalNf = vlTotalNf;
	}

	public String getCdOcorr() {
		return cdOcorr;
	}

	public void setCdOcorr(String cdOcorr) {
		this.cdOcorr = cdOcorr;
	}

	public String getDsOcorr() {
		return dsOcorr;
	}

	public void setDsOcorr(String dsOcorr) {
		this.dsOcorr = dsOcorr;
	}

	public String getDtOcorr() {
		return dtOcorr;
	}

	public void setDtOcorr(String dtOcorr) {
		this.dtOcorr = dtOcorr;
	}

	public String getHrOcorr() {
		return hrOcorr;
	}

	public void setHrOcorr(String hrOcorr) {
		this.hrOcorr = hrOcorr;
	}

	public String getDtProcess() {
		return dtProcess;
	}

	public void setDtProcess(String dtProcess) {
		this.dtProcess = dtProcess;
	}

	public String getHrProcess() {
		return hrProcess;
	}

	public void setHrProcess(String hrProcess) {
		this.hrProcess = hrProcess;
	}

	public String getDtSaida() {
		return dtSaida;
	}

	public void setDtSaida(String dtSaida) {
		this.dtSaida = dtSaida;
	}

	public String getDtPrometidaUm() {
		return dtPrometidaUm;
	}

	public void setDtPrometidaUm(String dtPrometidaUm) {
		this.dtPrometidaUm = dtPrometidaUm;
	}

	public String getTprCnpj() {
		return tprCnpj;
	}

	public void setTprCnpj(String tprCnpj) {
		this.tprCnpj = tprCnpj;
	}

	public String getNomeTrp() {
		return nomeTrp;
	}

	public void setNomeTrp(String nomeTrp) {
		this.nomeTrp = nomeTrp;
	}

	public Integer getContrato() {
		return contrato;
	}

	public void setContrato(Integer contrato) {
		this.contrato = contrato;
	}

	public String getDsContrato() {
		return dsContrato;
	}

	public void setDsContrato(String dsContrato) {
		this.dsContrato = dsContrato;
	}

	public Integer getEmpFil() {
		return empFil;
	}

	public void setEmpFil(Integer empFil) {
		this.empFil = empFil;
	}

	public Integer getFil() {
		return fil;
	}

	public void setFil(Integer fil) {
		this.fil = fil;
	}

	public String getDsFilial() {
		return dsFilial;
	}

	public void setDsFilial(String dsFilial) {
		this.dsFilial = dsFilial;
	}

	public BigDecimal getCep() {
		return cep;
	}

	public void setCep(BigDecimal cep) {
		this.cep = cep;
	}

	public String getMun() {
		return mun;
	}

	public void setMun(String mun) {
		this.mun = mun;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelo1() {
		return telo1;
	}

	public void setTelo1(String telo1) {
		this.telo1 = telo1;
	}

	public String getTelo2() {
		return telo2;
	}

	public void setTelo2(String telo2) {
		this.telo2 = telo2;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNomeFunc() {
		return nomeFunc;
	}

	public void setNomeFunc(String nomeFunc) {
		this.nomeFunc = nomeFunc;
	}

	public String getDtPrev() {
		return dtPrev;
	}

	public void setDtPrev(String dtPrev) {
		this.dtPrev = dtPrev;
	}

	public String getDtCorrig() {
		return dtCorrig;
	}

	public void setDtCorrig(String dtCorrig) {
		this.dtCorrig = dtCorrig;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	
	@Override
	public UltimoPontoCrl map(ResultSet rs) throws SQLException {
		UltimoPontoCrl model = new UltimoPontoCrl();
		model.setPedCd(rs.getInt("PED_CD"));
		model.setPedDsm(rs.getInt("PED_DSM"));
		model.setTipo(StringUtils.trim(rs.getString("TIPO")));
		model.setPedOn(StringUtils.trim(rs.getString("PED_ON")));
		model.setPedOnseq(rs.getInt("PED_ONSEQ"));
		model.setEntrega(rs.getBigDecimal("ENTREGA"));
		model.setSisOrig(StringUtils.trim(rs.getString("SIS_ORIG")));
		model.setNota(rs.getInt("NOTA"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setUnidNeg(StringUtils.trim(rs.getString("UNIDNEG")));
		model.setDsTpEntrega(StringUtils.trim(rs.getString("DS_TPENTREGA")));
		model.setTpVenda(StringUtils.trim(rs.getString("TPVENDA")));
		model.setTpDevoluc(StringUtils.trim(rs.getString("TPDEVOLUC")));
		model.setVlEntrega(StringUtils.trim(rs.getString("VL_ENTREGA")));
		model.setVlTotalNf(StringUtils.trim(rs.getString("VL_TOTAL_NF")));
		model.setCdOcorr(StringUtils.trim(rs.getString("CDOCORR")));
		model.setDsOcorr(StringUtils.trim(rs.getString("DSOCORR")));
		model.setDtOcorr(StringUtils.trim(rs.getString("DTOCORR")));
		model.setHrOcorr(StringUtils.trim(rs.getString("HROCORR")));
		model.setDtProcess(StringUtils.trim(rs.getString("DTPROCESS")));
		model.setHrProcess(StringUtils.trim(rs.getString("HRPROCESS")));
		model.setDtSaida(StringUtils.trim(rs.getString("DTSAIDA")));
		model.setDtPrometidaUm(StringUtils.trim(rs.getString("DTPROMET1")));
		model.setTprCnpj(StringUtils.trim(rs.getString("TRP_CNPJ")));
		model.setNomeTrp(StringUtils.trim(rs.getString("TRP_CNPJ")));
		model.setContrato(rs.getInt("CONTRATO"));
		model.setDsContrato(rs.getString("DSCONTRATO"));
		model.setEmpFil(rs.getInt("EMPFIL"));
		model.setFil(rs.getInt("FIL"));
		model.setDsFilial(StringUtils.trim(rs.getString("DSFILIAL")));
		model.setCep(rs.getBigDecimal("CEP"));
		model.setMun(StringUtils.trim(rs.getString("MUN")));
		model.setUf(StringUtils.trim(rs.getString("UF")));
		model.setEmail(StringUtils.trim(rs.getString("EMAIL")));
		model.setTelo1(StringUtils.trim(rs.getString("TEL01")));
		model.setTelo2(StringUtils.trim(rs.getString("TEL02")));
		model.setMatricula(StringUtils.trim(rs.getString("MATRICULA")));
		model.setNomeFunc(StringUtils.trim(rs.getString("NOMEFUNC")));
		model.setDtPrev(StringUtils.trim(rs.getString("DTPREV")));
		model.setDtCorrig(StringUtils.trim(rs.getString("DTCORRIG")));
		
		return model;
	}

}
