package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.repository.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.RepositoryException;

@Repository
public class StoredProcedureRepositoryImpl 
	extends BaseRepository 
	implements StoredProcedureRepository {

	public StoredProcedureRepositoryImpl() {
	}
	
	public StoredProcedureRepositoryImpl(Class<?> clazz) {
		super(clazz);
	}

	@Override
	public List<?> findAll(StoredProcedures storedProcedure, Map<String, Object> parameters) throws RepositoryException {	
		final String procedure = buildCall(storedProcedure);
		
		try {
			RowMapper<?> mapper = (RowMapper<?>)getMapperInstance();
			Object[] procedureParameters = parameters.values().toArray();
			return jdbcTemplate.query(procedure, procedureParameters, mapper);
		}
		catch (Exception e) {
			final String message = e.getCause().getMessage().trim();
			throw new RepositoryException(message, e);
		}
	}
	
	@Override
	public List<?> findAll(StoredProcedures storedProcedure) throws RepositoryException {
		final String procedure = buildCall(storedProcedure);
		
		try {
			RowMapper<?> mapper = (RowMapper<?>)getMapperInstance();
			return jdbcTemplate.query(procedure, mapper);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			final String message = e.getCause().getMessage().trim();
			throw new RepositoryException(message, e);
		}
	}
}