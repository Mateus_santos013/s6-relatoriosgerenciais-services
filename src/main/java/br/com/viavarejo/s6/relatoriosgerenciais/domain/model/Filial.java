package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Filial extends BaseModel<Filial> {

	private static final long serialVersionUID = -594985888457387929L;

	private Integer filial;

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	@Override
	public Filial map(ResultSet rs) throws SQLException {
		Filial model = new Filial();
		model.setFilial(rs.getInt("FILIAL"));
		return model;
	}
}