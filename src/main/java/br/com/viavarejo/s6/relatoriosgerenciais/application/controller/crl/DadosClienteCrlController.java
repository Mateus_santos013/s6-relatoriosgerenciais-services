package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.crl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.RepositoryCommand;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;

@RestController
@RequestMapping("/crl")
public class DadosClienteCrlController {
	
	@Autowired
	@Qualifier("DadosClienteCrl")
	private StoredProcedureRepository repository;
	
	
	@GetMapping("/dados-cliente-crl")
	public ResponseEntity<?> listar(@RequestParam(name="tipoSolicitacao") String tipoSolicitacao, @RequestParam(name="entregas") String entregas) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("tipoSolicitacao", tipoSolicitacao);
		params.put("entregas", entregas);
		
		RepositoryCommand command = new RepositoryCommand(this.repository);
		return command.execute(StoredProcedures.DADOS_CLIENTE_CRL, params);
	}

}
