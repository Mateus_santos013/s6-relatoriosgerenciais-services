package br.com.viavarejo.s6.relatoriosgerenciais.application.controller.logisticareversa;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.viavarejo.s6.relatoriosgerenciais.domain.StoredProcedures;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.model.SubidaLote;
import br.com.viavarejo.s6.relatoriosgerenciais.domain.repository.StoredProcedureRepository;
import br.com.viavarejo.s6.relatoriosgerenciais.infrastructure.exception.RepositoryException;

@RestController
@RequestMapping("/logistica-reversa")
public class SubidaLoteController {

	@Autowired
	@Qualifier("SubidaLote")
	private StoredProcedureRepository repository;

	@SuppressWarnings("unchecked")
	@PostMapping("/subida-lote")
	public ResponseEntity<?> listar(@RequestBody List<SubidaLote> lista) throws Exception {

		for (SubidaLote item : lista) {
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("opcao", item.getOpcao());
			params.put("tipo", item.getTipo());
			params.put("documento", item.getNumDocumento18());
			params.put("dsm", item.getDsm());
			params.put("dataProc", item.getDataProcPonto());
			params.put("horaProc", item.getHoraProc());
			params.put("ocorrencia", item.getOcorrPControle());
			params.put("funcionario", item.getFunc() + item.getObs());

			try {
				SubidaLote tmp = ((List<SubidaLote>) repository.findAll(StoredProcedures.SUBIDA_LOTE, params)).get(0);
				item.setCodRet(tmp.getCodRet());
				item.setMsgRet(tmp.getMsgRet());
			} 
			catch (RepositoryException e) {
				item.setCodRet(99);
				item.setMsgRet(e.getLocalizedMessage());
			}
		}
		
		return new ResponseEntity<List<SubidaLote>>(lista, HttpStatus.OK);
	}
}