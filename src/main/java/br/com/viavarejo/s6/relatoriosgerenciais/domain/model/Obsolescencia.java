package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class Obsolescencia extends BaseModel<Obsolescencia> {

	private static final long serialVersionUID = -6568956354657039357L;

	private Integer filial;
	private BigDecimal item;
	private String produto;
	private String departamento;
	private Integer qtdRecebida;
	private Integer qtdDisponivel;
	private Date dtRecebimento;
	private BigDecimal cmv;
	private BigDecimal cmvTotal;
	private Integer tempoSemVenda;
	private String tempoEmEstoque;
	private String nr;

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public BigDecimal getItem() {
		return item;
	}

	public void setItem(BigDecimal item) {
		this.item = item;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Integer getQtdRecebida() {
		return qtdRecebida;
	}

	public void setQtdRecebida(Integer qtdRecebida) {
		this.qtdRecebida = qtdRecebida;
	}

	public Integer getQtdDisponivel() {
		return qtdDisponivel;
	}

	public void setQtdDisponivel(Integer qtdDisponivel) {
		this.qtdDisponivel = qtdDisponivel;
	}

	public Date getDtRecebimento() {
		return dtRecebimento;
	}

	public void setDtRecebimento(Date dtRecebimento) {
		this.dtRecebimento = dtRecebimento;
	}

	public BigDecimal getCmv() {
		return cmv;
	}

	public void setCmv(BigDecimal cmv) {
		this.cmv = cmv;
	}

	public BigDecimal getCmvTotal() {
		return cmvTotal;
	}

	public void setCmvTotal(BigDecimal cmvTotal) {
		this.cmvTotal = cmvTotal;
	}

	public Integer getTempoSemVenda() {
		return tempoSemVenda;
	}

	public void setTempoSemVenda(Integer tempoSemVenda) {
		this.tempoSemVenda = tempoSemVenda;
	}

	public String getTempoEmEstoque() {
		return tempoEmEstoque;
	}

	public void setTempoEmEstoque(String tempoEmEstoque) {
		this.tempoEmEstoque = tempoEmEstoque;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public Obsolescencia map(ResultSet rs) throws SQLException {
		Obsolescencia model = new Obsolescencia();
		model.setFilial(rs.getInt("FILIAL"));
		model.setItem(rs.getBigDecimal("ITEM"));
		model.setProduto(StringUtils.trim(rs.getString("PRODUTO")));
		model.setDepartamento(StringUtils.trim(rs.getString("DEPARTAMENTO")));
		model.setQtdRecebida(rs.getInt("QUANTIDADE_RECEBIDA"));
		model.setQtdDisponivel(rs.getInt("QUANTIDADE_DISPONIVEL"));
		model.setDtRecebimento(rs.getDate("DATA_RECEBIMENTO"));
		model.setCmv(rs.getBigDecimal("CMV"));
		model.setCmvTotal(rs.getBigDecimal("CMV_TOTAL"));
		model.setTempoSemVenda(rs.getInt("TEMPO_SEM_VENDA"));
		model.setTempoEmEstoque(StringUtils.trim(rs.getString("TEMPO_ESTOQUE")));
		model.setNr(StringUtils.trim(rs.getString("NR")));
		return model;
	}
}