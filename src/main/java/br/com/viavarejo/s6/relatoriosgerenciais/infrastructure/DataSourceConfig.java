package br.com.viavarejo.s6.relatoriosgerenciais.infrastructure;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataSourceConfig {
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="datasource.db2")
	public DataSourceProperties appDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@Primary
	@ConfigurationProperties(prefix="datasource.db2")
	public HikariDataSource appDataSource() {
		return appDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}
}