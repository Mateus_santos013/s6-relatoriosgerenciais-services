package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RelatorioCSV extends BaseModel<RelatorioCSV> {

	private static final long serialVersionUID = 2262957725303325627L;

	private String conteudo;
	private Integer numLinha;

	public RelatorioCSV() {
	}
	
	public RelatorioCSV(Integer numLinha, String conteudo) {
		this.conteudo = conteudo;
		this.numLinha = numLinha;
	}
	
	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Integer getNumLinha() {
		return numLinha;
	}

	public void setNumLinha(Integer numLinha) {
		this.numLinha = numLinha;
	}

	@Override
	public RelatorioCSV map(ResultSet rs) throws SQLException {
		RelatorioCSV model = new RelatorioCSV();
		model.setConteudo(rs.getString("REL_CONTEUDO"));
		model.setNumLinha(rs.getInt("LINHA"));
		return model;
	}
}