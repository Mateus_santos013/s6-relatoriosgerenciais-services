package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;


import org.apache.commons.lang3.StringUtils;

public class DevolucoesPendentes extends BaseModel<DevolucoesPendentes> {

	private static final long serialVersionUID = -5953296737096478516L;

	private BigDecimal entrega;
	private Integer nota;
	private String serie;
	private Integer filial;
	private String uf;
	private String transportadora;
	private String nomeCliente;
	private Integer item;
	private String descItem;
	private Integer qtdeItem;
	private BigDecimal valorItem;
	private BigDecimal valorFrete;
	private BigDecimal valorDesconto;
	private String dataRetorno;
	private String ocorrencia;
	private String coleta;
	private String dataColeta;
	private String rtd;
	private String dataRetirada;
	private String tipoDoc;
	private Integer documento;
	private Integer dsm;

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public Integer getQtdeItem() {
		return qtdeItem;
	}

	public void setQtdeItem(Integer qtdeItem) {
		this.qtdeItem = qtdeItem;
	}

	public BigDecimal getValorItem() {
		return valorItem;
	}

	public void setValorItem(BigDecimal valorItem) {
		this.valorItem = valorItem;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(String dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public String getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(String dataColeta) {
		this.dataColeta = dataColeta;
	}

	public String getDataRetirada() {
		return dataRetirada;
	}

	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}

	public DevolucoesPendentes() {
	}

	public BigDecimal getEntrega() {
		return entrega;
	}

	public void setEntrega(BigDecimal entrega) {
		this.entrega = entrega;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getFilial() {
		return filial;
	}

	public void setFilial(Integer filial) {
		this.filial = filial;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(String transportadora) {
		this.transportadora = transportadora;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public String getColeta() {
		return coleta;
	}

	public void setColeta(String coleta) {
		this.coleta = coleta;
	}

	public String getRtd() {
		return rtd;
	}

	public void setRtd(String rtd) {
		this.rtd = rtd;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public Integer getDocumento() {
		return documento;
	}

	public void setDocumento(Integer documento) {
		this.documento = documento;
	}

	public Integer getDsm() {
		return dsm;
	}

	public void setDsm(Integer dsm) {
		this.dsm = dsm;
	}

	@Override
	public DevolucoesPendentes map(ResultSet rs) throws SQLException {
		DevolucoesPendentes model = new DevolucoesPendentes();
		model.setEntrega(rs.getBigDecimal("ENTREGA"));
		model.setTipoDoc(StringUtils.trim(rs.getString("TIPO DOC")));
		model.setDocumento(rs.getInt("DOCUMENTO"));
		model.setDsm(rs.getInt("DSM"));
		model.setNota(rs.getInt("NOTA"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setFilial(rs.getInt("FILIAL"));
		model.setUf(StringUtils.trim(rs.getString("UF")));
		model.setTransportadora(StringUtils.trim(rs.getString("TRANSPORTADORA")));
		model.setNomeCliente(StringUtils.trim(rs.getString("NOME CLIENTE")));
		model.setItem(rs.getInt("ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DESCRICAO ITEM")));
		model.setQtdeItem(rs.getInt("QTD ITEM"));
		model.setValorItem(rs.getBigDecimal("VALOR ITEM"));
		model.setValorFrete(rs.getBigDecimal("VALOR FRETE"));
		model.setValorDesconto(rs.getBigDecimal("VALOR DESCONTO"));
		model.setDataRetorno(rs.getString("DATA RETORNO PROMETIDA"));
		model.setColeta(StringUtils.trim(rs.getString("COL")));
		model.setDataColeta(rs.getString("DATA COL"));
		model.setRtd(StringUtils.trim(rs.getString("RTD")));
		model.setDataRetirada(rs.getString("DATA RTD"));
		return model;
	}
}