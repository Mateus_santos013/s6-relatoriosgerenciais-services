package br.com.viavarejo.s6.relatoriosgerenciais.domain.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class NotaFiscal extends BaseModel<NotaFiscal> {

	private static final long serialVersionUID = -1038442559866207950L;

	private Integer codigoFilial;
	private Date dataEmissao;
	private Integer nota;
	private String serie;
	private String cfop;
	private String descCfop;
	private Integer naturezaOperacao;
	private String descNatOperacao;
	private Integer item;
	private String descItem;
	private Integer qtde;
	private BigDecimal valorUnitario;
	private BigDecimal valorTotal;
	private String fabricante;

	public Integer getCodigoFilial() {
		return codigoFilial;
	}

	public void setCodigoFilial(Integer codigoFilial) {
		this.codigoFilial = codigoFilial;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	
	public String getCfop() {
		return cfop;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public String getDescCfop() {
		return descCfop;
	}

	public void setDescCfop(String descCfop) {
		this.descCfop = descCfop;
	}

	public Integer getNaturezaOperacao() {
		return naturezaOperacao;
	}

	public void setNaturezaOperacao(Integer naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}

	public String getDescNatOperacao() {
		return descNatOperacao;
	}

	public void setDescNatOperacao(String descNatOperacao) {
		this.descNatOperacao = descNatOperacao;
	}

	public Integer getItem() {
		return item;
	}

	public void setItem(Integer item) {
		this.item = item;
	}

	public String getDescItem() {
		return descItem;
	}

	public void setDescItem(String descItem) {
		this.descItem = descItem;
	}

	public Integer getQtde() {
		return qtde;
	}

	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	
	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public NotaFiscal map(ResultSet rs) throws SQLException {
		NotaFiscal model = new NotaFiscal();
		model.setCodigoFilial(rs.getInt("CODIGO_FILIAL"));	
		//model.setCodigoFilial(StringUtils.trim(rs.getInt("DESCRICAO_FILIAL")));
		model.setDataEmissao(rs.getDate("DATA_EMISSAO"));
		model.setNota(rs.getInt("NOTA"));
		model.setSerie(StringUtils.trim(rs.getString("SERIE")));
		model.setCfop(StringUtils.trim(rs.getString("CFOP")));
		model.setDescCfop(StringUtils.trim(rs.getString("DESCRICAO_CFOP")));
		model.setNaturezaOperacao(rs.getInt("NATUREZA_OPERACAO"));
		model.setDescNatOperacao(StringUtils.trim(rs.getString("NATUREZA_OPERACAO")));
		model.setItem(rs.getInt("ITEM"));
		model.setDescItem(StringUtils.trim(rs.getString("DESCRICAO_ITEM")));
		model.setQtde(rs.getInt("QTDE"));
		model.setValorUnitario(rs.getBigDecimal("VALOR_UNITARIO"));
		model.setValorTotal(rs.getBigDecimal("VALOR_TOTAL"));
		model.setFabricante(StringUtils.trim(rs.getString("FABRICANTE")));
		return model;
	}
}